<?php	require_once('setting/config.php'); ?>
	<!DOCTYPE html>
	<html lang="en">

	<?php	include_once('partials/head.php'); ?>

		<body class="inner">

			<br>
			<br>
			<br>

			<div class="container help-line">
				<div>
					<img src="images/help-line.png" class="hidden-xs">
					<img src="images/help-line2.png" class="visible-xs">
				</div>
			</div>

			<!-- main navigation -->
			<?php include_once('partials/mainnav2.php'); ?>
				<!-- end main navigation -->


				<div class="slide-wrapper">

					<?php	include_once('partials/inner_carousel.php'); ?>


						<!-- content -->
						<div class="container">
							<div class="body-container">

								<div class="breadcrumb">
									<div class="row">
										<div class="col-sm-6">
											<h1><b>SMILES  </b> Frequent Flyer Program</h1>
										</div>
										<div class="col-sm-6 text-right"> Services / SMILES </div>
									</div>
								</div>

								<div class="page-contents">
									<div class="row">
										<div class="col-sm-9 col-xs-12">
											<img src="images/services/smiles.jpg" width="100%" class="img-responsive">

											<p class="text-center">Welcome to the first Frequent Flyer program in Bangladesh!
												<br> NOVOAIR values your choice in our Service; and this is Our Way of Rewarding You for it.
												<br> There&rsquo;s plenty to look forward to when you fly with our NOVOAIR SMILES card.
												<br>

												<strong>
													<a href="http://secure.flynovoair.com/rewards/enroll.asp">Enroll Now</a>. Take full advantage of your personal business-choice Airlines.
												</strong>
											</p>

											<h2>Introducing NOVOAIR Rewards Programme</h2>

											<p class="text-justify">The NOVOAIR Rewards portfolio is a growing collection of Rewards choices from which our qualified Passengers will be able to redeem Personalized Services, one-way Flights, Travel Deals &amp; Packages, and Partner Promotions for their SMILES
												Miles. NOVOAIR is initially offering one-way Flights to your favorite destinations, and Status upgrades to take advantage of early access to Partner Promotions.</p>

											<p class="text-justify">Manage your personal Smiles Account; track your Smiles, book Reward Tickets, and more exclusive Rewards, all at the convenience of your home or office by simply logging into your Smiles Account with your Smiles Frequent Flyer Number and PIN.
												Members will be able to see most recent activity in updated SMILES Account within Seven days, and can only redeem Smiles Rewards if their Smiles Account has sufficient SMILES Miles. Please take some time to familiarize yourself with our frequent
												flyer programme (FFP) Policies and the many Membership Tier Benefits you can enjoy as a Smiles Member. </p>

											<h2>
												<strong>
													<a name="SmilesMembership"></a>
												</strong>Smiles Membership</h2>

											<p class="text-justify">

												<ul>

													<li>Application for membership of SMILES FFP is open to any individual who is at least 18 years of age. A corporation, firm, partnership or other entity is not eligible to become a Member.</li>

												</ul>

												<ul>

													<li>NOVOAIR may require additional information for Account validation purposes in respect of any change to a Member's personal details.</li>

												</ul>

												<ul>

													<li>NOVOAIR will issue a Member with a permanent membership number against which his or her Miles will be accrued. The membership card may state an expiry date and, if so, is only valid for use up to that expiry date. The membership card may
														only be used by the Member named on that card.</li>

												</ul>

												<ul>

													<li>Member Accounts will be available to a Member online while NOVOAIR considers him or her to be an active SMILES Member and will include information concerning SMILES Miles accrued, redeemed and expired.</li>

												</ul>

												<ul>

													<li>A membership card is not transferable and at all times remains the property of NOVOAIR. A Member must return his or her membership card without delay to any NOVOAIR Contact Centre whenever requested by NOVOAIR or any other party acting on
														its behalf. A Member must immediately report a lost membership card to any NOVOAIR Contact Centre.</li>

												</ul>

												<ul>

													<li>NOVOAIR gives no warranty or assurance in relation to such tax, duties or other liabilities. All tax, duties and other liabilities arising from the accumulation of SMILES Miles or the receipt and use of SMILES Rewards are and remain the sole
														responsibility of the Member and must be paid on the time of issuance of any Reward.</li>

												</ul>

												<ul>

													<li>A Member may at any time cancel his or her membership by advising in writing or returning his or her membership card to any NOVOAIR Contact Centre.</li>

												</ul>

												<ul>

													<li>Membership is not transferable or not endorsable. A Member is not permitted to hold more than one Account. All Miles and Rewards under NOVOAIR SMILES FFP are accrued by or issued to the Member in his or her Member Account.</li>

												</ul>

												<ul>

													<li>NOVOAIR may in its absolute discretion suspend, cancel or terminate a membership or a member's accumulated SMILES Miles at any time. NOVOAIR reserves the right to terminate the membership in the event of a breach of the Program Rules by a
														Member. Miles accumulated prior to termination in these circumstances will be immediately cancelled.</li>

												</ul>

												<ul>

													<li>Members are responsible for keeping their name, e-mail and mailing addresses up to date on-line. Account changes requests, including personal information, must be submitted in writing to any NOVOAIR Contact Centers with the account number
														and member&rsquo;s signature. Failure to update member information may result in the cancellation of further mailings until accurate information is provided.</li>

												</ul>

												<ul>

													<li>After Successful enrollment if any member wishes to change his/her name (minor modification), s/he has to send Email at <a href="mailto:smiles@novoair-bd.com">smiles@novoair-bd.com</a> or mail us at: In-charge, SMILES Frequent Flyer Program,
														NOVOAIR Ltd. House: 50, Road: 11, Block: F, Banani, Dhaka. The mail must be contains following information: Present Name, New Name, SMILES ID, Mobile No. &amp; Email Address (email must be according to FFP account). </li>

												</ul>

											</p>

											<div class="clearfix"></div>

											<h2>
												<strong>
													<a name="EarningMiles"></a>
												</strong>Earning Miles</h2>
											<p class="text-justify">
												<ul>

													<li>Any Member&rsquo;s total SMILES Miles in Account is defined as the sum of Passenger FLIGHT Miles earned and BONUS Miles Rewarded. Flight Miles is the accrued Miles earned by our Passenger based on actual Miles flown. Bonus Miles is the accrued
														Miles Rewarded to our Passenger based on promotional offers such as Web-Purchase and Ticket Class boosters. </li>

												</ul>

												<ul>

													<li>Flight Miles may be accrued towards any Member&rsquo;s SMILES Account on qualified fare families; to be credited only after the Flight is complete, even if reservation suffers a no-show. Flight Miles are based on domestic travel destinations
														as shown below:
													</li>

												</ul>
											</p>


											<table class="table table-responsive table-striped">
												<thead>
													<tr>
														<th>DOMESTIC SECTORS</th>
														<th colspan="5">FARE FAMILY WISE FLIGHT MILES ACCRUAL (ONE WAY)</th>
													</tr>

												</thead>
												<tbody>
													<tr style="font-size:1.2em">
														<td>(DAC TO/FROM)</td>
														<td>PROMO
															<br>(x0)</td>
														<td>SPECIAL
															<br>(x0.50)</td>
														<td>DISCOUNTED
															<br>(x0.75)</td>
														<td>SAVER
															<br>(x1.00)</td>
														<td>FLEXIBLE
															<br>(x1.25)</td>
													</tr>
													<tr>
														<td>Jessore (JSR)</td>
														<td>N/A</td>
														<td>46</td>
														<td>69</td>
														<td>69</td>
														<td>115</td>
													</tr>
													<tr>
														<td>Sylhet (ZYL)</td>
														<td>N/A</td>
														<td>60</td>
														<td>90</td>
														<td>120</td>
														<td>150</td>
													</tr>
													<tr>
														<td>Chittagong (CGP)</td>
														<td>N/A</td>
														<td>72</td>
														<td>107</td>
														<td>143</td>
														<td>179</td>
													</tr>
													<tr>
														<td>Cox&rsquo;s Bazar (CXB)</td>
														<td>N/A</td>
														<td>85</td>
														<td>128</td>
														<td>170</td>
														<td>213</td>
													</tr>
												</tbody>
											</table>

											<p class="text-center">
												<ul>

													<li>Bonus Miles may be accrued towards Member&rsquo;s SMILES Account on NOVOAIR Promotional offers. Current offers are:</li>

												</ul>
											</p>

											<table class="table table-responsive table-striped">
												<thead>
													<tr>
														<th>Bonus Miles boosters</th>
														<th>Basic</th>
														<th>Sapphire</th>
														<th>Platinum</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Tier Status</td>
														<td>x1.00</td>
														<td>x1.05</td>
														<td>x1.10</td>
													</tr>
													<tr>
														<td>Web-Purchase</td>
														<td>x1.10</td>
														<td>x1.10</td>
														<td>x1.10</td>
													</tr>
													<tr>
														<td>Ticket Class (Flexible)</td>
														<td>x1.25</td>
														<td>x1.25</td>
														<td>x1.25</td>
													</tr>
												</tbody>
											</table>
											<p class="text-center">
												<ul>

													<li>To earn Miles, a Member must either produce a membership card or quote the SMILES FF# with valid photo ID at the time of reservation or check-in for Qualifying Flights.</li>

												</ul>

												<ul>

													<li>
														<strong>Missing Mileage Claim:</strong> If members do not include SMILES FF# at the time of booking, member can request the credits be applied to his/her account within
														<strong>15 days</strong> of the travel date. A Member must provide travel information such as; Booking Reference#/PNR#, Flight details on the boarding passes, transaction documents, receipts and any other documents requested by NOVOAIR when making
														a retroactive claim. Requests received later than
														<strong>15 days</strong> after travel date will not be honored.</li>

												</ul>

												<ul>

													<li>Tickets issued on the following basis are not eligible to earn SMILES miles: FOC, Promotional Discount, Barter Agreement, Frequent Flyer Redemption and on any other promotion or reward ticket.</li>

												</ul>

												<ul>

													<li>NOVOAIR and/or any partner, at its/their discretion, may also exclude certain types of paid tickets from credit eligibility. These include, but are not limited to, unused tickets, travel industry discounts, charters, tickets obtained or used
														in a fraudulent manner and any other fares/tickets that may later be specified by NOVOAIR, with or without notice. SMILES Miles accrued for any of these reasons may be subsequently deducted from the account.</li>

												</ul>

												<ul>

													<li>If a Member voids or refunds any part of a ticket, Miles will be credited only for the journey flown on Qualifying Flights on that ticket.</li>

												</ul>

												<ul>

													<li>In the event a scheduled NOVOAIR Flight is postponed due to delays, or transferred by passenger, NOVOAIR SMILES FFP will endorse Member&rsquo;s account with the new itinerary.</li>

												</ul>

												<ul>

													<li>SMILES Member accounts will be credited
														<strong>within 7 (seven) days</strong> of completed of travel. Members cannot transfer Miles to any other frequent flyer program.</li>

												</ul>
											</p>
											<h2>
												<strong>
													<a name="MembershipTiers"></a>
												</strong>Membership Tiers and Status Policies </h2>

											<table class="table table-responsive table-striped">
												<thead>
													<tr>
														<th>Membership Tiers</th>
														<th>Basic</th>
														<th>Sapphire</th>
														<th>Platinum</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Qualification</td>
														<td>FFP Enrolment</td>
														<td>700
															<br> SMILES

															<br> Miles</td>
														<td>15,000 SMILES
															<br> Miles</td>
													</tr>
													<tr>
														<td>SMILES Miles Eligibility Period for status upgrades</td>
														<td>6 Months</td>
														<td>1 Year</td>
														<td>2 Years</td>
													</tr>
													<tr>
														<td>SMILES Miles Validity for Redeeming Rewards</td>
														<td>2 Years</td>
														<td>2 Years</td>
														<td>2 Years</td>
													</tr>
													<tr>
														<td>SMILES Card Validity</td>
														<td>No Card</td>
														<td>Rolling</td>
														<td>2 Years</td>
													</tr>
													<tr>
														<td>Tier-expiration</td>
														<td>1 Year</td>
														<td>2 Years</td>
														<td>2 Years</td>
													</tr>
												</tbody>
											</table>

											<p class="text-center">
												<ul>

													<li>SMILES Membership Tier-based Policies are :</li>

												</ul>


												<ul>

													<li>SMILES Member Tiers are defined as Basic, Sapphire and Platinum based on accrued SMILES Miles (&lsquo;SMILES&rsquo;) in Member Account. Tier Qualifications are Program enrolment for Basic, 700 SMILES for Sapphire, and 15,000 SMILES for Platinum
														Membership Tiers.
													</li>

												</ul>
											</p>

											<div>

												<p align="center">
													<img src="images/services/about_ff2.jpg">
													<br clear="ALL">

												</p>
											</div>

											<p class="text-center">
												<ul>

													<li>SMILES Members may apply for Members Status Upgrades based on Tier-based SMILES eligibility periods. Account SMILES earned within this period may be applied towards a Status Upgrade requests: 6 months for Basic Membership Tier, 1 year for
														Sapphire Membership Tier, and 2 years for Platinum Membership Tier.</li>

												</ul>

												<ul>

													<li>Membership status reviewed after every month end and update on 1st day of next month.

														<p align="center">
															<img src="images/services/about_ff1.jpg" width="383" height="166"> </p>

														When any member reaches SMILES Sapphire Status, an auto generated email has been send with address verification notification to his/her personal email address. Members need to verify his/her account information and confirm the mailing address. Incompletion
														may result; unsent SMILES Card and unable to enjoy the full benefits of SMILES program.

													</li>

												</ul>



												<ul>

													<li>SMILES Members may apply for SMILES Rewards based on accrued SMILES Miles. SMILES are valid for a period of 2 years from the date on which a Member undertakes the relevant Flight, and will expire (and be deducted from the Member's Account)
														at the end of its validity period.</li>

												</ul>

												<ul>

													<li>SMILES Membership Tier Status is valid for 1 year for Basic Membership Status, 2 years for Sapphire Membership Status, and 2 years for Platinum Membership Status. </li>

												</ul>

												<ul>

													<li>Unused, expired SMILES Miles, once deducted from the Member's Account, cannot be re-credited back to the Member&rsquo;s SMILES Account. It is the Member's responsibility to be aware of both the SMILES Miles in his or her Account and when
														the SMILES Miles will expire, using their on-line account.</li>

												</ul>

												<ul>

													<li>NOVOAIR SMILES FFP reserves the right to reverse or cancel any SMILES Miles credited to a Member incorrectly, or not in accordance with, or in breach of the Program Rules at any time.</li>

												</ul>
											</p>

											<h2>
												<strong>
													<a name="MembershipTiersBenefits"></a>
												</strong>Membership Tiers Benefits</h2>

											<ul>

												<li>SMILES Members will be entitled to the following Tier-based Membership Benefits :</li>

											</ul>

											<table class="table table-responsive table-striped">
												<thead>
													<tr>
														<th>Member Benefits &amp; Privileges</td>
															<th>Basic</th>
															<th>Sapphire</th>
															<th>Platinum</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Redeem NOVOAIR Rewards with SMILES Miles online</td>
														<td><i class="fa fa-check"></i></td>
														<td><i class="fa fa-check"></i></td>
														<td><i class="fa fa-check"></i></td>
													</tr>
													<tr>
														<td>Special member-only offers and travel packages by tier</td>
														<td><i class="fa fa-check"></i></td>
														<td><i class="fa fa-check"></i></td>
														<td><i class="fa fa-check"></i></td>
													</tr>
													<tr>
														<td>Personal preferences remembered</td>
														<td></td>
														<td><i class="fa fa-check"></i></td>
														<td><i class="fa fa-check"></i></td>
													</tr>
													<tr>
														<td>Waitlist priority</td>
														<td></td>
														<td><i class="fa fa-check"></i></td>
														<td><i class="fa fa-check"></i></td>
													</tr>
													<tr>
														<td>Bonus SMILES Miles when flying NOVOAIR</td>
														<td></td>
														<td><i class="fa fa-check"></i></td>
														<td><i class="fa fa-check"></i></td>
													</tr>
													<tr>
														<td>Priority service through our Sales Counters</td>
														<td></td>
														<td></td>
														<td><i class="fa fa-check"></i></td>
													</tr>
													<tr>
														<td>Priority check in and boarding</td>
														<td></td>
														<td></td>
														<td><i class="fa fa-check"></i></td>
													</tr>
													<tr>
														<td>Excess baggage allowances</td>
														<td></td>
														<td></td>
														<td><i class="fa fa-check"></i></td>
													</tr>
													<tr>
														<td>Priority baggage delivery</td>
														<td></td>
														<td></td>
														<td><i class="fa fa-check"></i></td>
													</tr>

												</tbody>
											</table>

											<p>
												<ul>

													<li>Additional baggage allowances are available to Platinum Members travelling on NOVOAIR Flights only. Platinum Members are entitled to an additional 5KG over the baggage allowance shown on the ticket. But subject to the flight load.</li>

												</ul>

												<ul>

													<li>The number of Tier Miles required for each Membership Tier and associated Tier benefits and privileges may be changed by NOVOAIR SMILES FFP at any time without prior notice. </li>

												</ul>
											</p>


											<h2>
												<strong>
													<a name="Rewards"></a>
												</strong>Rewards</h2>

											<p>

												<ul>

													<li>To claim a SMILES Reward, a Member must have accumulated the required amount of SMILES Miles in his or her Account. Members may redeem SMILES Miles for Rewards at any time while the SMILES Miles are valid, subject to availability and applicable
														conditions. When requesting a Reward, a Member may be requested to complete a Reward Authorization Form and supply a copy of his or her passport.</li>

												</ul>

												<ul>

													<li>SMILES Members can redeem their SMILES for Reward Flights only if their accrued Account SMILES are sufficient to meet the Reward Ticket qualifications as shown below:</li>

												</ul>

											</p>

											<table class="table table-responsive table-striped">
												<thead>
													<tr>
														<th colspan="2" class="text-center">SMILES REWARDS</td>
															<th class="text-center">Qualification</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<th colspan="2" class="text-center">One-Way Flights</th>
														<th class="text-center">Member Account SMILES</th>
													</tr>
													<tr>
														<td class="text-right">Dhaka to Jessore</td>
														<td class="text-left">Jessore to Dhaka</td>
														<td class="text-center">1,500</td>
													</tr>
													<tr>
														<td class="text-right">Dhaka to Sylhet</td>
														<td class="text-left">Sylhet to Dhaka</td>
														<td class="text-center">1,800</td>
													</tr>
													<tr>
														<td class="text-right">Dhaka to Chittagong</td>
														<td class="text-left">Chittagong to Dhaka</td>
														<td class="text-center">2,000</td>
													</tr>
													<tr>
														<td class="text-right">Dhaka to Cox&rsquo;s Bazar</td>
														<td class="text-left">Cox&rsquo;s Bazar to Dhaka</td>
														<td class="text-center">2,500</td>
													</tr>

												</tbody>
											</table>
											<p>

												<ul>

													<li>SMILES Members can book Reward Tickets via their SMILES Account on-line, or with a NOVOAIR Contact Centre or authorized Partner location. Open tickets will not be issued as Rewards. Outbound and return Flights must be booked and confirmed
														at the same time. Wait listing for Reward Flights are not permitted.</li>

												</ul>

												<ul>

													<li>Rewards do not have any cash value and neither SMILES Miles nor Rewards are convertible into cash.</li>

												</ul>

												<ul>

													<li>All taxes, duties and carrier imposed charges (including airport and/or government taxes) are the responsibility of the Member and must be paid on the time of ticketing.</li>

												</ul>

												<ul>

													<li>NOVOAIR SMILES FFP may be re-credited to a Members Account if a Reward ticket is completely unused, valid And refunded</li>

												</ul>

												<ul>

													<li>It is the responsibility of the Member to verify the availability of Reward travel in instances where a promotional Partner is providing joint-services with NOVOAIR. Reward travel may not be possible on certain joint services or codeshare
														services.</li>

												</ul>

												<ul>

													<li>The use of a Reward ticket may not entitle the Member to certain benefits which are associated with a full fare ticket.</li>

												</ul>

												<ul>

													<li>In the event of cancellation or major disruption to a Flight, a Member travelling on a Reward ticket will be transferred to the next NOVOAIR Flight based on priority status of membership</li>

												</ul>

												<ul>

													<li>A Member may not sell a Reward ticket or any other Reward for cash or other consideration and a Member may not purchase a Reward ticket or any other Reward from any third party. A Reward ticket that has been bought, sold or bartered by or
														to a Member may be cancelled or confiscated without prior notice in the sole discretion of NOVOAIR. </li>

												</ul>

												<ul>

													<li>A Photo Identify Card (Any ID Card) is required for travel.</li>

												</ul>

												<ul>

													<li>NOVOAIR may at any time without notice alter the number of SMILES Miles required to obtain a particular Reward, withdraw a Reward supplied or impose additional restrictions on a Reward or conditions of obtaining it.</li>

												</ul>

												<ul>

													<li> All Rewards are subject to availability. NOVOAIR or Partners may withdraw, replace or substitute Rewards at any time without any prior notice.

													</li>

												</ul>

											</p>

											<h2>
												<strong>
													<a name="PrivacyPolicy"></a>
												</strong>Privacy Policy</h2>

											<p>It is a condition of enrolment into the NOVOAIR SMILES FFP that a Member consents and authorizes NOVOAIR to collect, use and disclose information contained in the SMILES membership application form and other information, such as names; addresses;
												contact numbers; date of birth; and transaction details, including Miles accrual and Reward transactions relating to the SMILES FFP, for the purposes described below:</p>

											<ol type="I">

												<li>may be used to develop new services</li>

												<li>may be used for accounting and audit purposes</li>

												<li>may be used for marketing and market research purposes</li>

												<li>will be transferred to countries that may not have data protection laws</li>

												<li>will be retained and used by NOVOAIR and data processors to ensure the efficient running of the SMILES Program, including the crediting of Miles, the provision of membership statements and the Rewarding of membership levels</li>

												<li>may be disclosed as required by law, including disclosures to the police, immigration and customs authorities</li>

												<li>may be used by SMILES FFP to send you communications a
													<a name="_GoBack"></a>bout promotions, services, products and facilities offered by NOVOAIR</li>

												<li>may be disclosed to a Partner to assist that Partner in the planning and development of the SMILES FFP</li>

												<li>may be used by a Partner to send you separate communications about services, products and facilities offered by that Partner (unless the Member indicates otherwise).</li>

											</ol>

											<p>The consent given by a Member will continue in effect unless and until the Member withdraws the consent by notice in writing to a NOVOAIR Contact Centre. Withdrawal of consent may mean that certain services may no longer be provided to the
												Member, and also entitles SMILES FFP to terminate Membership immediately.</p>

											<p>&nbsp;</p>

											<p style="font-size:11px; color:#999;">NOVOAIR is solely responsible for the interpretation and application of the policies and procedures communicated in this guide and elsewhere to SMILES Miles members. All determinations by NOVOAIR shall be final and conclusive in each case.
												The SMILES Frequent Flyer program official rules, partners, special offers, blackout dates, awards and mileage levels, and service fees are subject to change with or without prior notice. NOVOAIR reserves the right to retroactively terminate
												the SMILES program or portions of the program at any time. NOVOAIR is not viable to compensate SMILES Members for failing to execute on FFP Rewards.
												<br> NOVOAIR reserves the right to audit any and all SMILES Miles accounts for compliance with the rules set forth in this guide at any time, without notice to the program member. In the event that the audit reveals discrepancies, penalties will
												apply, and the processing of award requests may be delayed until the discrepancies are resolved. NOVOAIR also reserves the right to disqualify any person from participation in the SMILES Miles program if, in the sole judgment of NOVOAIR, with
												or without cause.
												<br> Service by NOVOAIR to specific locations is subject to discontinuance with or without notice to the program members. Any SMILES promotional partner may discontinue or change its participation in the program with or without prior notice. NOVOAIR
												is not responsible for products and services provided by SMILES partners. SMILES Rewards may be subject to capacity constraints and therefore prone to blackout dates, service cancellations and other award limitations without prior notification.
												</p>
											<p align="center">
												<strong>FOR MORE DETAILS</strong>
											</p>

											<p align="center">
												<strong>Call 13603</strong>
												<br> Or
												<br> Email at <a href="mailto:smiles@novoair-bd.com">smiles@novoair-bd.com</a></p>

										</div>
										<!-- end 9 col -->
										<div class="col-sm-3 hidden-xs">
											<a href="Enroll Now" class="btn btn-block btn-info">
                				<i class="fa fa-user fa-5x"></i><br/>
                					Enroll Now!<br>Smiles Frequent Flyer
            					</a>

											<a href="tel:" class="btn btn-block btn-warning">
                				<i class="fa fa-phone fa-5x"></i><br/>
                					Contact <br>0176XXXXX
            					</a>

											<a href="mailto:smiles@novoair-bd.com" class="btn btn-block btn-warning">
                				<i class="fa fa-envelope fa-5x"></i><br/>
                					Email <br>smiles@novoair-bd.com
            					</a>
											<p>
												&nbsp;
											</p>
											<span class="text-muted">
												Quick Links
											</span>
											<ul>
												<li><a href="#SmilesMembership">Smiles Membership</a></li>
												<li><a href="#EarningMiles">Earning Miles</a></li>
												<li><a href="#MembershipTiers">Membership Tiers</a></li>
												<li><a href="#MembershipTiersBenefits">Membership Tiers Benefits</a></li>
												<li><a href="#Rewards">Rewards</a></li>
												<li><a href="#PrivacyPolicy">Privacy Policy</a></li>
											</ul>
										</div>
									</div>
									<!-- end row -->

									<div class="clearfix">&nbsp;</div>
									<br>
									<br>
									<!-- footer -->
									<?php	include_once('partials/footer.php'); ?>
										<!-- end footer -->
								</div>
								<!-- end page content -->
							</div>
						</div>
						<!-- end container -->
				</div>
				<!-- end slide wrapper -->

				<?php	include_once('partials/tail.php'); ?>

		</body>

	</html>
