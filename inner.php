<?php
	require_once('setting/config.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
 <?php
 	include_once('inc_top_includes.php');
 ?>
</head>
<body class="inner">

    <!-- top nav -->
			 <?php
                include_once('inc_topnav.php');
             ?>     
             
	<!-- end top nav -->  
    
    

<br>
<br>
<br>

<div class="container help-line">
    <div>
        <img src="images/help-line.png" class="hidden-xs">
        <img src="images/help-line2.png" class="visible-xs">
    </div>
</div>


<!-- main navigation -->
			 <?php
                include_once('inc_mainnav.php');
             ?>           
<!-- end main navigation -->





<div class="slide-wrapper">
<!-- inner page Carousel -->
 <?php
    include_once('inc_carousel_inner.php');
 ?>   
<!-- end inner page Carousel -->   


  <!-- content -->
    <div class="container">
    
    
    

	
    
    
    
    	<div class="body-container">
    
    
        
         <div class="breadcrumb">
        <div class="row">
          <div  class="col-sm-6">
            <h1><b>Services </b>&amp; Facilities</h1>
          </div>
          <div class="col-sm-6 text-right"> Services / Services </b>&amp; Facilities </div>
        </div>
      </div>     
        
      


        
        <div class="page-contents">
        

        
        
        
        
        
        

	




        <div class="row destination-icons-left">
        	

            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
	         <h2>Travel Safely, Comfortably & On-Time with NOVOAIR</h2>
             

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce congue erat sed magna egestas eu commodo tellus eleifend. Donec dui nisl, volutpat vitae hendrerit sit amet, egestas vel diam. Curabitur aliquam semper lobortis. Proin non nisl vitae massa porttitor tristique. Quisque ut lectus dolor. Vivamus vitae felis sit amet mauris lacinia dignissim. Phasellus laoreet consectetur dolor, pulvinar facilisis elit venenatis at. Vivamus vitae ante odio. Nam nisl purus, porttitor varius sodales eu, tincidunt at massa. Aenean et tellus est, sit amet vehicula tortor. Duis vestibulum accumsan augue rhoncus mollis. Donec est lorem, ultricies nec auctor vel, porta ac purus. Etiam ac tellus et lacus sodales congue non eu quam.


             
                    <ul>        
                     
                      <li>Your Safety, our  priority - Twin Jet Engine aircraft for a safer journey.</li>
                      <li>Your Comfort, our  responsibility – Leather Seat Advance Air-conditioning System to ensure utmost comfort.</li>
                      <li>Your Time is our Business - On-Time Departure</li>
                         <li>Family Friendly Airline –       Infant Free On-Board*</li>
                         <li>Online System,   easy &amp; secured payment system is just clicks away
                      <br>
                      
                      <ul style="margin-top:5px;">
                        <li>100% E-Ticketing</li>
                        <li>Bar Coded  Boarding Pass</li>
                        <li>Web Ticketing  Service</li>
                        <li>E-Payment</li>
                      </ul>
                         </li>
                    </ul>             
                    
               
                
            </div>
            
            <div class="first-col col-lg-3 col-md-3  hidden-sm hidden-xs ">
            	<div class="line-vert">

                    <a href="#"><img src="images/left-btn-locate-salse-office.jpg" 	class="img-responsive"></a>
                    <a href="#"><img src="images/left-btn-live-chat.jpg" 			class="img-responsive"></a>
                    <a href="#"><img src="images/left-btn-flight-schedule.jpg"		class="img-responsive"></a>
                    <a href="#"><img src="images/left-btn-todays-flights.jpg"		 class="img-responsive"></a>
                
                </div>
            </div>            
            
        </div>
		
        <div class="clearfix">&nbsp;</div>
        
        <div class="row hidden-lg hidden-md  destination-icons-bottom animate">
                    <div class="col-xs-3">
                        <a href="#"><img src="images/left-btn-locate-salse-office-2.jpg" 	class="img-responsive hidden-xs"><img src="images/icons/location.jpg" class="img-responsive hidden-sm"></a>
                    </div>
                    <div class="col-xs-3">
                        <a href="#"><img src="images/left-btn-live-chat.jpg" 			class="img-responsive hidden-xs"><img src="images/icons/chat.jpg" class="img-responsive hidden-sm"></a>
                    </div>
                    <div class="col-xs-3">
                        <a href="#"><img src="images/left-btn-flight-schedule.jpg"		class="img-responsive hidden-xs"><img src="images/icons/schedule.jpg" class="img-responsive hidden-sm"></a>
                    </div>
                    <div class="col-xs-3">
                        <a href="#"><img src="images/left-btn-todays-flights.jpg"		 class="img-responsive hidden-xs"><img src="images/icons/todays-flight.jpg" class="img-responsive hidden-sm"></a>
                    </div>
        </div>
<br>
<br>




<!-- footer -->
 <?php
 	include_once('inc_footer.php');
 ?>
<!-- end footer -->








                    
        </div>
        
        
        
    </div>
    </div>
    <!-- end content -->


</div>

<div style="clear:both"></div>







 <?php
 	include_once('inc_bottom_includes.php');
 ?>


   

</body>
</html>
