<?php
	require_once('setting/config.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
 <?php
 	include_once('inc_top_includes.php');
 ?>
</head>
<body class="inner">

    <!-- top nav -->
			 <?php
                include_once('inc_topnav.php');
             ?>     
             
	<!-- end top nav -->  
    
    

<br>
<br>
<br>

<div class="container help-line">
    <div>
        <img src="images/help-line.png" class="hidden-xs">
        <img src="images/help-line2.png" class="visible-xs">
    </div>
</div>


<!-- main navigation -->
			 <?php
                include_once('inc_mainnav.php');
             ?>           
<!-- end main navigation -->





<div class="slide-wrapper">
<!-- inner page Carousel -->
 <?php
    include_once('inc_carousel_inner.php');
 ?>   
<!-- end inner page Carousel -->   

  <!-- content -->
    <div class="container">
    
    	<div class="body-container">
    
    
        
         <div class="breadcrumb">
        <div class="row">
          <div  class="col-sm-6">
            <h1><b>Flight   </b> Schedule</h1>
          </div>
          <div class="col-sm-6 text-right"> Plan a Trip / Flight Schedule </div>
        </div>
      </div>     
        
      


        
        <div class="page-contents">
        

        
        
        
        
        
        

	




        <div class="row">
        	

            <div class="col-lg-12">
	         <h2>INTERNATIONAL</h2>
             
<br>
<br>
		
        <div class="row">
                <div class="col-lg-6" st  yle="background:red">
                
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table flight-schedule">
                <tr>
                  <th colspan="4" scope="col">DHAKA-YANGON</th>
                </tr>
                <tr>
                  <th width="20%">Flight No</th>
                  <th width="20%">Departure</th>
                  <th width="20%">Arrival</th>
                  <th width="40%">Days of Operation</th>
                </tr>
                <tr>
                  <td>VQ711</td>
                  <td>1130</td>
                  <td>1345</td>
                  <td>Fri, Sun, Tue</td>
                </tr>
            </table>
        
                </div>
                <div class="col-lg-6" st yle="background:blue">
                
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table flight-schedule">
                <tr>
                  <th colspan="4" scope="col">YANGON-DHAKA</th>
                </tr>
                <tr>
                  <th width="20%">Flight No</th>
                  <th width="20%">Departure</th>
                  <th width="20%">Arrival</th>
                  <th width="40%">Days of Operation</th>
                </tr>
                <tr>
                  <td>VQ712</td>
                  <td>1445</td>
                  <td>1600</td>
                  <td>Fri, Sun, Tue</td>
                </tr>

            </table>
                
                </div>
            </div>
			
			<h2>DOMESTIC</h2>

		&nbsp;
            <div class="row">
                <div class="col-lg-6" st  yle="background:red">
                
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table flight-schedule">
                        <tr>
                          <th colspan="4" scope="col">DHAKA-CHITTAGONG</th>
                        </tr>
                        <tr>
                          <th width="20%">Flight No</th>
                          <th width="20%">Departure</th>
                          <th width="20%">Arrival</th>
                          <th width="40%">Days of Operation</th>
                        </tr>
                        <tr>
                          <td>VQ901</td>
                          <td>0800</td>
                          <td>0845</td>
                          <td>Daily</td>
                        </tr>
                        <tr>
                          <td>VQ903</td>
                          <td>0900</td>
                          <td>0945</td>
                          <td>Daily except Fri &amp; Sat</td>
                        </tr>
                        <tr>
                          <td>VQ905</td>
                          <td>1030</td>
                          <td>1115</td>
                          <td>Daily</td>
                        </tr>
                        <tr>
                          <td>VQ909</td>
                          <td>1520</td>
                          <td>1605</td>
                          <td>Daily</td>
                        </tr>
                        <tr>
                          <td>VQ911</td>
                          <td>1830</td>
                          <td>1915</td>
                          <td>Daily</td>
                        </tr>
                        <tr>
                          <td>VQ913</td>
                          <td>1015</td>
                          <td>2100</td>
                          <td>Daily except Fri</td>
                        </tr>
                    </table>
        
                </div>
                <div class="col-lg-6" st yle="background:blue">
                
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table flight-schedule">
                <tr>
                  <th colspan="4" scope="col">CHITTAGONG-DHAKA</th>
                </tr>
                <tr>
                  <th width="20%">Flight No</th>
                  <th width="20%">Departure</th>
                  <th width="20%">Arrival</th>
                  <th width="40%">Days of Operation</th>
                </tr>
                <tr>
                  <td>VQ902</td>
                  <td>0915</td>
                  <td>1000</td>
                  <td>Daily</td>
                </tr>
                <tr>
                  <td>VQ904</td>
                  <td>1015</td>
                  <td>1100</td>
                  <td>Daily except Fri &amp; Sat</td>
                </tr>
                <tr>
                  <td>VQ906</td>
                  <td>1145</td>
                  <td>1230</td>
                  <td>Daily</td>
                </tr>
                <tr>
                  <td>VQ910</td>
                  <td>1635</td>
                  <td>1720</td>
                  <td>Daily</td>
                </tr>
                <tr>
                  <td>VQ912</td>
                  <td>1945</td>
                  <td>2030</td>
                  <td>Daily</td>
                </tr>
                <tr>
                  <td>VQ914</td>
                  <td>2130</td>
                  <td>2215</td>
                  <td>Daily except Fri</td>
                </tr>
            </table>
                
                </div>
            </div>
        
        
        &nbsp;
            <div class="row">
                <div class="col-lg-6" st  yle="background:red">
                
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table flight-schedule">
                <tr>
                  <th colspan="4" scope="col">DHAKA-COX'S BAZAR</th>
                </tr>
                <tr>
                  <th width="20%">Flight No</th>
                  <th width="20%">Departure</th>
                  <th width="20%">Arrival</th>
                  <th width="40%">Days of Operation</th>
                </tr>
                <tr>
                  <td>VQ931</td>
                  <td>1430</td>
                  <td>1525</td>
                  <td>Daily except Mon,Wed</td>
                </tr>
                <tr>
                  <td>VQ933</td>
                  <td>1130</td>
                  <td>1225</td>
                  <td>Daily</td>
                </tr>
            </table>
        
                </div>
                <div class="col-lg-6" st yle="background:blue">
                
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table flight-schedule">
                <tr>
                  <th colspan="4" scope="col">COX'S BAZAR-DHAKA</th>
                </tr>
                <tr>
                  <th width="20%">Flight No</th>
                  <th width="20%">Departure</th>
                  <th width="20%">Arrival</th>
                  <th width="40%">Days of Operation</th>
                </tr>
                <tr>
                  <td>VQ932</td>
                  <td>1555</td>
                  <td>1650</td>
                  <td>Daily except Mon,Wed</td>
                </tr>
                <tr>
                  <td>VQ934</td>
                  <td>1255</td>
                  <td>1350</td>
                  <td>Daily</td>
                </tr>
            </table>
                
                </div>
            </div>
        &nbsp;
            <div class="row">
                <div class="col-lg-6" st  yle="background:red">
                
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table flight-schedule">
                <tr>
                  <th colspan="4" scope="col">DHAKA-SYLHET</th>
                </tr>
                <tr>
                  <th width="20%">Flight No</th>
                  <th width="20%">Departure</th>
                  <th width="20%">Arrival</th>
                  <th width="40%">Days of Operation</th>
                </tr>
                <tr>
                  <td>VQ951</td>
                  <td>1300</td>
                  <td>1340</td>
                  <td>Daily</td>
                </tr>
            </table>
        
                </div>
                <div class="col-lg-6" st yle="background:blue">
                
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table flight-schedule">
                <tr>
                  <th colspan="4" scope="col">SYLHET-DHAKA</th>
                </tr>
                <tr>
                  <th width="20%">Flight No</th>
                  <th width="20%">Departure</th>
                  <th width="20%">Arrival</th>
                  <th width="40%">Days of Operation</th>
                </tr>
                <tr>
                  <td>VQ952</td>
                  <td>1410</td>
                  <td>1450</td>
                  <td>Daily</td>
                </tr>
            </table>
                
                </div>
            </div>
            
        &nbsp;
            <div class="row">
                <div class="col-lg-6" st  yle="background:red">
                
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table flight-schedule">
                <tr>
                  <th colspan="4" scope="col">DHAKA-JESSORE</th>
                </tr>
                <tr>
                  <th width="20%">Flight No</th>
                  <th width="20%">Departure</th>
                  <th width="20%">Arrival</th>
                  <th width="40%">Days of Operation</th>
                </tr>
                <tr>
                  <td>VQ943</td>
                  <td>1800</td>
                  <td>1835</td>
                  <td>Daily</td>
                </tr>
            </table>
        
                </div>
                <div class="col-lg-6" st yle="background:blue">
                
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table flight-schedule">
                <tr>
                  <th colspan="4" scope="col">JESSORE-DHAKA</th>
                </tr>
                <tr>
                  <th width="20%">Flight No</th>
                  <th width="20%">Departure</th>
                  <th width="20%">Arrival</th>
                  <th width="40%">Days of Operation</th>
                </tr>
                <tr>
                  <td>VQ944</td>
                  <td>1905</td>
                  <td>1940</td>
                  <td>Daily</td>
                </tr>
            </table>
                
                </div>
            </div>            

    <br>
<br>
                
            </div>
        
            
        </div>
		
        <div class="clearfix">&nbsp;</div>
        

                <!-- icon buttons on bottom -->
                 <?php
                    include_once('inc_inner_bottombar_iconbuttons.php');
                 ?>
                <!-- end icon buttons on bottom-->
<br>
<br>




<!-- footer -->
 <?php
 	include_once('inc_footer.php');
 ?>
<!-- end footer -->








                    
        </div>
        
        
        
    </div>
    </div>
    <!-- end content -->


</div>

<div style="clear:both"></div>







 <?php
 	include_once('inc_bottom_includes.php');
 ?>


   

</body>
</html>
