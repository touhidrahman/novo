<?php	require_once('../setting/config.php'); ?>
<!DOCTYPE html>
<html lang="en">

<?php	include_once('../inc.head.php'); ?>

	<body class="inner">

		<br>
		<br>
		<br>

		<div class="container help-line">
			<div>
				<img src="images/help-line.png" class="hidden-xs">
				<img src="images/help-line2.png" class="visible-xs">
			</div>
		</div>

		<!-- main navigation -->
		<?php include_once('../inc.mainnav2.php'); ?>
			<!-- end main navigation -->


			<div class="slide-wrapper">

				<?php	include_once('../inc.inner_carousel.php'); ?>


					<!-- content -->
					<div class="container">
						<div class="body-container">

							<div class="breadcrumb">
								<div class="row">
									<div class="col-sm-6">
										<h1><b>Charter A Flight </b></h1>
									</div>
									<div class="col-sm-6 text-right"> Services / Charter A Flight </div>
								</div>
							</div>

							<div class="page-contents">

								<div class="row">
									<div class="col-sm-8 col-xs-12 text-justify">
										<img src="images/services/charter.jpg" width="100%">
										NOVOAIR'S Aircraft on Charter Service, on any Domestic & international Destinations. Please Call 01795999111 or Email: charter@flynovoair.com for any query about Charter Services. <br><br>Demand More, Demand NOVOAIR.

										<h3><small>TERMS & CONDITIONS</small></h3>
											<ul>
											  <li>50 % Advance to confirm the Dates & Timing.</li>
											  <li>Rest 50% should be paid before 07 working days prior departure date.</li>
											  <li>Quoted rates are net to NOVOAIR.</li>
											  <li>Above rates are excluded of VAT and TAX.</li>
											  <li>Payment made through Cash or A/C Payee Cheque or Pay order in favor of NOVOAIR Limited.</li>
											  <li>Flight timing may change for any emergency/ weather/ any other natural calamity.</li>
											</ul>

									</div>
									<div class="col-sm-4 col-xs-12">
										<div class="blockquote-box blockquote-warning clearfix">
			                <div class="square pull-left">
			                    <span class="glyphicon glyphicon-earphone glyphicon-lg"></span>
			                </div>
		                <h3 class="novoblue">CALL NOW! <br>
		                    01795999111</h3>
            				</div>

										<div class="well sq-corner">
											<h4>Contact Point</h4>
											NOVOAIR Limited
											House-50, Road-11, Block-F
											Banani, Dhaka-1213, Bangladesh<br>
											Hotline: 13603<br>
											Cell: <a href="tel:+8801795999111">+8801795999111</a><br>
											Email: <a href="mailto:charter@flynovoair.com">charter@flynovoair.com</a>

										</div>
									</div>
								</div>


								<div class="clearfix">&nbsp;</div>
								<br>
								<br>
								<!-- footer -->
								<?php	include_once('../inc.footer.php'); ?>
								<!-- end footer -->
							</div>
							<!-- end page content -->
						</div>
					</div>
					<!-- end container -->
			</div>
			<!-- end slide wrapper -->

			<?php	include_once('../inc.tail.php'); ?>

	</body>

</html>
