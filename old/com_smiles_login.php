<script language=javascript src="js/plugins/ff_login/airline.js"></script>
<script language=javascript src="js/plugins/ff_login/jquery.alerts.js"></script>
<script language=javascript type="text/javascript">
<!--

    

function do_login()
{

	if (validation())
	{
		document.frm_Login.login_action.value = "dologin";
		document.frm_Login.submit();
	}
}
function forgot_password()
{
	jPrompt('Enter the <b>e-mail address</b> associated with your Frequent Flyer account and your password will be emailed to you.', '', 'Forgot Password', function(email) {
	    if(email && emailCheck(email))
		{
			document.frm_Login.email_password.value = email;
			document.frm_Login.login_action.value = "forgotpassword";
			document.frm_Login.submit();
		}
		else
			return;
	});

}
function validation()
{
	var errorcount = 0;
	var msg = "";
	msg += "You must enter correct information into the following fields for this form to be submitted:<br /><br />";	
	var err_login;
	var err_password;
	
	document.frm_Login.login.value = trim_id(document.frm_Login.login.value);
	
	if (!isNumeric(document.frm_Login.login.value) || document.frm_Login.login.value == "" || document.frm_Login.login.value.length != 8)
	{
 		msg += "Frequent Flyer Member ID<br /><br />";
		err_login = true;
		errorcount++;
	}
	if (document.frm_Login.password.value == "")
	{
 		msg += "Password<br />";
		err_password = true;
		errorcount++;
	}
		
	msg += "";
	
	if (errorcount > 0)
		//alert(msg);
		
		//$(document).ready(function(e) {
			$.colorbox({
			html: '<p class="title" style="text-align:center;font-size:12px;padding:10px;">'+ msg + '</p>',
				width: "400px;"
			});			
		//});

	if (err_login){document.frm_Login.login.focus();}
	else if (err_password){document.frm_Login.password.focus();}

	return (errorcount == 0);
}
//will strip out all non-numeric characters from a string
function trim_id(word)
{
	var newvalue = "";
	for (i=0; i<word.length; i++ )
	{
		if (isNumeric(word.charAt(i)))
			newvalue = newvalue + word.charAt(i);
	}
	return newvalue
}
function setfocus()
{
    if ( document.frm_Login.login.value == '' )
		document.frm_Login.login.focus();
}
//-->




</script>
<script language=javascript type="text/javascript">
<!--
var isNetscape = false;
var isIE = false;
var isWhoKnows = false;

//This determines which browser the user is using
if (parseInt(navigator.appVersion) >= 4)
{
	if(navigator.appName == "Netscape" || navigator.appName == "Opera")
	{
		isNetscape = true;
	}
	else if (navigator.appName == "Microsoft Internet Explorer")
	{
		isIE = true;
	}
	else
	{
		isWhoKnows = true;
	}
}
	
// this captures the events of the user for NetScape 4
if(document.captureEvents && Event.KEYUP)
{
	document.captureEvents(Event.KEYUP);
}
// this is for all other browsers
document.onkeyup = handleEnterKey

/*function handleEnterKey(evt)
{
	var keyCode;
	keyCode = getEventKey(evt);
	if (keyCode == 13) 
	{
		do_login();
	}
}*/
function benefits_popup()
{
	var strFeatures;
	var nWinTop;
	var nWinLeft;
	// first calculate a central position for the window
	nWinTop = (screen.availHeight - 238) / 2;
	if (nWinTop < 0) {
	  nWinTop = 0;
	}
	nWinLeft = (screen.availWidth - 515) / 2;
	if (nWinLeft < 0) {
	  nWinLeft = 0;
	}
	// set the width and height and any other features required or not required
	strFeatures = "width=515,height=238,top=" + nWinTop + ",left=" + nWinLeft + ",location=0,menubar=0,toolbar=0,resizable=0";
	// popup window
	popupWindow = window.open("benefits_popup.asp", "popupFS", strFeatures); 
	// focus to make sure it is at the front
	popupWindow.focus();
} 
//-->
</script>
<l ink href="css/bootstrap.min.css" rel="stylesheet" type="text/css">

<form name="frm_Login" action="http://secure.flynovoair.com/rewards/default.asp" method="post">
  
  <div class="smiles">
    <input type="hidden" name="login_action" value="">
    <input type="hidden" name="email_password" value="">
    <div class="smiles_logo"> <img src="images/logo-novoair-smiles.png" width="150" class="center-block"> </div>
    <div class="row tfest"> 
    	<div  class="col-lg-12 col-md-12  col-sm-4 fo rm-group">
          <label for="username">SMILE ID</label>
          <input type="text" onFocus="this.select()" name="login" value="" maxlength="8">
        </div>
        <div class="col-lg-12 col-md-12 col-sm-4 fo rm-group">
          <label for="password">PASSWORD</label>
          <input type="password" onFocus="this.select()" name="password" value="" maxlength="6">
        </div> 
        <div class="col-lg-12 col-md-12 col-sm-4 fo rm-group">
          <label class="visible-sm">&nbsp;</label>
          <input type="submit" class="btn btn-warning" name="cmdLogin" id="cmdLogin" onclick="javascript:do_login();return false;" value="Login">
          <input type="button" class="btn btn-info" name="join" id="join" value="Join" onclick="location.href='http://secure.flynovoair.com/rewards/enroll.asp'">
        </div> 
      </div>
  </div>
  
</form>
