<?php	require_once('setting/config.php'); ?>
<!DOCTYPE html>
<html lang="en">

<?php	include_once('partials/head.php'); ?>

	<body class="inner">

		<br>
		<br>
		<br>

		<div class="container help-line">
			<div>
				<img src="images/help-line.png" class="hidden-xs">
				<img src="images/help-line2.png" class="visible-xs">
			</div>
		</div>

		<!-- main navigation -->
		<?php include_once('partials/mainnav2.php'); ?>
			<!-- end main navigation -->


			<div class="slide-wrapper">

				<?php	include_once('partials/inner_carousel.php'); ?>


					<!-- content -->
					<div class="container">
						<div class="body-container">

							<div class="breadcrumb">
								<div class="row">
									<div class="col-sm-6">
										<h1>Terms & Conditions</h1>
									</div>
									<div class="col-sm-6 text-right"> About Us / Terms & Conditions </div>
								</div>
							</div>

							<div class="page-contents">

								<h2>Tickets Terms and Conditions</h2>

								<br>
								<br>
								<ul>
								<li>	As  used in this contract &quot;ticket &ldquo;means this passenger ticket and baggage  check or this itinerary/receipt if applicable, in the case of an electronic  ticket of which these conditions and the notices form part,  &quot;carriage&quot; is equivalent &quot;transportation&quot;,  &quot;carrier&quot; means all air carriers that carry or undertake to carry the  passenger or his baggage hereunder or perform any other service incidental to  such air carriage, &quot;electronic ticket&quot; means the Itinerary/Receipt  issued by or on behalf of Carrier, the Electronic Coupons and, if applicable, a  boarding document. &ldquo;We&rdquo; or &ldquo;us&quot; means NOVOAIR. &quot;Warsaw  Convention&quot; means the Convention for the Unification of Certain Rules  Relating to International Carriage by Air signed at Warsaw, 12th October 1929,  or that Convention as amended at The Hague, 28th September 1955, whichever may  be applicable.  	<p>&nbsp;</p></li>
								<li>Baggage  that has been checked during boarding will be delivered to the bearer of the  baggage check. If your baggage is damaged or lost, you must report in writing  to us immediately on arrival. No complaint will be deemed valid otherwise.<p>&nbsp;</p></li>
								<li>This  ticket is good for carriage for 90 days/mentioned in the ticket from the date  of issue, in carrier's tariffs conditions of carriage, or related regulations. The  fare for carriage hereunder is subject to change prior to commencement of  carriage. Carrier may refuse transportation if the applicable fare has not been  paid.    <p>&nbsp;</p>    </li>
								<li>We  undertake to ensure our best efforts to carry you and your baggage with  reasonable dispatch. Times shown in timetables or elsewhere are not guaranteed  and form no part of this contract. We may without notice substitute alternate  carriers or aircraft, and may alter or omit stopping places shown on the ticket  in case of necessity. Schedules are subject to change without any prior notice.  In case of delay to your journey by 30 minutes or more, full fare will be  refunded.<p>&nbsp;</p> </li>
								<li>You  are solely responsible for complying with all Government travel requirements to  undertake such transportation. You shall arrive at the airport by the time  fixed by us (check-in counter will be closed 15 minutes before flight  departure) to complete departure formalities. We would deny to aboard you if  you fail to report/finish check-in within the time mentioned above.<p>&nbsp;</p>  </li>
								<li>None  of our agents, employees or representatives has the authority to alter, modify  or waive any provision contained on the ticket and the conditions of this  contract of carriage.<p>&nbsp;</p></li>
								<li>Liability  of loss, delay or damage to baggage is strictly limited for domestic journey,  unless a higher value is declared in advance and additional charges are paid.  The applicable liability is BDT 120 per kilo for checked and unchecked baggage.  We shall not be liable for any baggage, which is improperly or inadequately  packed. We assume no liability for fragile, valuable or perishable articles.<p>&nbsp;</p></li>
								<li><strong>BAGGAGE</strong>

								<p>Your cabin baggage may be  weighed and measured and if necessary, charged for in accordance with valid  tariffs. You may carry on board the articles listed below free of charge over  and above checked and unchecked baggage, which is improperly or inadequately  packed. We assume no liability for fragile, valuable or perishable articles.</p>

								<ul>
								<li>A lady's handbag, pocket book or purse, which is  appropriate to normal travelling dress and is not being used as a container for  the transportation of articles which would otherwise be regarded as baggage</li>
								<li>An overcoat</li>
								<li>An umbrella or walking stick</li>
								<li>A laptop or notebook computer or a small camera</li>
								<li>A reasonable amount of reading material for the  flight</li>
								<li>Infants&rsquo; food for consumption in flight and  infant' carrying basket</li>
								<li>A fully collapsible wheelchair and/or a pair of  crutches and/or other prosthetic devices for the passenger's use provided the  passenger is dependent upon them will be carried only in the luggage hold</li>
								<li>Infant's carrying baskets or wheelchairs may be  used until boarding the aircraft, then will be stowed in the aircraft hold</li>
								<li>Infant&rsquo;s stroller provided the infant is on  board the aircraft will go in the luggage hold</li>
								</ul>
								<p>&nbsp;</p>
								</li>


								<li><strong>RESTRICTED AND DANGEROUS ARTICLES IN BAGGAGE</strong>
								<p>For safety reason, dangerous  goods as defined in the international air transport association (IATA)  dangerous goods regulations such as those listed below shall not be carried as,  within, or part of your baggage:</p>

								<ul>
								<li>Briefcase and attach cases with installed alarm  or pyrotechnic devices. Explosives, munitions, fireworks, flares, firearm,  handguns or any other weapons. </li>
								<li>Gases (flammable, non-flammable and poisonous)  such as camping gas. </li>
								<li>Flammable liquids such as lighter or heating  fuels.</li>
								<li>Flammable solids, such as matches and articles  which are easily ignited substances liable to spontaneous combustion;  substances which emit flammable gases on contact with water.</li>
								<li>Oxidizing substances (such as bleaching powder  and peroxides) poisonous (toxic) and infectious substances</li>
								<li>Radioactive materials.</li>
								<li>Corrosives (such as mercury, acids. Alkalis and  wet cell batteries).</li>
								<li>Magnetized materials and miscellaneous dangerous  goods as listed in the international air transport association dangerous goods  regulations</li>
								</ul>
								<p>&nbsp;</p>
								</li>

								<li><strong>GENERAL INFORMATION</strong>
								<p>Remember to lock your  baggage to prevent it from falling open and to help prevent pilferage of its  contents. The carrier will not be liable for any loss, damage to or delay in  the delivery of fragile or perishable articles, money, jewelry, electronics  articles, negotiable papers, securities or other identification documents or  samples or medicines or drugs or any other valuable items which are included in  the passenger&lsquo;s baggage whether with or without our knowledge.</p>
								<p>&nbsp;</p>
								</li>

								<li><strong>IDENTIFICATION OF PASSENGERS</strong>
								<p>You may be required to  produce appropriate identification at the time of check in. Appropriate photo  identification required to avail any special discount/services.</p>
								<p>&nbsp;</p>
								</li>

								<li><strong>IMPORTANT CHANGES OF ITINERARY</strong>
								<p>We do not require our  passengers to reconfirm their onward or return journey on our services unless  we otherwise specified. However, please Contact your agent or our  reservation/ticketing office if you wish to change your travel plans.</p>
								<p>&nbsp;</p>
								</li>
								<li><strong>IMPORTANT NOTICE REGARDING UNAUTHORIZE  TICKETS</strong>
								<p>NOVOAIR will not recognize  for purposes of carriage any ticket purchased from or resold to any source  other than NOVOAIR or its authorized travel agents. Passengers should carefully  examine their tickets. Particularly the conditions of contract and notices  contained therein.</p>
								<p>&nbsp;</p>
								</li>
								<li><strong>REFUNDS</strong>
								<p>NOVOAIR reserves the right  to make a refund only to the person named in the ticket or to the person who  originally paid for the ticket. Refund will not be entertained if any claim  done after the date of ticket expiry. Ticket issued through any agent must be processed  refund by the issuing agent only.</p>
								<p>&nbsp;</p>
								</li>
								<li><strong>IMPORTANT LABEL YOUR BAG</strong>
								<p>For the purpose of easy  identification, please label all baggage inside and outside with your name and  address.</p>
								<p>&nbsp;</p>
								</li>

								<li><strong>RIGHT TO REFUSE CARRIAGE</strong>
								<p>We may refuse to carry you  and/or your baggage from a flight, if this is determined to be necessary or  appropriate for safety reasons, or for the comfort and convenience of  passengers, You or any other passenger will also be refused carriage or removed  from a flight, if your or their behavior is such as to threaten safety, good  order, or discipline on board the aircraft, or to cause discomfort,  inconvenience or annoyance to passengers or crew members.</p>
								<p>&nbsp;</p>
								</li>

								<li><strong>NOTICE OF GOVERNMENT IMPOSED TAXES, FEES AND  SURCHARGES</strong>
								<p>The price of this ticket may  include taxes. fees and charges which are imposed  on air transportation by the government,  concern authorities and the carrier These taxes, fees and charges which may  represent as significant portion of the cost of air travel, are either included  in the fare or shown separately in the &quot;TAX&quot; box(es) of this ticket.  You may also be required to pay taxes, fees and charges which were not  collected at the time of issuance.</p>
								<p>&nbsp;</p>
								</li>

								<li><strong>WEATHER ADVISORY</strong>

								<p>At times flights are  disrupted due to weather conditions that are beyond control. We always try our  level best to ensure that our customers do not suffer waiting for their flights  at the airports due to any flight disruption and we try to inform them in  advance but at times it is not possible to reach customers due to last minute  constrains. As such we would like to recommend to our valued customers to call  flight information prior to proceeding for the airport.</p>
								<p>&nbsp;</p>
								</li>
								</ul>
								<br>
								<br>




								<div class="clearfix">&nbsp;</div>
								<br>
								<br>
								<!-- footer -->
								<?php	include_once('partials/footer.php'); ?>
								<!-- end footer -->
							</div>
							<!-- end page content -->
						</div>
					</div>
					<!-- end container -->
			</div>
			<!-- end slide wrapper -->

			<?php	include_once('partials/tail.php'); ?>

	</body>

</html>
