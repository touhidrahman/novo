<?php	require_once('setting/config.php'); ?>
<!DOCTYPE html>
<html lang="en">

<?php	include_once('partials/head.php'); ?>

	<body class="inner">

		<br>
		<br>
		<br>

		<div class="container help-line">
			<div>
				<img src="images/help-line.png" class="hidden-xs">
				<img src="images/help-line2.png" class="visible-xs">
			</div>
		</div>

		<!-- main navigation -->
		<?php include_once('partials/mainnav2.php'); ?>
			<!-- end main navigation -->


			<div class="slide-wrapper">

				<?php	include_once('partials/inner_carousel.php'); ?>


					<!-- content -->
					<div class="container">
						<div class="body-container">

							<div class="breadcrumb">
								<div class="row">
									<div class="col-sm-6">
										<h1><b>Group  </b> Booking</h1>
									</div>
									<div class="col-sm-6 text-right"> Plan a Trip / Group Booking </div>
								</div>
							</div>

							<div class="page-contents">

								<div class="row">
									<div class="col-sm-8 col-xs-12 text-justify">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
									</div>
									<div class="col-sm-4 col-xs-12">
										<div class="blockquote-box blockquote-warning clearfix">
			                <div class="square pull-left">
			                    <span class="glyphicon glyphicon-earphone glyphicon-lg"></span>
			                </div>
		                <h3 class="novoblue">CALL NOW! <br>
		                    0176XXXXXX</h3>
            				</div>
									</div>
								</div>


								<div class="clearfix">&nbsp;</div>
								<br>
								<br>
								<!-- footer -->
								<?php	include_once('partials/footer.php'); ?>
								<!-- end footer -->
							</div>
							<!-- end page content -->
						</div>
					</div>
					<!-- end container -->
			</div>
			<!-- end slide wrapper -->

			<?php	include_once('partials/tail.php'); ?>

	</body>

</html>
