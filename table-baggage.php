<?php
	require_once('setting/config.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php
 	include_once('inc_top_includes.php');
 ?>
</head>
<body class="inner">
<br>
<br>
<br>

<div class="container">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table baggage">
      <tbody>
        <tr>
          <th scope="col">PASSENGER TYPE</th>
          <th scope="col">CHECKED BAGGAGE</th>
          <th scope="col">CABIN BAGGAGE</th>
          <th scope="col">EXCESS BAGGAGE</th>
        </tr>
        <tr>
          <td><strong>Adult</strong></td>
          <td>20 Kg<br>
            Maximum 2 pc per person<br>
            Dimension per pc: <br>
          40 x 60 x 100 cm (15x24x40 in)</td>
          <td>7 Kg<br>
            1 pc per person<br>
            Dimension per pc: <br>
          15 x 22 x 35 cm (6 x 8 x 14 in)</td>
          <td rowspan="3">TK 100 per Kg</td>
        </tr>
        <tr>
          <td><strong>Child</strong></td>
          <td>20 Kg<br>
            Maximum 2 pc per person<br>
            Dimension per pc: <br>
          40 x 60 x 100 cm (15x24x40in)</td>
          <td>7 Kg<br>
            1 pc per person<br>
            Dimension per pc: <br>
          15 x 22 x 35 cm (6 x 8 x 14 in)</td>
        </tr>
        <tr>
          <td><strong>Infant</strong></td>
          <td>10 Kg<br>
            Dimension: <br>
          40 x 60 x 100 cm (15x24x40 in)</td>
          <td>Carryon Bag<br>
            Dimension: <br>
          15 x 22 x 35 cm (6 x8x14 in)</td>
        </tr>
      </tbody>
    </table>
</div>
<?php
 	include_once('inc_bottom_includes.php');
 ?>
</body>
</html>
