<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<?php
// Include the CKEditor class.
include("ckeditor/ckeditor.php");

// Create a class instance.
$CKEditor = new CKEditor();

// Path to the CKEditor directory.
$CKEditor->basePath = '/admin/ckeditor-integrated/ckeditor/';

// Replace all textarea elements with CKEditor.
$CKEditor->replaceAll();
?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<textarea class="ckeditor" name="txt1" rows="20" cols="20"></textarea><br>
<br>
<br>
<textarea class="ckeditor" name="txt2" rows="20" cols="20"></textarea><br>

</body>
</html>