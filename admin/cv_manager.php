<?php
		session_start();
		
	require_once('../setting/config.php');
	require_once('../rak_framework/connection.php');
	require_once('../rak_framework/session_career.php');	
	require_once('../rak_framework/fetch.php');
	require_once('../rak_framework/delete.php');	
	require_once('../rak_framework/listgrabber.php');
	require_once('../rak_framework/edit.php');	
	require_once('../rak_framework/insert.php');	
	require_once('../rak_framework/misfuncs.php');			
	
	$mainMenu = 'cv';
	$subMenu = 'CV Manager';
	
	if(!$list)
	{
		$list = 'unapproved';
	}




if(!$is_approved && !$is_validated)
{
	$is_approved = 'yes';
	$is_validated = 'yes';
}


if(!$is_validated && $is_approved)
{
	$is_validated = 'yes';
}

if($is_validated && !$is_approved)
{
	$is_approved = '';
	$is_validated = 'no';
}


	
?>	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome to RakAdminPanel</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/fonts/font-face.css" rel="stylesheet" type="text/css" />

<!-- jquery by raihan -->
<script language="javascript" src="js/common.js"></script>
<script language="javascript" src="js/jquery.min.js"></script>
<script language="javascript">

$(document).ready(function(){

	//alert($(".bg_holder").height());
	
	//$(".site_title").text($(".bg_holder").height())
	$("body").css('background','transparent url(images/bg2.png) repeat-x 0 '+$(".bg_holder").height()+'px');
	
	
	
		$("#postaction").click(function(){

			$("#msg").text("");
			
			if($("#name").val() == '')
			{
				$("#msg").text("Required  fields * must be filled!");
				return false;
			}
		})

	
	

});

</script>

</head>

<body>

<div class="bg_holder">
    <div class="siteWidth">



<div id="col_1">
        <!-- left column -->
    
    
            <div class="logo_bg" onclick="location.href='index.php'">
               <span class="logo">&nbsp;</span>
            </div>
            
            <div class="leftmenu_title">
            	Main Menu
            </div>
            
            <div class="leftMenu">        
				<?php 
				
				include_once('inc_menu.php');?>
            </div>
            
            
        
        
        
        <!-- end left column -->
        </div>

        
        <div id="col_2">
        
        
        
            <div class="banner">
                
            </div>
            
            
            
            
            
            
			<div id="col_2_2">
                <div class="submenu_title">
                    SUB MENU
                </div>
                <!-- if sub menu exist -->
                <!--div class="submenu" style="background:#2D4856 url(images/rakadmin.png) no-repeat;height:520px;"  align="center"-->
                <!-- end if sub menu exist -->
                
                <div class="submenu" st yle="background:transparent url(images/rakadmin.png) no-repeat;height:520px;"  align="center">
                
                    <div class="subMenu" >
                    
                    	<?php 
						
						include_once('inc_company_menu.php');?>
                    </div>                	
                </div>
                
            </div>            
    
    		<div id="col_2_1" st yle="width:100%; border:0px solid #CCCCCC; background-color:#000099;" >
               <div class="site_title">
                     <?=$subMenu?>
               </div>
               
               <div class="home_body_content">
               
                <div class="clild_menu">
                	<a href="#"><?=$subMenu?></a>
                    <div class="clear"></div>
                </div>
                
                



				<?php


			
					if($click_action == 'edit'){
                    	include_once('inc_company_edit.php');
					}
					else if($click_action == 'detail'){
                    	include_once('inc_company_detail.php');
					}
					else{
						include_once('inc_company_list.php');
						}			
			
                ?>                
                <!--
                
                <div class="grid">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <th>Material</th>
                        <th>Courtesy</th>
                        <th>spec</th>
                        <th>Qtn</th>
                        <th>Unit</th>
                        <th>Price(Tk)</th>
                        <th class="action">Actions</th>
                      </tr>
                      <tr>
                        <td>Raihan</td>
                        <td>raihan@rakplanet.com</td>
                        <td>28/06/2012</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>
                            <div class="action">
                                <span><a href="#" class="detail"></a></span>
                                <span><a href="#" class="edit"></a></span>
                                <span><a href="#" class="delete"></a></span>
                            </div> 
                        </td>
                      </tr>
                      <tr class="alt">
                        <td>Raihan</td>
                        <td>raihan@rakplanet.com</td>
                        <td>28/06/2012</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>
                            <div class="action">
                                 <span><a href="#" class="detail"></a></span> 
                                 <span><a href="#" class="edit"></a></span> 
                                 <span><a href="#" class="delete"></a></span> 
                            </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Raihan</td>
                        <td>raihan@rakplanet.com</td>
                        <td>28/06/2012</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><div class="action"> <span><a href="#" class="detail"></a></span> <span><a href="#" class="edit"></a></span> <span><a href="#" class="delete"></a></span> </div></td>
                      </tr>
                      <tr class="alt">
                        <td>Raihan</td>
                        <td>raihan@rakplanet.com</td>
                        <td>28/06/2012</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><div class="action"> <span><a href="#" class="detail"></a></span> <span><a href="#" class="edit"></a></span> <span><a href="#" class="delete"></a></span> </div></td>
                      </tr>
                    </table>
                </div>
                
                -->
                
                &nbsp;
                
                
                

                <br />
<br />
<br />
<div class="clear"></div>
               </div>
            
        	    <div class="clear"></div>
            </div>
            
                 
    <div class="clear"></div>        
        </div>
   <div class="clear"></div> 
    </div>
<div class="clear"></div>
</div>

<div class="clear"></div>
<!-- footer -->
<div id="footer" class="siteWidth">
	
<?php
	include_once('inc_footer.php');
	
	
	
?>


</div>

<!-- end footer -->
</body>
</html>
