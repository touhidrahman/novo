<?php
		session_start();
		
	require_once('../setting/config.php');
	require_once('../rak_framework/connection.php');
	require_once('../rak_framework/session_career.php');	
	require_once('../rak_framework/fetch.php');
	require_once('../rak_framework/delete.php');	
	require_once('../rak_framework/listgrabber.php');
	require_once('../rak_framework/edit.php');	
	require_once('../rak_framework/insert.php');	
	require_once('../rak_framework/misfuncs.php');			
	
	$mainMenu = 'job';
	$subMenu = 'Job Manager';
?>	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$siteTitle?></title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/fonts/font-face.css" rel="stylesheet" type="text/css" />

<!-- jquery by raihan -->
<script language="javascript" src="js/common.js"></script>
<script language="javascript" src="js/jquery.min.js"></script>
<script language="javascript">

$(document).ready(function(){

	//alert($(".bg_holder").height());
	
	//$(".site_title").text($(".bg_holder").height())
	$("body").css('background','transparent url(images/bg2.png) repeat-x 0 '+$(".bg_holder").height()+'px');
	
	
	
		$("#postaction").click(function(){

			$("#msg").text("");
			
			
			
			if(	$("#link_title").val() == '' || 	$("#link_address").val() == '')
			{
				$("#msg").text("Required  fields * must be filled!");
				return false;
			}
		})

	
	
	
	

});




      function performAjaxSubmission() {
	  
	  //alert('db_field: '+ $(this).attr("db_field")+',  db_value: '+ $(this).val() +',  db_id: '+ $(this).attr("db_id"));
	  
	  if($(this).attr("chk_required") && !$(this).val())
	  {
		  //alert($(this).attr("chk_required"));
		  $("#msg").html("This is a required field, pleasse don't leave it blank")
		  $(this).focus();
		  return false;
	  }
	  
	  
	  
	  
        $.ajax({
          url: '../rak_framework_jqajax/update.php',
          method: 'POST',
          data:{
            action: 'save',
            db_field: $(this).attr("db_field"),
            db_value: $(this).val(),
			db_where_field: $(this).attr("db_where_field"),
			db_where_value: $(this).attr("db_where_value"),
			db_table: 'material_market',
			success_msg: 'Material info updated successfully'
			
          },
          success: function(response) {
          //alert(response);
		  

		 
		$("#msg").fadeIn(10);
		
		$("#msg").html("<span style=color:#8dff4f;>"+response+"</span>").delay(3000).fadeOut(500);
//		$("#msg").clearQueue();
		  
		  
		  


		 // $('#msg').slideUp(300).delay(1500).fadeOut(400);
		   
          }
        });
//        alert($(this).attr("title"))
		$("#msg").html("Please wait...")
		$(this).attr('title',$(this).val());
		
		return false; // <--- important, prevents the link's href (hash in this example) from executing.
		

		//$(this).attr('title',$(this).val());
		
		
		
      }
	  
      jQuery(document).ready(function() {
       // $(".actionCaller").change(performAjaxSubmission);
		
      });	  

</script>

</head>

<body>

<div class="bg_holder">
    <div class="siteWidth">



<div id="col_1">
        <!-- left column -->
    
    
            <div class="logo_bg" onclick="location.href='index.php'">
               <span class="logo">&nbsp;</span>
            </div>
            
            <div class="leftmenu_title">
            	Main Menu
            </div>
            
            <div class="leftMenu">        
				<?php include_once('inc_menu.php');?>
            </div>
            
            
        
        
        
        <!-- end left column -->
        </div>

        
        <div id="col_2">
        
        
        
            <div class="banner">
                
            </div>
            
            
            
            
            
            
			<div id="col_2_2">
                <div class="submenu_title">
                   <!-- SUB MENU-->
                </div>
                <!-- if sub menu exist -->
                <!--div class="submenu" style="background:#2D4856 url(images/rakadmin.png) no-repeat;height:520px;"  align="center"-->
                <!-- end if sub menu exist -->
                
                <div class="submenu" style="background:transparent url(images/rakadmin.png) no-repeat;height:520px;"  align="center">
                
                    <!--div class="subMenu" >        
						
                    </div-->                	
                </div>
                
            </div>            
    		
    		<div id="col_2_1" st yle="width:100%; border:0px solid #CCCCCC; background-color:#000099;" >
               <div class="site_title">
                    <?=$subMenu?>
               </div>
               
               <div class="home_body_content">
               
                <div class="clild_menu">
                	<a href="#"><?=$subMenu?></a>
                    <div class="clear"></div>
                </div>
                
                



				<?php
					if($click_action == 'insert'){
                    	include_once('inc_job_edit.php');
					} else if($click_action == 'edit'){
                    	include_once('inc_job_edit.php');
					}
					else if($click_action == 'detail'){
                    	include_once('inc_job_detail.php');
					}
					else{
						include_once('inc_job_list.php');
						}
					
                ?>                
                
                
                &nbsp;
                
                
                

                <br />
<br />
<br />
<div class="clear"></div>
               </div>
            
        	    <div class="clear"></div>
            </div>
            
                 
    <div class="clear"></div>        
        </div>
   <div class="clear"></div> 
    </div>
<div class="clear"></div>
</div>

<div class="clear"></div>
<!-- footer -->
<div id="footer" class="siteWidth">
	
<?php
	include_once('inc_footer.php');
?>


</div>

<!-- end footer -->
</body>
</html>
