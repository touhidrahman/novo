<?php
	session_start();
		
	require_once('../../setting/config.php');
	require_once('../../rak_framework/connection.php');
	require_once('../../rak_framework/session_safety.php');	
	require_once('../../rak_framework/fetch.php');
	require_once('../../rak_framework/delete.php');	
	require_once('../../rak_framework/listgrabber.php');
	require_once('../../rak_framework/edit.php');	
	require_once('../../rak_framework/insert.php');	
	require_once('../../rak_framework/misfuncs.php');			
	
	
	$managerName = 'department';
	$table = 'department';




	
?>	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome to RakAdminPanel</title>
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/fonts/font-face.css" rel="stylesheet" type="text/css" />

<!-- jquery by raihan -->
<script language="javascript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.min.js"></script>
<script language="javascript">

$(document).ready(function(){

	//alert($(".bg_holder").height());
	
	//$(".site_title").text($(".bg_holder").height())
	$("body").css('background','transparent url(../images/bg2.png) repeat-x 0 '+$(".bg_holder").height()+'px');
	
	
	
		$("#postaction").click(function(){

			$("#msg").text("");
			
			if($("#name").val() == '')
			{
				$("#msg").text("Required  fields * must be filled!");
				return false;
			}
		})

	
	

});

</script>

</head>

<body>

<div class="bg_holder">
    <div class="siteWidth">



<div id="col_1">
        <!-- left column -->
    
    
            <div class="logo_bg" onclick="location.href='index.php'">
               <span class="logo">&nbsp;</span>
            </div>
            
            <div class="leftmenu_title">
            	Main Menu
            </div>
            
            <div class="leftMenu">        
				<?php 
				
				include_once('inc_menu.php');?>
            </div>
            
            
        
        
        
        <!-- end left column -->
        </div>

        
        <div id="col_2">
        
        
        
            <div class="banner">
                
            </div>
            
            
            
            
            
          <?php /*  
			<div id="col_2_2">
                <div class="submenu_title">
                    SUB MENU
                </div>
                <!-- if sub menu exist -->
                <!--div class="submenu" style="background:#2D4856 url(images/rakadmin.png) no-repeat;height:520px;"  align="center"-->
                <!-- end if sub menu exist -->
                
                <div class="submenu" st yle="background:transparent url(../images/rakadmin.png) no-repeat;height:520px;"  align="center">
                
                    <div class="subMenu" >
                    
                    	<?php 
						
						//include_once('inc_'.$managerName.'_menu.php');?>
                    </div>                	
                </div>
                
            </div>    
			*/ ?>        
    
    		<div id="col_2_1" st yle="width:100%; border:0px solid #CCCCCC; background-color:#000099;" >
               <div class="site_title">
                     <?=ucfirst($managerName)?> Manager
               </div>
               
               <div class="home_body_content">
               
                <div class="clild_menu">
                	<a href="#"><?=ucfirst($managerName)?> Manager</a>
                    <div class="clear"></div>
                </div>
                
                



               


				<?php
					if($click_action == 'insert'){
                    	include_once('inc_'.$managerName.'_edit.php');
					} else if($click_action == 'edit'){
                    	include_once('inc_'.$managerName.'_edit.php');
					}
					else if($click_action == 'detail'){
                    	include_once('inc_'.$managerName.'_detail.php');
					}
					else{
						include_once('inc_'.$managerName.'_list.php');
						}
					
                ?>  

               
                
                &nbsp;
                
                
                

                <br />
<br />
<br />
<div class="clear"></div>
               </div>
            
        	    <div class="clear"></div>
            </div>
            
                 
    <div class="clear"></div>        
        </div>
   <div class="clear"></div> 
    </div>
<div class="clear"></div>
</div>

<div class="clear"></div>
<!-- footer -->
<div id="footer" class="siteWidth">
	
<?php
	include_once('../inc_footer.php');
	
	
	
?>


</div>

<!-- end footer -->
</body>
</html>
