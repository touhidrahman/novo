<?php

	session_start();

	require_once('../setting/config.php');
	require_once('../rak_framework/fetch.php');
	require_once('../rak_framework/connection.php');
	if($_REQUEST['postaction'] == 'Login')
	{	
	
		require_once('../rak_framework/login.php');
		$UserName = trim($_REQUEST['txtUserName']);
		$Password =  trim($_REQUEST['txtPassword']);
		
		$module_id = fetchByID('admin','user_name',$UserName,'module');

		//echo $module_id;

		
		switch($module_id)
		{
			case 1:
			$targetPage = 'job_manager.php';
			break;
			case 2:
			$targetPage = 'safety/user_manager.php';
			break;
			
		}

		
		adminLogin($UserName,$Password,$msg,$module_id,$targetPage);
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$siteTitle?></title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/fonts/font-face.css" rel="stylesheet" type="text/css" />

<!-- jquery by raihan -->
<script language="javascript" src="js/jquery.min.js"></script>
<script language="javascript">

$(document).ready(function(){

	//alert($(".bg_holder").height());
	
	//$(".site_title").text($(".bg_holder").height())
	$("body").css('background','transparent url(images/bg2.png) repeat-x 0 '+$(".bg_holder").height()+'px');
	
	$("#txtUserName").focus();
	$("#postaction").click(function(){

			$("#msg").text("");
			
			if($("#txtUserName").val() == '' || $("#txtPassword").val()== '')
			{
				$("#msg").text("Username and Password must be filled!");
				return false;
			}
		})
	
	//background: transparent url(../images/bg2.png) repeat-x 0 600px

});

</script>

</head>

<body>

<div class="bg_holder">
    <div class="siteWidth">



<div id="col_1">
        <!-- left column -->
    
    
            <div class="logo_bg" onclick="location.href='index.php'">
               <span class="logo">&nbsp;</span>
            </div>
            

            
            
        
        
        
        <!-- end left column -->
        </div>

        
        <div id="col_2">
        
        
        
            <div class="banner">
                
            </div>
            
            
            
            
            
            
			<div id="col_2_2">
                <div class="submenu_title">
                     
                </div>
                <!-- if sub menu exist -->
                <!--div class="submenu" style="background:#2D4856 url(images/rakadmin.png) no-repeat;height:520px;"  align="center"-->
                <!-- end if sub menu exist -->
                
                <div class="submenu" style="background:transparent url(images/rakadmin.png) no-repeat;height:520px; margin-top:-45px; margin-left:-185px;"  align="center">
                
              	
                </div>
                
            </div>            
    
    		<div id="col_2_1" st yle="width:100%; border:1px solid #fff; background-color:#000099;" >
               <div class="site_title">
                    Admin Login
               </div>
               
               <div class="home_body_content">
                 <div class="grid"></div>
                
                
                
                &nbsp;
                
                
                



                <div class="message">
                	<!--span style="color:#ffd200;">This is a message for any action result success</span-->
                    <span style="color:#ffd200;" id="msg"><?=$msg?><?=($_REQUEST['logout']==1)?'<span style="color:#8dff4f;">Successfully logged out</span>':''?></span>
					
					
                    <!--span style="color:#8dff4f;">This is a message for any action result not success</span-->
                </div>
			<div class="form">
            
                	<div class="form_title">
                    	<div class="title">Enter username and password to login</div>
                        
                        <div class="back"><input type="button" value="Exit" onclick="location.href='http://www.flynovoair.com'" style="margin:0px;"></div>

                    </div>            
            
                <form action="<?=$_SERVER['PHP_SELF']?>" method="post" id="loginForm" >

                
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td colspan="2">
                      <div class="form_instruction">
                      Please fill all the input boxes and press action(LOGIN) button. You can also cancel your action by pressing the CANCEL button below.
                      If you feel you have come to this page accidentally press Exit button on the top of this form. 
                      
                        
                      </div>
                      </td>
                    </tr>
                    <tr>
                      <td width="40%" align="right"><strong>
                        User ID		</strong></td>
                      <td width="60%">
                        <input name="txtUserName" type="text" style="width:280px;" maxlength="20" id="txtUserName" /></td>
                    </tr>
                    <tr>
                      <td align="right"><strong>Passowrd </strong></td>
                      <td><input name="txtPassword" type="password" style="width:280px;" maxlength="20" id="txtPassword" /></td>
                    </tr>
                    <tr>
                      <td align="right" class="form_button_bar_left">                      </td>
                      <td class="form_button_bar_right">
                      
                      	
                        <input type="submit" name="postaction" value="Login" class="button" id="postaction" />
                      <input type="reset" name="Submit2" value="Clear" class="button" />                        </td>
                    </tr>
                  </table>
                </form>
                
                </div>                
                
               
            	
    
    
    
    
       
                  <p>&nbsp;</p>
<div class="clear"></div>
               </div>
            
        	    <div class="clear"></div>
            </div>
            
                 
    <div class="clear"></div>        
        </div>
   <div class="clear"></div> 
    </div>
<div class="clear"></div>
</div>

<div class="clear"></div>
<!-- footer -->
<div id="footer" class="siteWidth">
	
<?php
	include_once('inc_footer.php');
?>
    
    
</div>

<!-- end footer -->
</body>
</html>
