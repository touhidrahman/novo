<?php
	require_once('setting/config.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
 <?php
 	include_once('inc_top_includes.php');
 ?>
</head>
<body class="inner">

    <!-- top nav -->
			 <?php
                include_once('inc_topnav.php');
             ?>     
             
	<!-- end top nav -->  
    
    

<br>
<br>
<br>

<div class="container help-line">
    <div>
        <img src="images/help-line.png" class="hidden-xs">
        <img src="images/help-line2.png" class="visible-xs">
    </div>
</div>


<!-- main navigation -->
			 <?php
                include_once('inc_mainnav.php');
             ?>           
<!-- end main navigation -->





<div class="slide-wrapper">


<!-- inner page Carousel -->
 <?php
    include_once('inc_carousel_inner.php');
 ?>   
<!-- end inner page Carousel -->  
<!-- content -->
    <div class="container">
    
    	<div class="body-container">
    
    
        
         <div class="breadcrumb">
        <div class="row">
          <div  class="col-sm-6">
            <h1><b>PROHIBITED </b>  ITEMS</h1>
          </div>
          <div class="col-sm-6 text-right"> Travel Info / PROHIBITED ITEMS </div>
        </div>
      </div>     
        
      


        
        <div class="page-contents">
        

        
        
        
        
        
        

	




        <div class="row">
        	

            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
	         <h2>DANGEROUS ARTICLES NOT PERMITTED IN HAND LUGGAGE</h2>
             
<br>
<br>
<div class="row">
    <div class="col-xs-6">
    
    
    
    
    
		<div class="content">



            <p>
              The following items must not be packed in your hand luggage.  They must be placed in your hold baggage. If these items are discovered in your  hand luggage they will be confiscated and not returned.</p>
    
    
    
            <ol>
    
              <li><strong>Nail cutter/Nail file</strong><br>
    
                &nbsp;
    
              </li>
    
              <li><strong>Razor blades</strong><br>
    
                (razors that have the blade set into a plastic moulding are allowed, but razors  that can be opened and the razor blade removed are prohibited)<br>
    
                &nbsp;
    
              </li>
    
              <li><strong>Knives</strong><br>
    
                (with blades of any length)<br>
    
                &nbsp;
    
              </li>
    
              <li><strong>Household cutlery</strong><br>
    
                (large spoons/tablespoons are NOT permitted, however small teaspoons are  allowed)<br>
    
                &nbsp;
    
              </li>
    
              <li><strong>Corkscrews</strong><br>
    
                &nbsp;
    
              </li>
    
              <li><strong>Lighters</strong><br>
    
                &nbsp;
    
              </li>
    
              <li><strong>Sharp tools/objects</strong><br>
    
                (including multi-tools and penknives)<br>
    
                &nbsp;
    
              </li>
    
              <li><strong>Scissors</strong><br>
    
                (except where both blades are round-ended or less than three centimeters)<br>
    
                &nbsp;
    
              </li>
    
              <li><strong>Rope</strong><br>
    
                &nbsp;
    
              </li>
    
              <li><strong>Catapults</strong><br>
    
    &nbsp; </li>
    
              <li><strong>Darts</strong><br>
    
                &nbsp;
    
              </li>
    
              <li><strong>Billiard</strong><br>
    
                (Snooker or pool cues)<br>
    
                &nbsp;
    
              </li>
    
              <li><strong>Sporting bats<br>
    
                &nbsp;
    
              </strong></li>
    
              <li><strong>Toy/replica guns</strong><br>
    
                (metal or plastic)<br>
    
                &nbsp;
    
              </li>
    
              <li><strong>Walking/hiking poles<br>
    
                &nbsp;
    
              </strong></li>
    
              <li><strong>Hypodermic needles</strong><br>
    
                (unless required for medical reasons, for which proof will be required)<br>
    
                &nbsp;
    
              </li>
    
              <li><strong>Body spray<br>
    
                &nbsp;
    
              </strong></li>
    
              <li><strong>Measuring tape<br>
    
                &nbsp;
    
              </strong></li>
    
              <li><strong>Fish/Meat<br>
    
                &nbsp;
    
              </strong></li>
    
              <li><strong>Paint</strong></li>
    
            </ol>
    
            

        
		</div>    	
    </div>
    <div class="col-xs-6">
	    <img src="images/trlv/restricted-items.jpg"  class="img-responsive" align="right">
    </div>
</div>

		
        
    <br>
<br>



  




                
            </div>
            
            <div class="first-col col-lg-3 col-md-3  hidden-sm hidden-xs ">
                <!-- icon buttons -->
                 <?php
                    include_once('inc_inner_sidebar_iconbuttons.php');
                 ?>
                <!-- end icon buttons -->
            </div>            
            
        </div>
		
        <div class="clearfix">&nbsp;</div>
        
                <!-- icon buttons on bottom -->
                 <?php
                    include_once('inc_inner_bottombar_iconbuttons.php');
                 ?>
                <!-- end icon buttons on bottom-->
<br>
<br>




<!-- footer -->
 <?php
 	include_once('inc_footer.php');
 ?>
<!-- end footer -->








                    
        </div>
        
        
        
    </div>
    </div>
    <!-- end content -->


</div>

<div style="clear:both"></div>







 <?php
 	include_once('inc_bottom_includes.php');
 ?>


   

</body>
</html>
