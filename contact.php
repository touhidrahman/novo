<?php
	require_once('setting/config.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
 <?php
 	include_once('inc_top_includes.php');
 ?>
</head>
<body class="inner">

    <!-- top nav -->
			 <?php
                include_once('inc_topnav.php');
             ?>     
             
	<!-- end top nav -->  
    
    

<br>
<br>
<br>

<div class="container help-line">
    <div>
        <img src="images/help-line.png" class="hidden-xs">
        <img src="images/help-line2.png" class="visible-xs">
    </div>
</div>


<!-- main navigation -->
			 <?php
                include_once('inc_mainnav.php');
             ?>           
<!-- end main navigation -->





<div class="slide-wrapper">

<!-- inner page Carousel -->
 <?php
    include_once('inc_carousel_inner.php');
 ?>   
<!-- end inner page Carousel -->   

  <!-- content -->
    <div class="container">
    
    	<div class="body-container">
    
    
        
         <div class="breadcrumb">
        <div class="row">
          <div  class="col-sm-6">
            <h1><b>Contact  </b> Us</h1>
          </div>
          <div class="col-sm-6 text-right"> Information / Contact  Us </div>
        </div>
      </div>     
        
      


        
        <div class="page-contents">
        

        
        
        
        
        
        

	




        <div class="row">
        	

            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
	         <h2>NOVOAIR Offices</h2>
             
<br>
<br>





<div class="table contactinfo">

      <div class="tr">

        

        <div class="td"><h2>Dhaka</h2>

          <p><strong><a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=dhk_corporate">Corporate Office</a></strong><br>

            House-50, 3rd  Floor, Road-11, Block-F, Banani, Dhaka <a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=dhk_corporate"><img src="images/icons/icon_find.png" alt="View Location Map" width="20" height="20" border="0" align="absmiddle"></a><br>

			Phone: 09666722224, Reservation Ext: 5100 – 03, Marketing &amp; Sales Ext: 5115 -16, 19, 20 

            </p>

            

          <p><strong><a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=dhk_banani">NOVOAIR, Banani</a></strong><br>

            House-32, Chandiwala Mansion, 2nd Floor, Road-11, Block-G, Banani<a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=dhk_banani"><img src="images/icons/icon_find.png" alt="View Location Map" width="20" height="20" border="0" align="absmiddle"></a><br>

            Phone: 09666722224, ext: 5111, 5400-03; 01755656662</p>

          <p><strong><a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=dhk_gulshan">NOVOAIR Sales Counter, Gulshan</a></strong><br>

            Z Tower, Ground Floor, Plot-4, Road-132, Gulshan 1, Dhaka<a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=dhk_gulshan"><img src="images/icons/icon_find.png" alt="View Location Map" width="20" height="20" border="0" align="absmiddle"></a><br>

            Phone: 09666 722224, ext: 5124; 01755656665; 02 9889896</p>

          <p><strong><a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=dhk_uttara">NOVOAIR, Uttara</a></strong><br>

           House-53, Road-18, Sector-3, Uttara, Dhaka<a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=dhk_uttara"><img src="images/icons/icon_find.png" alt="View Location Map" width="20" height="20" border="0" align="absmiddle"></a><br>

            Phone: 09666 722224, ext: 5121; 01755656664</p>

          <p><a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=dhk_airport"><strong>Airport  Sales Counter at Hazrat Shahjalal International Airport, Domestic Terminal</strong><img src="images/icons/icon_find.png" alt="View Location Map" width="20" height="20" border="0" align="absmiddle"></a><br>

            Phone: 09666 722224, 

            ext: 5123;  01755656663</p>

          

        </div>

      </div>

      <div class="tr">

        

        <div class="td"> <h2>Chittagong</h2>

          <p><strong><a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=ctg_gec">NOVOAIR, GEC Circle</a></strong><br>

           Husna Mansion, 2nd Floor, 1702 CDA Avenue, GEC Circle, Chittagong <a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=ctg_gec"><img src="images/icons/icon_find.png" alt="View Location Map" width="20" height="20" border="0" align="absmiddle"></a><br>

            Phone: 09666 722224, ext: 5130-5133; 01755656666; 031 2558281/82 </p>

          <p><strong><a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=ctg_cepz">NOVOAIR, CEPZ</a></strong><br>

            BEPZA Complex, CEPZ, Halishahar, CEPZ, Chittagong <a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=ctg_cepz"><img src="images/icons/icon_find.png" alt="View Location Map" width="20" height="20" border="0" align="absmiddle"></a><br>

            Phone: 09666 722224, ext: 5143; 01755656667</p>

          <p><a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=ctg_airport"><strong>Airport Sales Counter, Shah Amanat International Airport, Chittagong</strong><img src="images/icons/icon_find.png" alt="View Location Map" width="20" height="20" border="0" align="absmiddle"></a><br>

            Phone: 09666 722224, 

            ext: 5144; 01755656668</p>

          

        </div>

      </div>

      <div class="tr">

        

        <div class="td"> <h2>Cox's Bazar</h2>

          <p><strong><a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=coxs_bazar">NOVOAIR, Cox’s Bazar</a></strong><br>

           Hotel Kallol, Kolatoli Beach Road, Cox’s Bazar  <a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=coxs_bazar"><img src="images/icons/icon_find.png" alt="View Location Map" width="20" height="20" border="0" align="absmiddle"></a><br>

            Phone: 09666 722224, ext: 5145; 01755656669; 0341 63142 </p>

          

        </div>

      </div>

      <div class="tr">

        

        <div class="td"><h2>Jessore</h2>

          <p><strong><a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=jessore">NOVOAIR, Jessore</a></strong><br>

           Rail Road, Chourasta, Jessore <a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=jessore"><img src="images/icons/icon_find.png" alt="View Location Map" width="20" height="20" border="0" align="absmiddle"></a><br>

            Phone: 09666 722224, ext: 5147; 01755656670 </p>



        </div>

      </div>

      

      

		<div class="tr">

        

        <div class="td"><h2>Khulna</h2>

          <p><strong><a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=khulna">NOVOAIR, Khulna</a></strong><br>

            Hotel Tiger Garden Int. Hotel, Sena Kalyan Bhaban, 1 KDA Avenue, Khulna<a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=khulna"><img src="images/icons/icon_find.png" alt="View Location Map" width="20" height="20" border="0" align="absmiddle"></a><br>

            Phone: 09666 722224, ext: 5150; 01755656671; 041 2833135; 041-2831365</p>

        </div>

      </div>      

      

      

      

      <div class="tr">

        

        <div class="td"><h2>Sylhet</h2>

          <p><strong><a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=sylhet">NOVOAIR,  Sylhet</a></strong><br>

           Anando Tower, Ground Floor, Jail Road, Sylhet <a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=sylhet"><img src="images/icons/icon_find.png" alt="View Location Map" width="20" height="20" border="0" align="absmiddle"></a><br>

            Phone: 09666 722224, ext: 5152; 01755656672; 0821-728960</p>

          

        </div>

      </div>

      

    </div>
		
        
    <br>
<br>



  




                
            </div>
            
            <div class="first-col col-lg-3 col-md-3  hidden-sm hidden-xs ">
                <!-- icon buttons -->
                 <?php
                    include_once('inc_inner_sidebar_iconbuttons.php');
                 ?>
                <!-- end icon buttons -->
            </div>            
            
        </div>
		
        <div class="clearfix">&nbsp;</div>
        
                <!-- icon buttons on bottom -->
                 <?php
                    include_once('inc_inner_bottombar_iconbuttons.php');
                 ?>
                <!-- end icon buttons on bottom-->
<br>
<br>




<!-- footer -->
 <?php
 	include_once('inc_footer.php');
 ?>
<!-- end footer -->








                    
        </div>
        
        
        
    </div>
    </div>
    <!-- end content -->


</div>

<div style="clear:both"></div>







 <?php
 	include_once('inc_bottom_includes.php');
 ?>


   

</body>
</html>
