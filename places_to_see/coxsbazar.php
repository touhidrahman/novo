<?php require_once('/setting/config.php'); ?>
<!DOCTYPE html>
<html lang="en">
<?php include_once('/partials/head.php'); ?>

	<body class="inner">
		<br>
		<br>
		<br>
		<div class="container help-line">
			<div>
				<img src="images/help-line.png" class="hidden-xs">
				<img src="images/help-line2.png" class="visible-xs"> </div>
		</div>
		<!-- main navigation -->
		<?php include_once('../partials/mainnav2.php'); ?>
		<!-- end main navigation -->
		<div class="slide-wrapper">
			<?php include_once('../partials/inner_carousel.php'); ?>
			<!-- content -->
			<div class="container">
				<div class="body-container">

					<div class="breadcrumb">
						<div class="row">
							<div class="col-sm-6">
								<h1><b>Contacts </b> And Addresses</h1>
							</div>
							<div class="col-sm-6 text-right"> About Us / Contacts </div>
						</div>
					</div>

					<div class="page-contents">
						<!-- ==============START CONTENT============= -->
            <p>
            <img class="img-responsive" src="http://visitbangladesh.gov.bd/wp-content/uploads/2015/11/coxbazar.jpg" alt="coxbazar"/>

                Cox’s Bazar sea beach, the world’s longest natural sandy sea beach with its incredible 120 km length. It is the most visited tourist destination in Bangladesh. Every year millions of foreigner and local people come here to enjoy their holidays. You
            		will enjoy sunrise and sunset view very much in this beach because the sea changes color in those time. Warm shark free water is best for swimming and sunbathing. You will enjoy surfing, jogging, cycling in this beach too. It is always crowded by tourists.
            		Near the beach you will find there are hundreds of shops selling souvenirs and beach accessories to the tourists.</span>
            </p>
            <p>
            	Here you will find many Buddhist Temples and tribes, colorful Pagodas and delightful sea-food which make Cox’s bazar so special.
            </p>
            <p>
            		<strong>Location</strong>: Cox’s Bazar, Chittagong
            </p>

            <p>
            		<strong>How to get there</strong>
            </p>
            <p>
            	You can come to Cox’s Bazar by bus or air. There are several bus service depart from Dhaka to Cox’s Bazar. To book a NOVOAIR flight <a href="index.php">click here</a>
            </p>




						<!-- ==============END CONTENT============= -->
						<div class="clearfix">&nbsp;</div>
						<br>
						<br>
						<!-- footer -->
						<?php include_once('../partials/footer.php'); ?>
							<!-- end footer -->
					</div>
					<!-- end page content -->
				</div>
			</div>
			<!-- end container -->
		</div>
		<!-- end slide wrapper -->
		<?php include_once('../partials/tail.php'); ?>
	</body>

</html>
