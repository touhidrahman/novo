<?php
	require_once('setting/config.php');
	
	
	require_once('rak_framework/connection.php');
	require_once('rak_framework/fetch.php');
	require_once('rak_framework/misfuncs.php');	
	
	// Check the right query string;

	$id = $_REQUEST["id"]; //in this case the value is 12345
	$hash = $_REQUEST["token"];
	if (chkRakHash($id, $hash)) {
	  //no tampering detected, proceed with other processing
		  $_REQUEST['id'] = $_REQUEST['id'];
	} else {
	  //tampering of data detected
		  $_REQUEST['id'] = '';
		  echo 'Unexpected Query String......';
		  exit();
	}
	
	
	$inputData = array(
	'TableName' => 'job_manager',
	'FetchByKey' => 'id',
	'FetchByValue' =>  $_REQUEST['id'],

	'job_title' => '',
	'description' => '',
	'application_instruction' => '',
	'deadline' => '',
	'entry_date' => '',
	'edit_date' => ''
);

		if($_REQUEST['id'])
	   {
			//#EDIT
			$id = $_REQUEST['id'];	
				fetchData($inputData,$data);
				//print_r($data);
	  }
	  else
	  {
		$msg = 'Error: Invalid job id';  
	  }

	//page parameters
	$section = 'Corporate';
	$pageTitle = 'Career';
	$siteTitle = $siteTitle." : ".$pageTitle;	
?>


<!DOCTYPE html>
<html lang="en">

<head>
 <?php
 	include_once('inc_top_includes.php');
 ?>
</head>
<body class="inner">

    <!-- top nav -->
			 <?php
                include_once('inc_topnav.php');
             ?>     
             
	<!-- end top nav -->  
    
    

<br>
<br>
<br>

<div class="container help-line">
    <div>
        <img src="images/help-line.png" class="hidden-xs">
        <img src="images/help-line2.png" class="visible-xs">
    </div>
</div>


<!-- main navigation -->
			 <?php
                include_once('inc_mainnav.php');
             ?>           
<!-- end main navigation -->





<div class="slide-wrapper">

<!-- inner page Carousel -->
 <?php
    include_once('inc_carousel_inner.php');
 ?>   
<!-- end inner page Carousel -->  

<!-- content -->
    <div class="container">
    
    	<div class="body-container">
    
    
        
         <div class="breadcrumb">
        <div class="row">
          <div  class="col-sm-6">
            <h1><b>Career  </b>  </h1>
          </div>
          <div class="col-sm-6 text-right"> Information / Career </div>
        </div>
      </div>     
        
      


        
        <div class="page-contents">
        

        
        
        
        
        
        

	




        <div class="row">
        	

            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
	         <h2><?=$data['job_title']?></h2>
             
	<br>


<form>
<?php
if($_REQUEST['id'])
{
?>

<p>
<?=$data['description']?>
</p>

<h2>Deadline: <?=formatDate($data['deadline'])?></h2>
<br>



<h2>Application Instruction</h2>
<p>
<?=$data['application_instruction']?>
</p>
<!--APPLY ONLINE OR SEND YOU CV TO career@flynovoair.com<br>
CV FOR THE ATTENTION OF: XXXXX YYYYY<br>
APPLICATIONS MUST REACH US NO LATER THAN: TUESDAY, 20 JANUARY 2013-->



<br>




<a href="javascript:void(0);"  class="btn btn-danger btn-lg link_btn sub_window"  width="800px" height="720px" style="width:250px;" path="job_apply.php?id=<?=$_REQUEST['id']?>">Apply</a>
<?php
}
?>

                </form>
   
                
              

		
        
    <br>
<br>



  




                
            </div>
            
            <div class="first-col col-lg-3 col-md-3  hidden-sm hidden-xs ">
                <!-- icon buttons -->
                 <?php
                    include_once('inc_inner_sidebar_iconbuttons.php');
                 ?>
                <!-- end icon buttons -->
            </div>            
            
        </div>
		
        <div class="clearfix">&nbsp;</div>
        
                <!-- icon buttons on bottom -->
                 <?php
                    include_once('inc_inner_bottombar_iconbuttons.php');
                 ?>
                <!-- end icon buttons on bottom-->
<br>
<br>




<!-- footer -->
 <?php
 	include_once('inc_footer.php');
 ?>
<!-- end footer -->








                    
        </div>
        
        
        
    </div>
    </div>
    <!-- end content -->


</div>

<div style="clear:both"></div>







 <?php
 	include_once('inc_bottom_includes.php');
 ?>

<script language="javascript">
$(document).ready(function(e) {
	$("#agentForm").submit(function(e){


		if(!IsEmail($("input[name=email]").val()))
		{
			top.$.fn.colorbox({
			html: '<p class="title" style="text-align:center;font-size:12px;padding:10px;">Please enter a valid email address</p>',
				width: "400px;"
			});	
			
						
			$("select[name=email]").focus();
			return false;
			
		}


		if(!$("input[name=phone]").val())
		{
			//alert("Please select passengers");
			
			top.$.fn.colorbox({
			html: '<p class="title" style="text-align:center;font-size:12px;padding:10px;">Please enter a phone number</p>',
				width: "400px;"
			});	
			
						
			$("select[name=phone]").focus();
			return false;
		}



		if(!$("input[name=agent_form]").val())
		{
			//alert("Please select passengers");
			
			top.$.fn.colorbox({
			html: '<p class="title" style="text-align:center;font-size:12px;padding:10px;">Please attach the application form before sending</p>',
				width: "400px;"
			});	
			
						
			$("select[name=agent_form]").focus();
			return false;
		}
		
		
		
	});
});
</script>
   

</body>
</html>
