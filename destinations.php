<?php
	require_once('setting/config.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
 <?php
 	include_once('inc_top_includes.php');
 ?>
</head>
<body class="inner">

    <!-- top nav -->
			 <?php
                include_once('inc_topnav.php');
             ?>     
             
	<!-- end top nav -->  
    
    

<br>
<br>
<br>

<div class="container help-line">
    <div>
        <img src="images/help-line.png" class="hidden-xs">
        <img src="images/help-line2.png" class="visible-xs">
    </div>
</div>


<!-- main navigation -->
			 <?php
                include_once('inc_mainnav.php');
             ?>           
<!-- end main navigation -->





<div class="slide-wrapper">

<!-- inner page Carousel -->
 <?php
    include_once('inc_carousel_inner.php');
 ?>   
<!-- end inner page Carousel -->   


  <!-- content -->
    <div class="container">
    
    	<div class="body-container">
    
    
        
        <div class="breadcrumb">
        <div class="row">
          <div  class="col-sm-6">
            <h1><b>Destination  </b>   We Fly</h1>
          </div>
          <div class="col-sm-6 text-right"> Services / Destination We Fly </div>
        </div>
      </div>      
        
      


        
        <div class="page-contents">
        



        <div class="row">
        	

            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">


<?php
if($_REQUEST['show'] == 'dhaka' || $_REQUEST['show'] == 'all' ){
?>

<h2>DHAKA </h2>
             

<br>
<p>
<img src="images/destinations/dhaka_f.jpg" class="img-responsive">
</p>

<p>
DHAKA is the capital and largest city of Bangladesh. Having a rich history and colorful cultural heritage, Dhaka is known to the world as the city of mosques and muslin. Its fame and glamour has always attracted travelers from distant and nearby counties for ages. Today the city has grown into a mega city of about 20 million people; with an area of about 1353 sq. km. Dhaka has become the hub of the nation's industrial, commercial, cultural, educational and political activities.
</p>
<strong>SIGHTS TO SEE</strong>

<ul>
  <li>National Memorial</li>
  <li>Central ShahidMinar</li>
  <li>Parliament House</li>
  <li>Lalbagh Fort</li>
  <li>Ahsan Manzil Museum</li>
  <li>National Museum</li>
  <li>Sonargaon</li>
  <li>Dhakeshwari Temple</li>
  <li>Khan Mohammad Mridha’s Mosque</li>
  <li>Armenian Church</li>
  <li>Star Mosque (Tara Masjid)</li>
  
  
</ul>
<br>
<br>

<hr>

<?php
}
if($_REQUEST['show'] == 'chittagong' || $_REQUEST['show'] == 'all' ){
?>

<h2>CHITTAGONG  </h2>
             

<br>
<p>
<img src="images/destinations/chittagong_f.jpg" class="img-responsive">
</p>
<p>
CHITTAGONG has the largest seaport and is the second largest city in Bangladesh placed by the Bay of Bengal. It is located at 264 km South-East of Dhaka, & famous for its hilly region and natural beauty. Chittagong is also very popular for its coastline. The city has been recognized also as the town of Aulias (Muslim saints). The green hills and forests, broad sandy beaches and reasonably temperate climate of the city have always attracted a high number of holiday-makers.
</p>
<p>
Chittagong has the country's principal port and is the central spot for the establishment of heavy, medium and light industries. Bangladesh's only steel mill and oil refinery are also located in Chittagong.


</p>
<strong>SIGHTS TO SEE</strong>

<ul>
  <li>War Cemetery</li>
  <li>Ethnological Museum</li>
  <li>Court Building Museum</li>
  <li>Shrines</li>
  <li>Fays Lake</li>
  <li>Patenga &amp; Fouzdarhat</li>
  <li>Port Area</li>
  <li>Rangamati</li>
  <li>Khagrachari</li>
  <li>Bandarban</li>
  
  
  
</ul>
<br>
<br>
<hr>


<?php
}
if($_REQUEST['show'] == 'sylhet' || $_REQUEST['show'] == 'all' ){
?>

<h2>SYLHET    </h2>
             

<br>
<p>
<img src="images/destinations/sylhet_f.jpg" class="img-responsive">
</p>
<p>
SYLHET is the land of Shrine’s, natural Hills, forests, beautiful tree plantations and lots of Haors. It is an old city with full of natural beauties. A large number of tourists come every year to visit Sylhet. For miles and miles around, the visitor can see the teagardens spread like a green carpet over the plain land or on the sloping hills.
</p>
<strong>SIGHTS TO SEE</strong>

<ul>

  <li> The Shrine of Hazrat ShahJalal</li>
  <li> Sri Chaitannya Dev Temple</li>
  <li> Gour Gobinda Fort</li>
  <li> Jaflong</li>
  <li> Tamabil</li>
  <li> Sripur</li>
  <li> Jointapur's Rajbari</li>
  <li> Srimongol</li>
  <li> Madhabkunda Waterfall</li>
  <li> Lawacherra Rain Forest</li>
  <li> Handicrafts of Sylhet</li>
  <li> Manipuri Dance</li>
</ul>
<br>
<br>
<hr>

<?php
}
if($_REQUEST['show'] == 'coxbazar' || $_REQUEST['show'] == 'all' ){
?>

<h2>COX'S BAZAR     </h2>
             

<br>
<p>
<img src="images/destinations/coxsbazar_f.jpg" class="img-responsive">
</p>
<p>
COX'S BAZAR is one of the most attractive tourist spots &amp; the longest sea beach in the world (approx. 120 km long). Miles of golden sands, towering cliffs, surfing waves, rare conch shells, colorful pagodas, Buddhist temples and tribes, delightful seafood--this is Cox's Bazar, the tourist capital of Bangladesh. Every year lots of foreign & local tourist come here to spend their leisure in Cox’s Bazar. Though the season is in winter but Cox's Bazar sea beach is crowded almost throughout the year.
</p>
<strong>SIGHTS TO SEE</strong>

<ul>

Himchori &amp; Inani Beach</li>
<li>Moheshkhali Island</li>
<li>Sonadia Island</li>
<li>St. Martins Island</li>
<li>Nijhum Island</li>
<li>Aggameda Khyang</li>
<li>Ramu</li>

</ul>
<br>
<br>

<?php
}
if($_REQUEST['show'] == 'jessore' || $_REQUEST['show'] == 'all' ){
?>



<h2>JESSORE   </h2>
             

<br>
<p>
<img src="images/destinations/jessore_f.jpg" class="img-responsive">
</p>
<p>
JESSORE is a district in the southwestern region of Bangladesh. Apart from some attractive colonial architecture, most notably the huge Court House building, there is little to see in Jessore in terms of sights. But Jessore is the entree point for air travelers to Sundarban. It is the world biggest mangrove forest. In Bangladesh tourism, Sunderban plays the most vital role. 
</p>
<strong>SIGHTS TO SEE</strong>

<ul>
  <li> Sundarban</li>
  <li> Mongla</li>
  <li> Shat Gambuj Mosque</li>
  <li> Shrine-Hajrat Khan Jahan Ali</li>
  <li> Shagordari</li>
  <li> Shaldia Thakur Bari</li>
</ul>
<br>
<br>
<hr>



<?php
}
if($_REQUEST['show'] == 'barisal' || $_REQUEST['show'] == 'all' ){
?>



<h2>BARISAL   </h2>
             

<br>
<p>
<img src="images/destinations/barishal_f.jpg" class="img-responsive">
</p>
<p>
BARISAL division is in the south west part of Bangladesh, has an area of 13297 sq. km and a population of 8.11 million. There are 6 districts and 22 municipalities under Barisal. Barishal is a Division of rivers and canals. It is also famous for gardens of coconut trees. Kuakata is the main tourist spot of the division. Durga Sagor - a beautiful &amp; famous Dighi in Barishal City has always attracted a lot of guest birds in every winter season. Kuakata, locally known as Sagar Kannya (Daughter of the Sea) is located under this city &amp; is famous for its rare scenic beauty, sandy beach, blue sky, huge expanse of water of the Bay and evergreen forest etc. </p>
<strong>SIGHTS TO SEE</strong>

<ul>
  <li>Kuakata</li>
  <li>Horinghata</li>
  <li>Fisherman Village</li>
</ul>
<br>
<br>
<hr>

<?php
}
if($_REQUEST['show'] == 'khulna' || $_REQUEST['show'] == 'all' ){
?>



<h2>KHULNA   </h2>
             

<br>
<p>
<img src="images/destinations/jessore_f.jpg" class="img-responsive">
</p>
<p>
KHULNA division is in the southwest part of Bangladesh; has an area of 22274 sq. km and a population of 14.47 million. There are 10 districts and 28 municipalities under Khulna. It has the world's biggest mangrove forest, the Sundarbans. Sundarban's beauty lies in its unique natural surroundings. Thousands of meandering streams, creeks, rivers and estuaries have always enhanced its charm. Other main attraction of the city can be previewed through wildlife photography including photography of the famous Royal Bengal Tiger, wildlife viewing, boating inside the forest, nature study, meeting fishermen, wood-cutters and honey-collectors, peace and tranquility in the wilderness, seeing the world's largest mangrove forest and the revering beauty. In Khulna town one can also visit Khulna Museum and Zoo.</p>
<strong>SIGHTS TO SEE</strong>

<ul>
  <li>Karamjol</li>
  <li>Katka</li>
  <li>Kochikhali</li>
  <li>Hiran point</li>
  <li>Mandarbaria</li>
  <li>Dublar Char</li>
</ul>
<br>
<br>
<hr>

<?php
}
if($_REQUEST['show'] == 'rajshahi' || $_REQUEST['show'] == 'all' ){
?>



<h2>RAJSHAHI   </h2>
             

<br>
<p>
<img src="images/destinations/jessore_f.jpg" class="img-responsive">
</p>
<p>
RAJSHAHI division, famous for its archeological and historical sites is situated in the northern part of Bangladesh, consists of an area of 34513 sq. km with a population of 29.99 million. The city is situated besides the river Padma. In monsoon the great Padma is in full spate with its tides and waves whereas in winter it dwindles. The famous archeological and historical places situated are Mohastnangor, Paharpur Buddhist Monastery, Kantajee’s Temple, Ramshagar Dighi, Choto Sona Masjid, Shopnopuri etc. There is also Rajshahi University, structurally a very well planned University, where one can  see the documents and photography’s from the language movement of 1952 to the liberation was 1972 and also visit The Shahid Smriti Sangraha Shala situated in the University. There is also Borendra Research Museum famous for containing ancient elements of Paharpur, Mohasthangar and Mohenjodaro.</p>
<strong>SIGHTS TO SEE</strong>

<ul>

  <li>Mohastnangor</li>
  <li>Paharpur Buddhist Monastery</li>
  <li>Kantajee’s Temple</li>
  <li>Ramshagar Dighi</li>
  <li>Choto Sona Masjid</li>
  <li>Shopnopuri</li>
  <li>Varendra Research Museum</li>
  <li>Puthia</li>
  
</ul>
<br>
<br>
<hr>





<?php
}
?>






                
          </div>
            
            <div class="first-col col-lg-3 col-md-3  hidden-sm hidden-xs ">
                <!-- icon buttons -->
                 <?php
                    include_once('inc_inner_sidebar_iconbuttons.php');
                 ?>
                <!-- end icon buttons -->
            </div>            
            
        </div>
		
        <div class="clearfix">&nbsp;</div>
        
                <!-- icon buttons on bottom -->
                 <?php
                    include_once('inc_inner_bottombar_iconbuttons.php');
                 ?>
                <!-- end icon buttons on bottom-->
<br>
<br>




<!-- footer -->
 <?php
 	include_once('inc_footer.php');
 ?>
<!-- end footer -->








                    
        </div>
        
        
        
    </div>
    </div>
    <!-- end content -->


</div>

<div style="clear:both"></div>







 <?php
 	include_once('inc_bottom_includes.php');
 ?>


   

</body>
</html>
