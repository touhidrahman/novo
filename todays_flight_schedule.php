<?php
	require_once('setting/config.php');
	date_default_timezone_set('Asia/Dhaka');
	require_once "todays_flight_schedule_rss_data_parser.php";	
?>
<!DOCTYPE html>
<html lang="en">

<head>
 <?php
 	include_once('inc_top_includes.php');
 ?>
</head>
<body class="inner">

    <!-- top nav -->
			 <?php
                include_once('inc_topnav.php');
             ?>     
             
	<!-- end top nav -->  
    
    

<br>
<br>
<br>

<div class="container help-line">
    <div>
        <img src="images/help-line.png" class="hidden-xs">
        <img src="images/help-line2.png" class="visible-xs">
    </div>
</div>


<!-- main navigation -->
			 <?php
                include_once('inc_mainnav.php');
             ?>           
<!-- end main navigation -->





<div class="slide-wrapper">


<!-- inner page Carousel -->
 <?php
    include_once('inc_carousel_inner.php');
 ?>   
<!-- end inner page Carousel -->  
<!-- content -->
    <div class="container">
    
    	<div class="body-container">
    
    
        
         <div class="breadcrumb">
        <div class="row">
          <div  class="col-sm-6">
            <h1><b>
                        <!--sun  Nov 11-->
                        <?php
                        	//echo date("D    M    d");
							/*
							echo date("D").' &nbsp;&nbsp;&nbsp;';
							echo date("M").' &nbsp;&nbsp;&nbsp;';
							echo date("d");
							*/
							echo date("l");
							//echo 'Flight Schedule';
						?>             
            
               </b> Schedule</h1>
          </div>
          <div class="col-sm-6 text-right"> Plan a Trip / Today's Flight Schedule </div>
        </div>
      </div>     
        
      


        
        <div class="page-contents">
        

        
        
        
        
        
        

	




        <div class="row">
        	

            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
	         <h2>Flight Schedule,  
             <?php
				echo date("D").'  ';
				echo date("M").'  ';
				echo date("d");             
			?>
             </h2>
             
<br>
<br>

        <?php parserSide("http://secure.flynovoair.com/rss", "status"); ?>     
  
                        

    <br>
<br>
                
            </div>
            <div class="first-col col-lg-3 col-md-3  hidden-sm hidden-xs ">
                <!-- icon buttons -->
                 <?php
                    include_once('inc_inner_sidebar_iconbuttons.php');
                 ?>
                <!-- end icon buttons -->
            </div>         
            
        </div>
		
        <div class="clearfix">&nbsp;</div>
        

                <!-- icon buttons on bottom -->
                 <?php
                    include_once('inc_inner_bottombar_iconbuttons.php');
                 ?>
                <!-- end icon buttons on bottom-->
<br>
<br>




<!-- footer -->
 <?php
 	include_once('inc_footer.php');
 ?>
<!-- end footer -->








                    
        </div>
        
        
        
    </div>
    </div>
    <!-- end content -->


</div>

<div style="clear:both"></div>







 <?php
 	include_once('inc_bottom_includes.php');
 ?>


   

</body>
</html>
