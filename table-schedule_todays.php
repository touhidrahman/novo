<?php
	require_once('setting/config.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php
 	include_once('inc_top_includes.php');
 ?>
</head>
<body class="inner" style="background:#ccc">
<div class="container"  style="background:#ccc">
<br>
<br>
<br>
<ul>
<li class="r1"><span class="rows"><span class="col1">VQ-941</span><span class="col2">DAC</span><span  class="col_s1"> - </span><span  class="col_s2">JSR</span><span class="col3">07:45</span><span class="col4">Departed</span></span></li>
<li class="r2"><span class="rows"><span class="col1">VQ-901</span><span class="col2">DAC</span><span  class="col_s1"> - </span><span  class="col_s2">CGP</span><span class="col3">08:00</span><span class="col4">Departed</span></span></li>
<li class="r1"><span class="rows"><span class="col1">VQ-942</span><span class="col2">JSR</span><span  class="col_s1"> - </span><span  class="col_s2">DAC</span><span class="col3">08:50</span><span class="col4">Departed</span></span></li>
<li class="r2"><span class="rows"><span class="col1">VQ-903</span><span class="col2">DAC</span><span  class="col_s1"> - </span><span  class="col_s2">CGP</span><span class="col3">09:00</span><span class="col4">On Time</span></span></li>
<li class="r1"><span class="rows"><span class="col1">VQ-902</span><span class="col2">CGP</span><span  class="col_s1"> - </span><span  class="col_s2">DAC</span><span class="col3">09:15</span><span class="col4">Departed</span></span></li>
<li class="r2"><span class="rows"><span class="col1">VQ-904</span><span class="col2">CGP</span><span  class="col_s1"> - </span><span  class="col_s2">DAC</span><span class="col3">10:15</span><span class="col4">On Time</span></span></li>
<li class="r1"><span class="rows"><span class="col1">VQ-905</span><span class="col2">DAC</span><span  class="col_s1"> - </span><span  class="col_s2">CGP</span><span class="col3">10:00</span><span class="col4">Departed</span></span></li>
<li class="r2"><span class="rows"><span class="col1">VQ-933</span><span class="col2">DAC</span><span  class="col_s1"> - </span><span  class="col_s2">CXB</span><span class="col3">10:30</span><span class="col4">Departed</span></span></li>
<li class="r1"><span class="rows"><span class="col1">VQ-906</span><span class="col2">CGP</span><span  class="col_s1"> - </span><span  class="col_s2">DAC</span><span class="col3">11:15</span><span class="col4">Departed</span></span></li>
<li class="r2"><span class="rows"><span class="col1">VQ-934</span><span class="col2">CXB</span><span  class="col_s1"> - </span><span  class="col_s2">DAC</span><span class="col3">11:55</span><span class="col4">Departed</span></span></li>
<li class="r1"><span class="rows"><span class="col1">VQ-951</span><span class="col2">DAC</span><span  class="col_s1"> - </span><span  class="col_s2">ZYL</span><span class="col3">12:45</span><span class="col4">Delayed</span></span></li>
<li class="r2"><span class="rows"><span class="col1">VQ-952</span><span class="col2">ZYL</span><span  class="col_s1"> - </span><span  class="col_s2">DAC</span><span class="col3">13:55</span><span class="col4">Delayed</span></span></li>
<li class="r1"><span class="rows"><span class="col1">VQ-931</span><span class="col2">DAC</span><span  class="col_s1"> - </span><span  class="col_s2">CXB</span><span class="col3">14:30</span><span class="col4">On Time</span></span></li>
<li class="r2"><span class="rows"><span class="col1">VQ-909</span><span class="col2">DAC</span><span  class="col_s1"> - </span><span  class="col_s2">CGP</span><span class="col3">15:20</span><span class="col4">On Time</span></span></li>
<li class="r1"><span class="rows"><span class="col1">VQ-932</span><span class="col2">CXB</span><span  class="col_s1"> - </span><span  class="col_s2">DAC</span><span class="col3">15:55</span><span class="col4">On Time</span></span></li>
<li class="r2"><span class="rows"><span class="col1">VQ-910</span><span class="col2">CGP</span><span  class="col_s1"> - </span><span  class="col_s2">DAC</span><span class="col3">16:35</span><span class="col4">On Time</span></span></li>
<li class="r1"><span class="rows"><span class="col1">VQ-943</span><span class="col2">DAC</span><span  class="col_s1"> - </span><span  class="col_s2">JSR</span><span class="col3">17:30</span><span class="col4">On Time</span></span></li>
<li class="r2"><span class="rows"><span class="col1">VQ-911</span><span class="col2">DAC</span><span  class="col_s1"> - </span><span  class="col_s2">CGP</span><span class="col3">18:00</span><span class="col4">On Time</span></span></li>
<li class="r1"><span class="rows"><span class="col1">VQ-944</span><span class="col2">JSR</span><span  class="col_s1"> - </span><span  class="col_s2">DAC</span><span class="col3">18:35</span><span class="col4">On Time</span></span></li>
<li class="r2"><span class="rows"><span class="col1">VQ-912</span><span class="col2">CGP</span><span  class="col_s1"> - </span><span  class="col_s2">DAC</span><span class="col3">19:15</span><span class="col4">On Time</span></span></li>
<li class="r1"><span class="rows"><span class="col1">VQ-913</span><span class="col2">DAC</span><span  class="col_s1"> - </span><span  class="col_s2">CGP</span><span class="col3">19:45</span><span class="col4">On Time</span></span></li>
<li class="r2"><span class="rows"><span class="col1">VQ-914</span><span class="col2">CGP</span><span  class="col_s1"> - </span><span  class="col_s2">DAC</span><span class="col3">21:00</span><span class="col4">On Time</span></span></li>
<ul>
</div>
<?php
 	include_once('inc_bottom_includes.php');
 ?>
</body>
</html>
