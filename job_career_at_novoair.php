<?php
	require_once('setting/config.php');
	
	require_once('rak_framework/connection.php');
	require_once('rak_framework/listgrabber.php');
	require_once('rak_framework/misfuncs.php');			
	
	
	
	$inputData = array(
	'TableName' => 'job_manager',
	'OrderBy' => 'id',
	'ASDSOrder' => 'DESC',

	'id' => '',
	'job_title' => '',
	'description' => '',
	'application_instruction' => '',
	'deadline' => '>=CURDATE()',
	'entry_date' => '',
	'edit_date' => ''
);

	listData($inputData,$data);
	//print_r($data);


//########### Start paging
if($rowPerPage == 'all'){
	$rowPerPage = '2';
}
else if($rowPerPage)
{
	$rowPerPage = $rowPerPage;
}
else
{
	$rowPerPage = 10;
	}
require_once('rak_framework/pagination.php');		
//########### End paging 
//echo $rowPerPage .'<p>';


	//page parameters
	$section = 'Corporate';
	$pageTitle = 'Career';
	$siteTitle = $siteTitle." : ".$pageTitle;	
?>
<!DOCTYPE html>
<html lang="en">

<head>
 <?php
 	include_once('inc_top_includes.php');
 ?>
</head>
<body class="inner">

    <!-- top nav -->
			 <?php
                include_once('inc_topnav.php');
             ?>     
             
	<!-- end top nav -->  
    
    

<br>
<br>
<br>

<div class="container help-line">
    <div>
        <img src="images/help-line.png" class="hidden-xs">
        <img src="images/help-line2.png" class="visible-xs">
    </div>
</div>


<!-- main navigation -->
			 <?php
                include_once('inc_mainnav.php');
             ?>           
<!-- end main navigation -->





<div class="slide-wrapper">

<!-- inner page Carousel -->
 <?php
    include_once('inc_carousel_inner.php');
 ?>   
<!-- end inner page Carousel -->  


<!-- content -->
    <div class="container">
    
    	<div class="body-container">
    
    
        
         <div class="breadcrumb">
        <div class="row">
          <div  class="col-sm-6">
            <h1><b>Career  </b>  </h1>
          </div>
          <div class="col-sm-6 text-right"> Information / Career</div>
        </div>
      </div>     
        
      


        
        <div class="page-contents">
        

        
        
        
        
        
        

	




        <div class="row">
        	

            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
	         <h2>Take off Your Career With NOVOAIR</h2>
             
	<br>

<p>
<img src="images/page_headers/career.jpg" class="img-responsive">
</p>    
<br> 


<p>We pride ourselves in finding the right  people for the right roles. We rely on the combined skills and efforts of our  existing and future team members to continue to build the success of our  airline. In return we are privileged to offer our employees with excellent  working conditions and competitive salary and benefits package including travel  privileges. </p>

<p>When you work at NOVOAIR, you’re able  to pursue your passion – through creativity &amp; engineering skills, through  your dexterity as a flight crew, through customer satisfaction, through  marketing &amp; sales – turn it into something meaningful, both personally and  professionally. Whether you’re interested in engineering or sales, finance or  customer service or even as a crew, we can give you valuable opportunities with  the highest level of training domestically &amp; in abroad. At NOVOAIR, with  the right qualifications, attitude and commitment, there's almost no limit on  how high you’ll be able to mark on here. Check out our available positions at the  moment for recruitment or you may email us your resume with cover letter &amp;photograph  at <a href="mailto:hr@novoair-bd.com">hr@novoair-bd.com</a>.</p>




<br>
<br>
<h2>Available Positions</h2>





<form>

<?php
if($totalRecords > 0)
{
?>


                
                	<table class="table job-list">
                    	<tr>
                        	<th width="60%">Job Position</th><th  width="20%"> 	Post Date</th><th  width="20%">Deadline</th><th  width="20%">Detail</th>
                        </tr>    
<?php
for ($i=$startIndex; $i<$endIndex; $i++) {
?>						<tr>
                            <td><a href="job_detail.php?id=<?=$data[$i]['id']?>&token=<?=rakHash($data[$i]['id']);?>"><?=$data[$i]['job_title']?></a></td>
                            <td><?=formatDate($data[$i]['entry_date'])?></td>
                            <td><?=formatDate($data[$i]['deadline'])?></td>
                            <td><input type="button" class="btn btn-danger" value="Detail" onClick="location.href='job_detail.php?id=<?=$data[$i]['id']?>&token=<?=rakHash($data[$i]['id'])?>'"></td>
                        </tr>
                        
                        
                        
<?php
	  $nextPage = $pageIndex+1;
	  $prevPage = $pageIndex-1;
	  $IELTSAvarage = '';
  
	  if($on ==1)
	  {
		$on = 0;
	  }
	  else
	  {
		$on = 1;
	  }
}
?>                        
                    
         </table>    
                



<?php
}
else
{
	echo '<div class="clear">&nbsp;</div><div align="center" style="padding:20px;margin-top:20px;border:1px solid #ccc;">No job available now</div>';
}
?> 

            
              
                </form>

   
   
                
              

		
        
    <br>
<br>



  




                
            </div>
            
            <div class="first-col col-lg-3 col-md-3  hidden-sm hidden-xs ">
                <!-- icon buttons -->
                 <?php
                    include_once('inc_inner_sidebar_iconbuttons.php');
                 ?>
                <!-- end icon buttons -->
            </div>            
            
        </div>
		
        <div class="clearfix">&nbsp;</div>
        
                <!-- icon buttons on bottom -->
                 <?php
                    include_once('inc_inner_bottombar_iconbuttons.php');
                 ?>
                <!-- end icon buttons on bottom-->
<br>
<br>




<!-- footer -->
 <?php
 	include_once('inc_footer.php');
 ?>
<!-- end footer -->








                    
        </div>
        
        
        
    </div>
    </div>
    <!-- end content -->


</div>

<div style="clear:both"></div>







 <?php
 	include_once('inc_bottom_includes.php');
 ?>

<script language="javascript">
$(document).ready(function(e) {
	$("#agentForm").submit(function(e){


		if(!IsEmail($("input[name=email]").val()))
		{
			top.$.fn.colorbox({
			html: '<p class="title" style="text-align:center;font-size:12px;padding:10px;">Please enter a valid email address</p>',
				width: "400px;"
			});	
			
						
			$("select[name=email]").focus();
			return false;
			
		}


		if(!$("input[name=phone]").val())
		{
			//alert("Please select passengers");
			
			top.$.fn.colorbox({
			html: '<p class="title" style="text-align:center;font-size:12px;padding:10px;">Please enter a phone number</p>',
				width: "400px;"
			});	
			
						
			$("select[name=phone]").focus();
			return false;
		}



		if(!$("input[name=agent_form]").val())
		{
			//alert("Please select passengers");
			
			top.$.fn.colorbox({
			html: '<p class="title" style="text-align:center;font-size:12px;padding:10px;">Please attach the application form before sending</p>',
				width: "400px;"
			});	
			
						
			$("select[name=agent_form]").focus();
			return false;
		}
		
		
		
	});
});
</script>
   

</body>
</html>
