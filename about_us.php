<?php
	require_once('setting/config.php');
?>
  <!DOCTYPE html>
  <html lang="en">

  <head>
    <?php
 	include_once('inc_top_includes.php');
 ?>
  </head>

  <body class="inner">

    <!-- top nav -->
    <?php
                include_once('inc_topnav.php');
             ?>

      <!-- end top nav -->



      <br>
      <br>
      <br>

      <div class="container help-line">
        <div>
          <img src="images/help-line.png" class="hidden-xs">
          <img src="images/help-line2.png" class="visible-xs">
        </div>
      </div>


      <!-- main navigation -->
      <?php
                include_once('inc_mainnav.php');
             ?>
        <!-- end main navigation -->





        <div class="slide-wrapper">
          <!-- inner page Carousel -->
          <?php
    include_once('inc_carousel_inner.php');
 ?>
            <!-- end inner page Carousel -->



            <!-- content -->
            <div class="container">

              <div class="body-container">



                <div class="breadcrumb">
                  <div class="row">
                    <div class="col-sm-6">
                      <h1><b>Company </b> Information</h1>
                    </div>
                    <div class="col-sm-6 text-right"> About Us / Company Information</div>
                  </div>
                </div>


                <div class="page-contents">

                  <div class="row">


                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                      <h1><b>NOVOAIR </b> - The Art of Aviation</h1>


                      <br>

                      <p>
                        <img src="http://www.novoair-bd.com/images/flynovoair/0.jpg" width="920" height="250" class="img-responsive">
                      </p>
                      <br>
											<p>
												NOVOAIR, the premium Scheduled Passenger Airline in Bangladesh is conducting domestic and international flights with EMB-145 Jet and ATR-72 aircrafts. The experienced management, well trained human resources and corporate institutional and financial strength enable NOVOAIR to initiate and successfully operate its airline with regulatory compliance, safety and service at the top its corporate priority.
											</p>

											<p>
												To know more about NOVOAIR and other sister concerns, please visit the <a href="http://www.novoair-bd.com/novoair.php">Corporate Website</a>.
											</p>

                    </div>

                      <div class="first-col col-lg-3 col-md-3  hidden-sm hidden-xs ">
                        <!-- icon buttons -->
                  <?php
                    include_once('inc_inner_sidebar_iconbuttons.php');
                 ?>
                          <!-- end icon buttons -->
                      </div>


                      <div class="clearfix">&nbsp;</div>

                      <!-- icon buttons on bottom -->
                      <?php
                    include_once('inc_inner_bottombar_iconbuttons.php');
                 ?>
                        <!-- end icon buttons on bottom-->
                        <br>
                        <br>




                        <!-- footer -->
                        <?php
 	include_once('inc_footer.php');
 ?>
                          <!-- end footer -->









                    </div>



                  </div>
                </div>
                <!-- end content -->


              </div>

              <div style="clear:both"></div>







              <?php
 	include_once('inc_bottom_includes.php');
 ?>




  </body>

  </html>
