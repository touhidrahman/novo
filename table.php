<?php
	require_once('setting/config.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php
 	include_once('inc_top_includes.php');
 ?>
</head>
<body class="inner">
<div class="container">
  <table border="0" cellpadding="0" cellspacing="0" class="table table-striped fare-chart">
    <tr>
      <th width="50%">ROUTE</th>
      <th width="25%">FARE TYPE</th>
      <th width="25%">FARE</th>
    </tr>
    <tr>
      <td><strong>Dhaka - Chittagong </strong>& vice versa</td>
      <td colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table"	>
          <tr>
            <td width="50%" rowspan="2">Flexible</td>
            <td>8,200 </td>
          </tr>
          <tr>
            <td>7,600</td>
          </tr>
          <tr>
            <td rowspan="2">Saver</td>
            <td>7,100 </td>
          </tr>
          <tr>
            <td>6,600</td>
          </tr>
          <tr>
            <td rowspan="2">Discounted</td>
            <td>6,000 </td>
          </tr>
          <tr>
            <td>5,500</td>
          </tr>
          <tr>
            <td rowspan="2">Special</td>
            <td>5,000 </td>
          </tr>
          <tr>
            <td>4,700</td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td><strong>Dhaka - Cox’s Bazar</strong> & vice versa</td>
      <td colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table"	>
          <tr>
            <td width="50%" rowspan="2">Flexible</td>
            <td>9,200</td>
          </tr>
          <tr>
            <td>8,600</td>
          </tr>
          <tr>
            <td rowspan="2">Saver</td>
            <td>8,100 </td>
          </tr>
          <tr>
            <td>7,500</td>
          </tr>
          <tr>
            <td rowspan="2">Discounted</td>
            <td>6,800 </td>
          </tr>
          <tr>
            <td>6,400</td>
          </tr>
          <tr>
            <td rowspan="2">Special</td>
            <td>6,000 </td>
          </tr>
          <tr>
            <td>5,700</td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td><strong>Dhaka - Sylhet</strong> & vice versa</td>
      <td colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table"	>
          <tr>
            <td width="50%" rowspan="2">Flexible</td>
            <td>7,200 </td>
          </tr>
          <tr>
            <td>6,500</td>
          </tr>
          <tr>
            <td rowspan="2">Saver</td>
            <td>5,900 </td>
          </tr>
          <tr>
            <td>5,500</td>
          </tr>
          <tr>
            <td rowspan="2">Discounted</td>
            <td>5,000 </td>
          </tr>
          <tr>
            <td>4,400</td>
          </tr>
          <tr>
            <td rowspan="2">Special</td>
            <td>4,000 </td>
          </tr>
          <tr>
            <td>3,700</td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td><strong>Dhaka - Jessore</strong> & vice versa</td>
      <td colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table"	>
          <tr>
            <td width="50%" rowspan="2">Flexible</td>
            <td>6,700 </td>
          </tr>
          <tr>
            <td>6,100</td>
          </tr>
          <tr>
            <td rowspan="2">Saver</td>
            <td>5,600 </td>
          </tr>
          <tr>
            <td>5,000</td>
          </tr>
          <tr>
            <td rowspan="2">Discounted</td>
            <td>4,500</td>
          </tr>
          <tr>
            <td>4,200</td>
          </tr>
          <tr>
            <td rowspan="2">Special</td>
            <td>4,000 </td>
          </tr>
          <tr>
            <td>3,700</td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
<?php
 	include_once('inc_bottom_includes.php');
 ?>
</body>
</html>
