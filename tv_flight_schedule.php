<?php 
	date_default_timezone_set('Asia/Dhaka');
	require_once "tv_rss_data_parser.php";
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
	body{
		margin:0;
		padding:0;
}
	<?php
		$popWinBoxWidth = '1000';
		$popWinBoxHeight = '483';
	?>
	.tv_frame{
		/*
		width:500px;
		height:298px;
		*/
		width:<?=$popWinBoxWidth?>px;
		height:<?=$popWinBoxHeight?>px;
		
		background:#fff;
}

#news-container
{
	width: <?=$popWinBoxWidth?>px!important; 
/*	height:379px!important;*/
	height:501px!important;
	margin-top: 0px!important;
	border: 0px solid #333333;
	
}

#news-container ul
{
	padding-left:0px;
	width:<?=$popWinBoxWidth?>px;
	border:0;
}


#news-container ul li{
	display:block; 
	font-family:arial;
	font-size:30px;
	color:#000;
	pad ding-top:5px!important;
	border-top:5px solid #000000;
	background:#808284;

}
#news-container ul li:nth-child(even){background: #414142; color:#fff;}

.rows{
	display:block;
	padding:11px 0px;
}

.rows .col1{padding-left:58px;}
.rows .col2{margin-left:95px;}
.rows .col_s1{margin-left:25px;}
.rows .col_s2{margin-left:25px;}
.rows .col3{margin-left:133px;}
.rows .col4{margin-left:110px;}


.header .h_rows{
	text-transform:uppercase;
	color:#9bcde9;
	font-size:30px;
	display:block;
	padding-top:100px;
}


.header .h_rows{padding-left:58px;}
.header .h_rows .h_col2{margin-left:102px;}
.header .h_rows .h_col_s1{margin-left:58px;}
.header .h_rows .h_col3{margin-left:158px;}
.header .h_rows .h_col4{margin-left:120px;}



.screen2{

	height:200px;
	border:0px solid #CCC;
	margin-top:0px;
	}
	
.screen_top{
	color:#9bcde9;
	position:absolute;
	text-align:center;
	width:<?=$popWinBoxWidth?>px;
	padding-top:12px;
	font-size:40px;
}
.header{
	background: #00447b; /* Old browsers */
	background: -moz-linear-gradient(left,  #00447b 1%, #00447b 68%, #00bbde 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, right top, color-stop(1%,#00447b), color-stop(68%,#00447b), color-stop(100%,#00bbde)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(left,  #00447b 1%,#00447b 68%,#00bbde 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(left,  #00447b 1%,#00447b 68%,#00bbde 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(left,  #00447b 1%,#00447b 68%,#00bbde 100%); /* IE10+ */
	background: linear-gradient(to right,  #00447b 1%,#00447b 68%,#00bbde 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00447b', endColorstr='#00bbde',GradientType=1 ); /* IE6-9 */
	height:135px;
}
.footer{

background: #f8941e; /* Old browsers */
background: -moz-linear-gradient(left,  #f8941e 0%, #b72f2a 43%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, right top, color-stop(0%,#f8941e), color-stop(43%,#b72f2a)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(left,  #f8941e 0%,#b72f2a 43%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(left,  #f8941e 0%,#b72f2a 43%); /* Opera 11.10+ */
background: -ms-linear-gradient(left,  #f8941e 0%,#b72f2a 43%); /* IE10+ */
background: linear-gradient(to right,  #f8941e 0%,#b72f2a 43%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f8941e', endColorstr='#b72f2a',GradientType=1 ); /* IE6-9 */

height:44px;
border-top:5px solid #000000;

}
</style>

<script src="js/jquery.min.js" type="text/javascript"></script> 
<script type="text/javascript" src="js/plugins/virtical_ticker/jquery.vticker-min.js"></script>
<script type="text/javascript">
$(function(){
	$('#news-container').vTicker({ 
		speed: 500,
		pause: 3000,
		animation: '',
		mousePause: true,
		showItems: 8
	});
});
</script>

<!-- common include area -->

    
    <!--font-solution -->
	<script type="text/javascript" src="js/plugins/cufon_yui/cufon-yui.js"></script>
    <script type="text/javascript" src="js/plugins/cufon_yui/fonts/bebas.cufonfonts.js"></script>
    <script type="text/javascript" src="js/plugins/cufon_yui/fonts/titillium-text.cufonfonts.js"></script>
	<script type="text/javascript">
            //<![CDATA[
						Cufon.replace('.screen_top', { fontFamily: 'Bebas', hover: true });
						Cufon.replace('.header .h_rows', { fontFamily: 'TitilliumText22L-Medium', hover: true });

              //]]>
              </script>
	<!-- end font-solution -->  


<!-- auto page refresh -->
<script type="text/JavaScript">
<!--
function AutoRefresh(interval) {
	setTimeout("location.reload(true);",interval);
}
//   -->
</script>




</head>
<body scroll="0" onload="JavaScript: AutoRefresh(300000);">

<div class="tv_frame">

    <div class="screen_top" style="font-size:50px!important; padding-top:25px;">
       
        
                        <!--sun  Nov 11-->
                        <?php
                        	//echo date("D    M    d");
							/*
							echo date("D").' &nbsp;&nbsp;&nbsp;';
							echo date("M").' &nbsp;&nbsp;&nbsp;';
							echo date("d");
							*/
							echo date("l");
							//echo 'Flight Schedule';
						?>       
    </div>
	<div class="screen2">
            <div class="header">
                <!--img src="images/screen_top_large.png"  /-->
                <span class="h_rows"><span class="h_col1">FLIGHT</span><span class="h_col2">FROM</span><span class="h_col_s1">TO</span><span class="h_col3">TIME</span><span class="h_col4">STATUS</span></span>
            </div>
                    
                    
            <div id="news-container">

                

<!-- rss contents -->



<?php parserSide("http://secure.flynovoair.com/rss", "status"); ?>         





<!-- end rss contents -->
 
            <div style="clear:both"></div>
            </div>        
        
       
       
       <div class="footer"> 
       	
       </div>
        
        </div>




</div>
</body>
</html>
