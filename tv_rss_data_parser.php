<?php
function parserSide($feedURL, $status = null) {
	
	$rss = simplexml_load_file($feedURL);
	echo '<ul>';
	

		foreach ($rss->channel->item as $feedItem)
		{
			
			if($feedItem->DepartureStatus != 'Cancelled')
			{
				echo '<li>';
				echo '<span class="rows">';
				echo '<span class="col1">'.$feedItem->FlightNumber.'</span>';
				echo '<span class="col2">'.$feedItem->Origin.'</span><span  class="col_s1"> - </span><span  class="col_s2">'.$feedItem->Destination.'</span>';
				echo '<span class="col3">'.$feedItem->STD.'</span>';
				if($status != null)
				{
					echo '<span class="col4">'.$feedItem->DepartureStatus.'</span>';
				}
				echo '</span>';
				echo '</li>';
			}
	
		}
	
	
	echo '</ul>';
}

?>