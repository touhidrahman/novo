<div class="navbar yamm navbar-default primary-menu">
	<div class="container">
		<div class="navbar-header">
			<button type="button" data-toggle="collapse" data-target="#navbar" class="navbar-toggle">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php">
				<img src="images/logo-novoair.png" class="img-responsive">
			</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse" aria-expanded="false" aria-controls="navbar">
			<ul class="nav navbar-nav navbar-right nav-pills">
				<!-- Classic list -->
				<li class="active dropdown yamm-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Plan A Trip<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li>
							<!-- Content container to add padding -->
							<div class="yamm-content">
								<div class="row">
									<div class="col-sm-3">
										<p class="menu-icon text-center novoblue">
											<span class="fa fa-plane"></span>
										</p>
										<p class="text-center"><a class="btn btn-default btn-lg btn-block novoblue" href="https://secure.flynovoair.com/bookings/flight_selection.aspx">Book Flights</a></p>
									</div>
									<div class="col-sm-3">
										<p class="menu-icon text-center  novoblue">
											<span class="fa fa-suitcase"></span>
										</p>
										<p class="text-center"><a class="btn btn-default btn-lg btn-block  novoblue" href="#">Book Holiday Package</a></p>
									</div>
									<div class="col-sm-3">
										<p class="menu-icon text-center  novoblue">
											<span class="fa fa-hotel"></span>
										</p>
										<p class="text-center"><a class="btn btn-default btn-lg btn-block  novoblue" href="#">Book Hotel</a></p>
									</div>
									<div class="col-sm-3">
										<ul class="list-group">
											<li class="list-group-item">
												<a class=" novoblue" href="group_booking.php">
													<span class="fa fa-users"></span> Group Booking (8+ Traveller)</a>
											</li>
											<!-- <li class="list-group-item">
												<a class=" novoblue" href="">
													<span class="fa fa-car"></span> Car/Taxi Reservation</a>
											</li> -->
											<li class="list-group-item">
												<a class=" novoblue" href="https://secure.flynovoair.com/bookings/retrieve_reservation.aspx">
													<span class="fa fa-sliders"></span> Manage Booking</a>
											</li>
										</ul>
									</div>

								</div>
							</div>
						</li>
					</ul>
				</li>

				<!-- TRAVEL INFO -->
				<li class="dropdown yamm-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Travel Info<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li>
							<!-- Content container to add padding -->
							<div class="yamm-content">
								<div class="row">
									<div class="col-sm-3">
										<p class="menu-icon text-center novoblue">
											<span class="fa fa-paper-plane"></span>
										</p>
										<p class="text-center"><a class="btn btn-default btn-lg btn-block novoblue" href="">Destinations</a></p>
										<p>
											<strong class="text-muted">International</strong>
											<ul class="list-unstyled">
												<li><a href="#">Yangon</a></li>
												<li><a href="#">Future Destinations</a></li>
											</ul>
										</p>
										<p>

											<strong class="text-muted">Domestic</strong>
											<ul class="list-unstyled">
												<li><a href="#">Dhaka</a></li>
												<li><a href="#">Chittagong</a></li>
												<li><a href="#">Cox's Bazar</a></li>
												<li><a href="#">Sylhet</a></li>
												<li><a href="#">Jessore</a></li>
												<li><a href="#">Rajshahi</a></li>
												<li><a href="#">Barisal</a></li>
												<li><a href="#">Syedpur</a></li>
											</ul>
										</p>
									</div>


									<div class="col-sm-3">
										<p class="menu-icon text-center novoblue">
											<span class="fa fa-binoculars"></span>
										</p>
										<p class="text-center"><a class="btn btn-default btn-lg btn-block novoblue" href="#">Tourist Information</a></p>



										<p>
											<strong class="text-muted">Top Places to See</strong>
											<ul class="list-unstyled">
												<li><a href="places_to_see/dhaka.php">Dhaka</a></li>
												<li><a href="places_to_see/coxsbazar.php">Cox's Bazar</a></li>
												<li><a href="#">Sundarban</a></li>
												<li><a href="#">Bandarban</a></li>
												<li><a href="#">Sylhet</a></li>
												<li><a href="#">Kuakata</a></li>
												<li><a href="#">St. Martin Island</a></li>
												<li></li>
												<li>
													<strong><a href="#">View More</a></strong>
												</li>
											</ul>
										</p>
										<p>
											<strong class="text-muted">Tips and Guides</strong>
											<ul class="list-unstyled">
												<li><a href="#">Transportation</a></li>
												<li><a href="#">General Guidelines</a></li>
												<li><a href="#">Media Gallery</a></li>
											</ul>
										</p>



									</div>


									<div class="col-sm-3">
										<p class="menu-icon text-center novoblue">
											<span class="fa fa-suitcase"></span>
										</p>
										<p class="text-center"><a class="btn btn-default btn-lg btn-block novoblue" href="">Other Information</a></p>
										<p>
											<strong class="text-muted">Baggage Information</strong>
											<ul class="list-unstyled">
												<li><a href="#">Baggage Size</a></li>
												<li><a href="#">Prohibited Items</a></li>
												<li><a href="pets.php">Pets/Animal (AVIH)</a></li>
												<li><a href="sporting_equipments.php">Sports Gears</a></li>
												<li><a href="sporting_equipments.php?#firearms">Fire arms and Ammunition</a></li>
												<li><a href="#">Life Saving/Medical Equipments</a></li>
											</ul>
										</p>
										<p>
											<strong class="text-muted">Policies</strong>
											<ul class="list-unstyled">
												<li><a href="#">Unaccompanied Minor/Young Passengers</a></li>
												<li><a href="#">Unaccepted Travellers</a></li>
												<li><a href="#">Expected Mother/Pregnant Traveller</a></li>
												<li><a href="#">Infant</a></li>
											</ul>
										</p>
									</div>

									<div class="col-sm-3">
										<ul class="list-group">
											<li class="list-group-item">
												<a class=" novoblue" href="">
													<span class="fa fa-calendar"></span> Flight Schedule</a>
											</li>
											<li class="list-group-item">
												<a class=" novoblue" href="">
													<span class="fa fa-clock-o"></span> Flight Status</a>
											</li>
											<li class="list-group-item">
												<a class=" novoblue" href="">
													<span class="fa fa-map"></span> Route Map</a>
											</li>
											<li class="list-group-item">
												<a class=" novoblue" href="">
													<span class="fa fa-usd"></span> Fare Search</a>
											</li>
										</ul>

									</div>



								</div>
							</div>
						</li>
					</ul>
				</li>

				<!-- Services -->
				<li class="dropdown yamm-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Services<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li>
							<div class="yamm-content">
								<div class="row">
									<div class="col-sm-9">
										<div class="row">
											<div class="col-sm-4">
												<p class="menu-icon text-center novoblue">
													<span class="fa fa-globe"></span>
												</p>
												<p class="text-center"><a class="btn btn-default btn-lg btn-block novoblue" href="">Visa Services</a></p>
											</div>
											<div class="col-sm-4">
												<p class="menu-icon text-center  novoblue">
													<span class="fa fa-fighter-jet"></span>
												</p>
												<p class="text-center"><a class="btn btn-default btn-lg btn-block  novoblue" href="smiles.php">Smiles Frequent Flyer</a></p>
											</div>
											<div class="col-sm-4">
												<p class="menu-icon text-center  novoblue">
													<span class="fa fa-ship"></span>
												</p>
												<p class="text-center"><a class="btn btn-default btn-lg btn-block  novoblue" href="#">Holiday Packages</a></p>
											</div>
										</div>

										<!-- 2nd Row -->
										<div class="row">
											<div class="col-sm-4">
												<p class="menu-icon text-center novoblue">
													<span class="fa fa-rocket"></span>
												</p>
												<p class="text-center"><a class="btn btn-default btn-lg btn-block novoblue" href="services/charter.php">Charter a Flight</a></p>
											</div>
											<div class="col-sm-4">
												<p class="menu-icon text-center  novoblue">
													<span class="fa fa-truck"></span>
												</p>
												<p class="text-center"><a class="btn btn-default btn-lg btn-block  novoblue" href="cargo_services.php">Cargo Services</a></p>
											</div>
											<!-- <div class="col-sm-3">
												<p class="menu-icon text-center  novoblue">
													<span class="fa fa-hotel"></span>
												</p>
												<p class="text-center"><a class="btn btn-default btn-lg btn-block  novoblue" href="#">Holiday Packages</a></p>
											</div> -->
										</div>

									</div>

									<div class="col-sm-3">
										<ul class="list-group">
											<li class="list-group-item">
												<a class=" novoblue" href="">
													<span class="fa fa-cutlery"></span> In-flight Comforts</a>
											</li>
											<li class="list-group-item">
												<a class=" novoblue" href="">
													<span class="fa fa-wheelchair"></span> Special Assistance</a>
											</li>
											<li class="list-group-item">
												<a class=" novoblue" href="">
													<span class="fa fa-map-marker"></span> Locate Sales Offices</a>
											</li>
											<li class="list-group-item">
												<a class=" novoblue" href="">
													<span class="fa fa-money"></span> Fare Search</a>
											</li>
										</ul>
									</div>



								</div>
							</div>
						</li>
					</ul>
				</li>


				<!-- ABOUT US -->
				<li class="dropdown yamm-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">About Us<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li>
							<div class="yamm-content">
								<div class="row">
									<div class="col-sm-9">

										<div class="row">
											<div class="col-sm-3">
												<p class="menu-icon text-center novoblue">
													<span class="fa fa-phone"></span>
												</p>
												<p class="text-center"><a class="btn btn-default btn-lg btn-block novoblue" href="contact.php">Contact Us</a></p>
											</div>
											<div class="col-sm-3">
												<p class="menu-icon text-center  novoblue">
													<span class="fa fa-comments-o"></span>
												</p>
												<p class="text-center"><a class="btn btn-default btn-lg btn-block  sub_window novoblue" href="javascript:void(0)" path="http://www.flynovoair.com/webim/client.php" width="650" height="560">Live Chat</a></p>
											</div>
											<div class="col-sm-3">
												<p class="menu-icon text-center  novoblue">
													<span class="fa fa-envelope"></span>
												</p>
												<p class="text-center"><a class="btn btn-default btn-lg btn-block  novoblue" href="#">Send Feedback</a></p>
											</div>
											<div class="col-sm-3">
												<p class="menu-icon text-center  novoblue">
													<span class="fa fa-question-circle"></span>
												</p>
												<p class="text-center"><a class="btn btn-default btn-lg btn-block  novoblue" href="faq.php">FAQ</a></p>
											</div>
										</div>

										<!-- 2nd Row -->
										<div class="row">
											<div class="col-sm-3">
												<p class="menu-icon text-center novoblue">
													<span class="fa fa-facebook"></span>
												</p>
												<p class="text-center"><a class="btn btn-default btn-lg btn-block novoblue" href="http://www.facebook.com/flynovoair">Facebook</a></p>
											</div>
											<div class="col-sm-3">
												<p class="menu-icon text-center  novoblue">
													<span class="fa fa-youtube"></span>
												</p>
												<p class="text-center"><a class="btn btn-default btn-lg btn-block  novoblue" href="https://www.youtube.com/c/flynovoair">Youtube</a></p>
											</div>
											<div class="col-sm-3">
												<p class="menu-icon text-center  novoblue">
													<span class="fa fa-google-plus"></span>
												</p>
												<p class="text-center"><a class="btn btn-default btn-lg btn-block  novoblue" href="https://plus.google.com/+Flynovoair">Google+</a></p>
											</div>
											<div class="col-sm-3">
												<p class="menu-icon text-center  novoblue">
													<span class="fa fa-twitter"></span>
												</p>
												<p class="text-center"><a class="btn btn-default btn-lg btn-block  novoblue" href="https://twitter.com/NOVOAIR">Twitter</a></p>
											</div>
										</div>

									</div>

									<div class="col-sm-3">
										<ul class="list-group">
											<li class="list-group-item">
												<a class=" novoblue" href="company_info.php">
													<span class="fa fa-building-o"></span> Company Information</a>
											</li>
											<li class="list-group-item">
												<a class=" novoblue" href="">
													<span class="fa fa-space-shuttle"></span> Fleet Information</a>
											</li>
											<li class="list-group-item">
												<a class=" novoblue" href="">
													<span class="fa fa-newspaper-o"></span> Latest News And Media</a>
											</li>
											<li class="list-group-item">
												<a class=" novoblue" href="terms.php">
													<span class="fa fa-check-square-o"></span> Terms & Conditions</a>
											</li>
											<li class="list-group-item">
												<a class=" novoblue" href="">
													<span class="fa fa-certificate"></span> Privacy Policy</a>
											</li>
										</ul>
									</div>



								</div>
							</div>
						</li>
					</ul>
				</li>


			</ul>
		</div>
	</div>
</div>
