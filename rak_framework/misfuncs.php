<?php

function rakHash($id){
	global $secret;
	$hash = hash_hmac('sha256', $id, $secret);
	return $hash;
}

function chkRakHash($id, $hash){
	global $secret;
	$str1 = hash_hmac('sha256', $id, $secret);
	$str2 = $hash;
	
	/*
	if(hash_equals($str1, $str2)){
			return true;
		}else{
			return false;
		}
	*/
	
	// if hash_equals is not available in older php;
	
	//r-if(!function_exists('hash_equals'))
	//r-{
		//r-function hash_equals($str1, $str2)
		//r-{
			if(strlen($str1) != strlen($str2))
			{
				return false;
			}
			else
			{
				//return true;
				
				$res = $str1 ^ $str2;
				$ret = 0;
				for($i = strlen($res) - 1; $i >= 0; $i--)
				{
					$ret |= ord($res[$i]);
				}
				return !$ret;
				
			}
		//r-}
	//}


}

function formatDate($str)
{
	if (preg_match ("/ /", $str)) 
	{ 
   		$str = explode(" ",$str);
		$str = $str[0]; 
	} 
	$formattedDate = explode('-',$str);
	
	//return as 15 Jan 2008
	return $formattedDate[2].' '.date ("M", mktime (0,0,0,$formattedDate[1],1,1)).' '.$formattedDate[0];
	
}



function formatDateReverse($str)
{

   		$formattedDate = explode("/",$str);

	
	return $formattedDate[2].'-'.$formattedDate[1].'-'.$formattedDate[0];
	
}

function formateDateToMySql($str)
{
	return date("Y-m-d", strtotime($str));
}

function formatDateAndTIme($str)
{
	return date("d M Y, g:i a", strtotime($str));
}



function formatDate2($str)
{
	if (preg_match ("/ /", $str)) 
	{ 
   		$str = explode(" ",$str);
		$str = $str[0]; 
	} 
	$formattedDate = explode('-',$str);
	
	//return as 15 Jan 2008
	if($str){
	return $formattedDate[2].'/'.$formattedDate[1].'/'.$formattedDate[0];
	}else
	{
		return '';
		}
	
}





function formatUrl($url) {

    if(!(strpos($url, "http://") === 0)
	&& !(strpos($url, "https://") === 0)) {

        $url = "http://$url";
	
	}
	
    return $url;
	//echo $url;

}




function arrayToString($array, $separator){
		foreach ($array as $key => $value) { 
			$string .= $value.$separator; 
		}
		return $string;
	}

function getFileExtension($file_name){
	$file_type = explode(".",$file_name);
	$n = count($file_type);
	$n =  $n-1;
	return $file_type[$n];
}

// This function will keep track of all transaction between mother banks and other accounts.
// it will update the log and reflect the mother bank;
// Version 1.0
// Date: Aug31-08
// Developer: Raihan A.K.

/* Transaction Points:
	1. Primary Share
	-------------------
		* Apply Share
		* Edit Applied Share
		* Share Full Rejection
		* Share Partially Rejected
		* Undo Rejection Share
		* Delete a share lot
	2. Secondary Share
		* Share Buy
		* Share Sell
		* Share Edit
*/
function recordTransaction($desc,$amount,$type,$trnsDate,&$msg)
{
	global $date;
	//fetch balance from mother bank acc fetchByID($tblName,$QueriedByID,$QueriedByIDValue,$ReturnValue)
	$currentBalance	= fetchByID('tbl_mother_bank_information','user_id',$_SESSION['USER_ID'],'current_balance');
	
	//reflect Mother Bank ; values 'dr', 'cr';
	if($type == 'cr')
	{
		$updatedBalance = $currentBalance+$amount;
	}
	else
	{
		$updatedBalance = $currentBalance-$amount;
	}
	

	
	
	if($trnsDate)
	{
		$trnsDate = $trnsDate;
	}
	else
	{
		$trnsDate = $date;
	}
	
	//insert data to tbl_transaction_history table
	 global $dbname;
	 
	 mysql_select_db($dbname);

	//insert
	$str = 'INSERT INTO tbl_transaction_history (description,user_id,amount_'.$type.',current_balance,date_of_trns) VALUES ("'.$desc.'","'.$_SESSION['USER_ID'].'","'.$amount.'", "'.$updatedBalance.'","'.$trnsDate.'")';
	
	$result = mysql_query($str)
	
	or die("insert recordTransaction error: " . mysql_error());
	
	if($result)
	{
		$msg = '<span class="message"><b><font face="arial" size=2 color=green>Data successfully added</b></font></span>';
	}
	else
	{
		$msg = '<span class="message"><b><font face="arial" size=2 color=red>Could not add data</b></font></span>';
	}
	


	//update Mother Bank
	$condition = 'user_id = '.$_SESSION['USER_ID'];
	
	updateByID('tbl_mother_bank_information','current_balance',$updatedBalance,$condition);

}

function formatTime($str)
{
	if (preg_match ("/ /", $str)) 
	{ 
   		$str = explode(" ",$str);
		$str = $str[1]; 
	} 
	//$formattedDate = explode('-',$str);
	return $str; //$formattedDate[2].'/'.$formattedDate[1].'/'.$formattedDate[0];
}

function getYearOnly($str)
{
	if (preg_match ("/ /", $str)) 
	{ 
   		$str = explode(" ",$str);
		$str = $str[0]; 
	} 
	$formattedDate = explode('-',$str);
	//return $formattedDate[2].'/'.$formattedDate[1].'/'.$formattedDate[0];
	return $formattedDate[0];
	//echo $formattedDate[0];
}
function getMonthOnly($str)
{
	if (preg_match ("/ /", $str)) 
	{ 
   		$str = explode(" ",$str);
		$str = $str[0]; 
	} 
	$formattedDate = explode('-',$str);
	//return $formattedDate[2].'/'.$formattedDate[1].'/'.$formattedDate[0];
	return $formattedDate[1];
	//echo $formattedDate[0];
}

function getDayOnly($str)
{
	if (preg_match ("/ /", $str)) 
	{ 
   		$str = explode(" ",$str);
		$str = $str[0]; 
	} 
	$formattedDate = explode('-',$str);
	//return $formattedDate[2].'/'.$formattedDate[1].'/'.$formattedDate[0];
	return $formattedDate[2];
	//echo $formattedDate[0];
}

function phpMaillerAltMsg($message)
{
	$altMessage = str_replace("<br>", "\n", $message);
	$altMessage = str_replace("<b>", "", $altMessage);
	$altMessage = str_replace("</b>", "", $altMessage);
	return $altMessage;
}

function sendEmail($userID,$email,$pwd, &$msg)
{
	$headers  = "MIME-Version: 1.0\r\n";
	$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
	
	$to = $email;
	$fromMail = 'mail@eshna.com';
	//$to = 'info@allpestcontrol.ca';
	$subject = 'Welcome to Eshna';
	$body = '
	<font face=arial>
  Hello '.$userID.',<br><br>
	Thank you for your interest with us. You have successfully registered and submitted your resume.<br><br>

	Following is you account information to access your user panel to edit your resume.<br><br>

	User name: '.$userID.'<br>
	Password:'.$pwd.'
	<br><br>
	Soon you will hear from us.
	<br><br>
	Thank you
	<br><br>
	Eshna Web Team
  </font>
	';
	//echo $body;

	$headers .= "From: Eshna Account Info <".$fromMail.">\r\n";

	
	/* and now mail it */

	if(mail($to, $subject, $body, $headers))
	{
		$msg = '<font face=arial color=green size=2><b>Mail send successfully</font>';
	}
	else
	{
		$msg = '<font face=arial color=red size=2><b>Problem occured. Could not send mail.</b></font>';
	}
}
//#### Function for fetching csv data from other server;
function extrair_url_fopen ($url) 
{ 
	$contents = file_get_contents($url);
	return $contents;
}

//#### Function for dumping updated rate to dump/data.csv file;
function writeCSV($contents)
{
	$filename = 'dump/data.csv';
	$fp = fopen($filename, "w");
	@chmod ($filename, 0777);
	$write = fputs($fp, $contents);
	fclose($fp);
}
//#### Function for writing error log file;
function writeLog($errTxt)
{
	$filename = 'log.txt';
	$fp = fopen($filename, "a");
	@chmod ($filename, 0777);
	$newErr = date("F j, Y, g:i a")." - ".$errTxt."\n";
	$write = fputs($fp, $newErr);
	fclose($fp);
}

function returnYears(&$fromYear,&$toYear)
{
	$fromYear = date("Y")-$fromYear;
	$toYear = date("Y")-$toYear;
}

function returnAge($years)
{

	if($years != '0000'){
	$years = date("Y")-$years;
	return $years;
	}else{
	 $years = 0;
	 return $years;
	}
}



?>