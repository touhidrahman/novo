<?php
/**
 * ----------------------------------------------
 * RAK FRAMEWORK
 * Version 1.1
 * Last Updated: Jul15-08
 * Developer: Md. Abul Kashem (Raihan)
 * Email: raihanak@yahoo.com
 * ----------------------------------------------
 */
function fetchData($inputData,&$outputData)
{
	
	global $dbname;
	mysql_select_db($dbname);
	 
	$str = 'SELECT * FROM '.$inputData[TableName].' WHERE '.$inputData[FetchByKey].'='.$inputData[FetchByValue].'';
	//echo $str.'<p>';

	
	$inputData = array_slice($inputData, 3); //removed 1st 2 'TableName','FetchByKey','FetchByValue' elements from the array;
	$result = mysql_query($str)
	or die("Invalid query: " . mysql_error());
	
	
	while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) 
	{ 
		$dbCols = 0;
		foreach($inputData as $key => $value)
		{
			$outputData[$key] = $row[$key];
			$dbCols++;
		}
	 }
}

//fetchByID('occupation_list','occupation_id',$randomData[0]['present_proffession'],'name')
function fetchByID($tblName,$QueriedByID,$QueriedByIDValue,$ReturnValue)
{
	global $dbname;
	mysql_select_db($dbname);

	$str = 'SELECT '.$ReturnValue.' FROM '.$tblName.' WHERE '.$QueriedByID.'="'.$QueriedByIDValue.'"';
	//echo $str;
	$result = mysql_query($str);
	$outputData = mysql_fetch_array($result);
	return $outputData[0];
}


function fetchByCondition($tblName,$codition,$WantedField)
{
	global $dbname;
	mysql_select_db($dbname);
	 
	$str = 'SELECT '.$WantedField.' FROM '.$tblName.' WHERE '.$codition;

	//echo $str;

	$result = mysql_query($str);
	$outputData = mysql_fetch_array($result);
	return $outputData[0];

}


function fetchMaxRecord($tblName,$WantedField)
{
	global $dbname;
	mysql_select_db($dbname);
	 
	$str = 'SELECT `'.$WantedField.'` FROM `'.$tblName.'` ORDER BY `'.$WantedField.'` DESC ';
	
	$result = mysql_query($str)
	or die("Invalid query: " . mysql_error());
	$outputData = mysql_fetch_array($result);
	
	return $outputData[0]+1;
	
}


function fetchMaxRecord_old($inputData,&$outputData)
{
	
	global $dbname;
	mysql_select_db($dbname);
	 
	$str = 'SELECT MAX('.$inputData[KeyToBeFetched].')+1 FROM '.$inputData[TableName];
	//echo $str.'<p>';

	
	$inputData = array_slice($inputData, 1); //removed 1st 2 'TableName','FetchByKey','FetchByValue' elements from the array;
	$result = mysql_query($str)
	or die("Invalid query: " . mysql_error());
	
	$outputData = mysql_fetch_array($result);
	$outputData = $outputData[0];
	//$outputData = str_pad($outputData, 4, "0", STR_PAD_LEFT);
}
function fetchTotalRecord($tblName,$QueriedByID,$QueriedByIDValue)
{
	global $dbname;
	mysql_select_db($dbname);

	$str = 'SELECT * FROM '.$tblName.' WHERE '.$QueriedByID.'='.$QueriedByIDValue;
	//echo $str;
	$result = mysql_query($str);
	return mysql_num_rows($result);
}

//fetchTotalRecordByCondition('tbl_album','UserID = "'.$uid.'" AND show_in_mycontent = 1','show_in_mycontent');
function fetchTotalRecordByCondition($tblName,$codition,$WantedField)
{
	global $dbname;
	mysql_select_db($dbname);
	 
	$str = 'SELECT '.$WantedField.' FROM '.$tblName.' WHERE '.$codition;
	//echo $str;
	$result = mysql_query($str)
	or die("fetchTotalRecordByCondition: " . mysql_error());
	


	$outputData[0] = mysql_num_rows($result);
	
	return $outputData[0];

}



function fetchTotalSum($tblName,$colName,$condition)
{
	global $dbname;
	mysql_select_db($dbname);
	 
	//$str = 'SELECT  SUM('.$colName.') AS "total" FROM '.$tblName.' WHERE '.$codition;
	$str = 'SELECT SUM('.$colName.') AS "total" FROM '.$tblName.' WHERE '.$condition; 
	//echo $str.'<br>';
	$result = mysql_query($str)
	or die("fetchTotalRecordByCondition: " . mysql_error());

	//return mysql_fetch_row['total'];
	while ($row = mysql_fetch_assoc($result))
	{
 		return $row["total"];
	}

	
}

function fetchAdminEmails(&$EmailAddresses)
{
			global $dbname;
			 
			 mysql_select_db($dbname);
			 

		 	$str	= 'SELECT Email from tbl_admin';
			 
			  $result = mysql_query($str);
			  
			   while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) 
			   { 
				$EmailAddresses .= $row["Email"].', ';
			   }
			   $EmailAddresses = substr($EmailAddresses,0, strlen($EmailAddresses)-2);
}
?>