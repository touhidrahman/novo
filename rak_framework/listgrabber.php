<?php
/**
 * ----------------------------------------------
 * RAK FRAMEWORK
 * Version 1.3
 * Last Updated: May 05, 2010
 * Last Updated: Feb 24, 2010
 * Last Updated: March 23, 2009
 
 
 * Developer: Md. Abul Kashem (Raihan)
 * Email: raihan@rakplanet.com
 * ----------------------------------------------
 
  Change info on version 1.2;
  1. searchData($inputData,&$outputData); added
  
  
  Change info on version 1.3;
  1. listJointData($inputData,&$outputData); added
 
 	Sample Inner Joint query;
	
 	SELECT * 
	FROM tbl_lost_bin
	INNER JOIN cmb_category ON tbl_lost_bin.category_id = cmb_category.id
	ORDER BY  `cmb_category`.`name` DESC 
	LIMIT 0 , 30
	
	sample use of listJointData

	$inputLostData = array(
	'TableName' => 'tbl_lost_bin',
	'OrderBy' => $OrderBy,
	'ASDSOrder' => $ASDSOrder,
	'InnerJoint' => 'cmb_category',
	'On' => 'tbl_lost_bin.category_id = cmb_category.id',
	
	
	'tbl_lost_bin.id' => '',
	'tbl_lost_bin.user_id' => '',
	'tbl_lost_bin.category_id' => 1,
	'cmb_category.name' => '',
	'tbl_lost_bin.title' => '',
	'tbl_lost_bin.picture' => '',
	'tbl_lost_bin.description' => '',
	'tbl_lost_bin.city' => '',
	'tbl_lost_bin.country' => '',
	'tbl_lost_bin.reward_info' => ''
	);

	
	
	
 */
function listData($inputData,&$outputData)
{
	global $dbname;
	mysql_select_db($dbname);
	
	
	$str = 'SELECT * FROM '.$inputData[TableName].'';
	

	//Generate Where Statement	
	
	$conditions = array_slice($inputData, 3);
	foreach($conditions as $key => $value)
	{
		if(strstr($value, 'NOTNULL'))
		{
			$where .=' '.$key .'!="" AND';	
		}
		else if(strstr($value, 'LIKE'))
		{
			$where .=' '.$key .' '.$value.' AND';	
		}

		else if(strstr($value, '<='))
		{
			$where .=' '.$key .' <= '.substr($value,2).' AND';	
		}		


		else if(strstr($value, '<'))
		{
			$where .=' '.$key .' < '.substr($value,1).' AND';	
		}

		else if(strstr($value, '>=')) //use:  'end_date' => ' >= "'.date("Y-m-d").'"',
		{
			$where .=' '.$key .' >= '.substr($value,2).' AND';	
		}		


		else if(strstr($value, '>'))	//use:  'end_date' => ' > "'.date("Y-m-d").'"',
		{
			$where .=' '.$key .' > '.substr($value,1).' AND';	
		}
		else if($value)
		{
			if(substr($value,0,4) == 'NOT=')
			{
				$where .=' '.$key .'!='.substr($value,4).' AND';
			}
			else
			{
				 $where .=' '.$key .'="'.$value.'" AND';
			}
		}
	}
	//echo $where;
	if($where){
	$str .= ' WHERE ';
	}
	
	$where = substr($where, 0,-3);
	
	$str .= $where. ' ORDER BY `'.$inputData[OrderBy].'` '.$inputData[ASDSOrder].'';
	
	//echo $str.'<p>';

	$inputData = array_slice($inputData, 3); //removed 1st 2 element from the array; on is 'TableName', and the other one is OrderBy.	
	$result = mysql_query($str) 
	or die ('Error: '.mysql_error ());
	$dbRows = 0;
	

	while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
	{ 	
		$dbCols = 0;
		foreach($inputData as $key => $value)
		{
			$outputData[$dbRows][$key] = $row[$key];
			//echo $dbCols.'. '.$row[$key].'<br>';
			$dbCols++;
		}
		$dbRows++;
	 } 
}
/*
	$inputFoundData = array(
	'TableName' => 'tbl_found_bin',
	'OrderBy' => $OrderBy,
	'ASDSOrder' => $ASDSOrder,
	'InnerJoint' => 'cmb_category',
	'On' => 'tbl_found_bin.category_id = cmb_category.id',
	
	
	'tbl_found_bin.id' => '',
	'tbl_found_bin.user_id' => '',
	'tbl_found_bin.category_id' => '',
	'cmb_category.name' => '',
	'tbl_found_bin.title' => '',
	'tbl_found_bin.picture' => '',
	'tbl_found_bin.description' => '',
	'tbl_found_bin.city' => '',
	'tbl_found_bin.country' => '',
	'tbl_found_bin.btn_state' => ''
	);
	
	listJointData($inputFoundData,$data);	
*/
function listJointData($inputData,&$outputData)
{
	global $dbname;
	mysql_select_db($dbname);
	
	// generate column data
	
	$columnsToList = array_slice($inputData, 5);
	
	foreach($columnsToList as $column => $colValues)
	{
		$colNames .= $column.', ';
	}
	
	$colNames = substr($colNames, 0, strlen(trim($colNames))-1);
	
	$str = 'SELECT '.$colNames.' FROM '.$inputData[TableName].'';
	

	
	//$str = 'SELECT * FROM '.$inputData[TableName].' INNER JOIN '.$inputData[InnerJoint].' ON '.$inputData[On].' ' ;
	
	$str = $str.' INNER JOIN '.$inputData[InnerJoint].' ON '.$inputData[On].' ' ;
	
	


	//Generate Where Statement	
	
	$conditions = array_slice($inputData, 5);
	
	/*
	foreach($conditions as $key => $value)
	{
		if(strstr($value, 'NOTNULL'))
		{
			$where .=' '.$key .'!="" AND';	
		}
		else if(strstr($value, 'LIKE'))
		{
			$where .=' '.$key .' '.$value.' AND';	
		}
		else if($value)
		{
			if(substr($value,0,4) == 'NOT=')
			{
				$where .=' '.$key .'!='.substr($value,4).' AND';
			}
			else
			{
				 $where .=' '.$key .'="'.$value.'" AND';
			}
		}
	}
	*/
	
	foreach($conditions as $key => $value)
	{
		if(strstr($value, 'NOTNULL'))
		{
			$where .=' '.$key .'!="" AND';	
		}
		else if(strstr($value, 'LIKE'))
		{
			$where .=' '.$key .' '.$value.' AND';	
		}

		else if(strstr($value, '<='))
		{
			$where .=' '.$key .' <= '.substr($value,2).' AND';	
		}		


		else if(strstr($value, '<'))
		{
			$where .=' '.$key .' < '.substr($value,1).' AND';	
		}

		else if(strstr($value, '>=')) //use:  'end_date' => ' >= "'.date("Y-m-d").'"',
		{
			$where .=' '.$key .' >= '.substr($value,2).' AND';	
		}		


		else if(strstr($value, '>'))	//use:  'end_date' => ' > "'.date("Y-m-d").'"',
		{
			$where .=' '.$key .' > '.substr($value,1).' AND';	
		}
		else if($value)
		{
			if(substr($value,0,4) == 'NOT=')
			{
				$where .=' '.$key .'!='.substr($value,4).' AND';
			}
			else
			{
				 $where .=' '.$key .'="'.$value.'" AND';
			}
		}
	}	
	
	//echo $where;
	if($where){
	$str .= ' WHERE ';
	}
	
	$where = substr($where, 0,-3);
	
	$str .= $where. ' ORDER BY `'.$inputData[OrderBy].'` '.$inputData[ASDSOrder].'';
	
	//echo $str.'<p>';

	$inputData = array_slice($inputData, 5); //removed 1st 2 element from the array; on is 'TableName', and the other one is OrderBy.	
	$result = mysql_query($str);
	$dbRows = 0;
	

	while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
	{ 	
		$dbCols = 0;
		foreach($inputData as $key => $value)
		{
			$key = explode(".",$key);
			$key = $key[1];
			$outputData[$dbRows][$key] = $row[$key];
			//echo $dbCols.'. '.$row[$key].'<br>';
			$dbCols++;
		}
		$dbRows++;
	 } 
}



function searchData($inputData,&$outputData)
{
	global $dbname;
	mysql_select_db($dbname);
	
	$CopiedTableName = $inputData[TableName];
	
	$str = 'SELECT * FROM '.$inputData[TableName].'';
	

	//Generate Where Statement	
	
	$conditions = array_slice($inputData, 3);
	foreach($conditions as $key => $value)
	{

		
		if($value)
		{
			$where .=' '.$key .' LIKE "%'.$value.'%" AND';
		}
	}
	
	if($CopiedTableName == 'people')
	{
		$where .= ' first_name != ""      ';
	}
	
	if($CopiedTableName == 'business')
	{
		$where .= ' company_name  != ""      ';
	}	

	
	//echo $where;
	if($where){
	$str .= ' WHERE ';
	}
	
	
	$where = substr($where, 0,-3);
	
	$str .= $where. ' ORDER BY `'.$inputData[OrderBy].'` '.$inputData[ASDSOrder].'';
	
	//echo $str.'<p>';

	$inputData = array_slice($inputData, 3); //removed 1st 2 element from the array; on is 'TableName', and the other one is OrderBy.	
	$result = mysql_query($str);
	$dbRows = 0;
	

	while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
	{ 	
		$dbCols = 0;
		foreach($inputData as $key => $value)
		{
			$outputData[$dbRows][$key] = $row[$key];
			//echo $dbCols.'. '.$row[$key].'<br>';
			$dbCols++;
		}
		$dbRows++;
	 } 
}

function searchDataForAdmin($inputData,&$outputData)
{
	global $dbname;
	mysql_select_db($dbname);
	
	$CopiedTableName = $inputData[TableName];
	
	$str = 'SELECT * FROM '.$inputData[TableName].'';
	

	//Generate Where Statement	
	
	$conditions = array_slice($inputData, 3);
	foreach($conditions as $key => $value)
	{

		
		if($value)
		{
			$where .=' '.$key .' LIKE "%'.$value.'%" AND';
		}
	}
	
	

	
	//echo $where;
	if($where){
	$str .= ' WHERE ';
	}
	
	
	$where = substr($where, 0,-3);
	
	$str .= $where. ' ORDER BY `'.$inputData[OrderBy].'` '.$inputData[ASDSOrder].'';
	
	//echo $str.'<p>';

	$inputData = array_slice($inputData, 3); //removed 1st 2 element from the array; on is 'TableName', and the other one is OrderBy.	
	$result = mysql_query($str);
	$dbRows = 0;
	

	while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
	{ 	
		$dbCols = 0;
		foreach($inputData as $key => $value)
		{
			$outputData[$dbRows][$key] = $row[$key];
			//echo $dbCols.'. '.$row[$key].'<br>';
			$dbCols++;
		}
		$dbRows++;
	 } 
}


function listRandomData($inputData,&$outputData)
{
	global $dbname;
	mysql_select_db($dbname);
	
	$str = 'SELECT * FROM '.$inputData[TableName].'';
	

	//Generate Where Statement	
	
	$conditions = array_slice($inputData, 3);
	foreach($conditions as $key => $value)
	{
		if(strstr($value, 'NOTNULL'))
		{
			$where .=' '.$key .'!="" AND';	
		}
		else if($value)
		{
			if(substr($value,0,4) == 'NOT=')
			{
				$where .=' '.$key .'!='.substr($value,4).' AND';
			}
			else
			{
				 $where .=' '.$key .'="'.$value.'" AND';
			}
		}
	}
	//echo $where;
	if($where){
	$str .= ' WHERE ';
	}
	
	$where = substr($where, 0,-3);
	
	$str .= $where. ' ORDER BY '.$inputData[OrderBy].' '.$inputData[ASDSOrder].'';
	
	//echo $str.'<p>';

	$inputData = array_slice($inputData, 3); //removed 1st 3 element from the array; on is 'TableName', and the other one is OrderBy.	
	$result = mysql_query($str);
	$dbRows = 0;
	

	while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
	{ 	
		$dbCols = 0;
		foreach($inputData as $key => $value)
		{
			$outputData[$dbRows][$key] = $row[$key];
			//echo $dbCols.'. '.$row[$key].'<br>';
			$dbCols++;
		}
		$dbRows++;
	 } 
}

// list data group by and adding SUM() method
function listDataGroupBy($inputData,&$outputData)
{
	global $dbname;
	mysql_select_db($dbname);
	
	$columnsToList = array_slice($inputData, 4);
	
	foreach($columnsToList as $column => $colValues)
	{
		$colNames .= $column.', ';
	}
	
	$colNames = substr($colNames, 0, strlen(trim($colNames))-1);
	
	$str = 'SELECT '.$colNames.' FROM '.$inputData[TableName].'';
	

	//Generate Where Statement	
	
	$conditions = array_slice($inputData, 4);

	foreach($conditions as $key => $value)
	{
		if(strstr($value, 'NOTNULL'))
		{
			$where .=' '.$key .'!="" AND';	
		}
		else if($value)
		{
			if(substr($value,0,4) == 'NOT=')
			{
				$where .=' '.$key .'!='.substr($value,4).' AND';
			}
			else
			{
				 $where .=' '.$key .'="'.$value.'" AND';
			}
		}
	}
	//echo $where;
	if($where){
	$str .= ' WHERE ';
	}
	
	$where = substr($where, 0,-3);
	
	$str .= $where. 'GROUP BY `'.$inputData[GroupBy].'` ORDER BY `'.$inputData[OrderBy].'` '.$inputData[ASDSOrder].'';
	
	//echo $str.'<p>';

	$inputData = array_slice($inputData, 3); //removed 1st 2 element from the array; on is 'TableName', and the other one is OrderBy.	
	$result = mysql_query($str);
	$dbRows = 0;
	

	while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
	{ 	
		$dbCols = 0;
		foreach($inputData as $key => $value)
		{
			if(strstr($key,' ')){
			$key = explode(' ', $key);
			$key = trim($key[2]);
			}
			
			$outputData[$dbRows][$key] = $row[$key];
			//echo $key.'. '.$row[$key].'<br>';
			$dbCols++;
		}
		$dbRows++;
	 } 
}
//Below is the old version o flistData;
function listData_OLD($inputData,&$outputData)
{
	global $dbname;
	mysql_select_db($dbname);
	
	$str = 'SELECT * FROM '.$inputData[TableName].'';
	

	//Generate Where Statement	
	
	$conditions = array_slice($inputData, 3);
	foreach($conditions as $key => $value)
	{
		if($value){
		$where .=' '.$key .'="'.$value.'" AND';
		}
	}
	if($where){
	$str .= ' WHERE ';
	}
	
	$where = substr($where, 0,-3);
	
	$str .= $where. ' ORDER BY `'.$inputData[OrderBy].'` '.$inputData[ASDSOrder].'';
	

	//echo $str.'<p>';

	$inputData = array_slice($inputData, 3); //removed 1st 2 element from the array; on is 'TableName', and the other one is OrderBy.	
	$result = mysql_query($str);
	$dbRows = 0;
	

	while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
	{ 	
		$dbCols = 0;
		foreach($inputData as $key => $value)
		{

			$outputData[$dbRows][$dbCols] = $row[$key];
			//echo $dbCols.'. '.$row[$key].'<br>';
			$dbCols++;
		}
		$dbRows++;
	 } 
}

function listDataWithAgeRange($inputData,&$outputData,$ageFrom,$ageTo)
{
	global $dbname;
	mysql_select_db($dbname);
	
	$str = 'SELECT * FROM '.$inputData[TableName].'';
	

	//Generate Where Statement	
	
	$conditions = array_slice($inputData, 3);

	$where .='`dob` <="'.$ageFrom.'-00-00 00:00:00" AND `dob` >= "'.$ageTo.'-00-00 00:00:00"';

	if($where){
	$str .= ' WHERE ';
	}
	

	
	$str .= $where. ' ORDER BY `'.$inputData[OrderBy].'` '.$inputData[ASDSOrder].'';
	

	//echo $str.'<p>';

	$inputData = array_slice($inputData, 3); //removed 1st 2 element from the array; on is 'TableName', and the other one is OrderBy.	
	$result = mysql_query($str);
	$dbRows = 0;
	

	while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
	{ 	
		$dbCols = 0;
		foreach($inputData as $key => $value)
		{
			$outputData[$dbRows][$dbCols] = $row[$key];
			//echo $dbCols.'. '.$row[$key].'<br>';
			$dbCols++;
		}
		$dbRows++;
	 } 
}

function listCmbData($inputData,&$outputData,$condition)
{
	global $dbname;
	mysql_select_db($dbname);
	
	//$str = 'SELECT DISTINCT '.$inputData[OrderBy].' FROM '.$inputData[TableName].' '.$condition.' ORDER BY `'.$inputData[OrderBy].'` '.$inputData[ASDSOrder].'';
	$str = 'SELECT DISTINCT '.$inputData[OrderBy].' FROM '.$inputData[TableName].' WHERE  '.$inputData[OrderBy].' != \'\' '.$condition.' ORDER BY `'.$inputData[OrderBy].'` '.$inputData[ASDSOrder].'';



	$inputData = array_slice($inputData, 3); //removed 1st 2 element from the array; on is 'TableName', and the other one is OrderBy.
	


	//echo $str.'<p>';
	$result = mysql_query($str)
	or die(mysql_error());
	$dbCols = 0;
	

	while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
	{ 	
		
		foreach($inputData as $key => $value)
		{
			$outputData[$dbCols] = $row[$key];
			//echo $dbCols.'. '.$row[$key].'<br>';
			$dbCols++;
		}
		
	 } 
}
/*
	foreach($arrayUsr as $key => $value)
	{
		echo $key.' : '.$value.'<br>';
	}
*/
?>