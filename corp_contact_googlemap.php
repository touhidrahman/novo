<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>NOVOAIR Location Map</title>
<style>
html, body{height:100%; margin:0; padding:0;}
</style>
</head>

<body>
<?php
$target = $_REQUEST['target'];

switch($target)
{
	case 'dhk_corporate':
	echo '<iframe src="https://www.google.com/maps/embed?pb=!1m5!3m3!1m2!1s0x0%3A0x0!2zKzIzwrA0NycyNy4wNiIsICs5MMKwMjQnMTIuNzci!5e0!3m2!1sen!2s!4v1389526069941" width="100%" height="100%" frameborder="0" style="border:0"></iframe>';
	break;
	
	case 'dhk_banani':
	echo '<iframe src="https://www.google.com/maps/embed?pb=!1m5!3m3!1m2!1s0x0%3A0x0!2zKzIzwrA0NycyNi43NCIsICs5MMKwMjQnMTUuMDIi!5e0!3m2!1sen!2s!4v1389526143606" width="100%" height="100%" frameborder="0" style="border:0"></iframe>';
	break;

	case 'dhk_gulshan':
	echo '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d912.7632240796846!2d90.41741145077911!3d23.781130639133217!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zKzIzwrA0Nic1Mi4zOSIsICs5MMKwMjUnMi4xNSI!5e0!3m2!1sen!2s!4v1389525995133" width="100%" height="100%" frameborder="0" style="border:0"></iframe>';
	break;

	case 'dhk_uttara':
	echo '<iframe src="https://www.google.com/maps/embed?pb=!1m5!3m3!1m2!1s0x0%3A0x0!2zKzIzwrA1MicxLjU2IiwgKzkwwrAyMyczNy42MiI!5e0!3m2!1sen!2s!4v1389526244530" width="100%" height="100%" frameborder="0" style="border:0"></iframe>';
	break;

	case 'dhk_airport':
	echo '<iframe src="https://www.google.com/maps/embed?pb=!1m5!3m3!1m2!1s0x0%3A0x0!2zKzIzwrA1MCc0Ny40OCIsICs5MMKwMjQnMjMuODQi!5e0!3m2!1sen!2s!4v1389526362053" width="100%" height="100%" frameborder="0" style="border:0"></iframe>';
	break;

	case 'ctg_gec':
	echo '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3689.8324685589832!2d91.82238976455686!3d22.359953790553785!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zKzIywrAyMSczNi42NiIsICs5McKwNDknMTcuNDQi!5e0!3m2!1sen!2s!4v1389526440022" width="100%" height="100%" frameborder="0" style="border:0"></iframe>';
	break;

	case 'ctg_cepz':
	echo '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3691.609842843089!2d91.78157693386841!3d22.292764511455367!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zKzIywrAxNyczNC4yMCIsICs5McKwNDYnNTAuODIi!5e0!3m2!1sen!2s!4v1389526521607" width="100%" height="100%" frameborder="0" style="border:0"></iframe>';
	break;

	case 'ctg_airport':
	echo '<iframe src="https://www.google.com/maps/embed?pb=!1m5!3m3!1m2!1s0x0%3A0x0!2zKzIywrAxNCczOS42MSIsICs5McKwNDgnNTUuMTUi!5e0!3m2!1sen!2s!4v1389526608200" width="100%" height="100%" frameborder="0" style="border:0"></iframe>';
	break;

	case 'coxs_bazar':
	echo '<iframe src="https://www.google.com/maps/embed?pb=!1m5!3m3!1m2!1s0x0%3A0x0!2zKzIxwrAyNSc0Mi43NSIsICs5McKwNTgnMjUuOTci!5e0!3m2!1sen!2s!4v1389526716516" width="100%" height="100%" frameborder="0" style="border:0"></iframe>';
	break;

	case 'jessore':
	echo '<iframe src="https://www.google.com/maps/embed?pb=!1m5!3m3!1m2!1s0x0%3A0x0!2zKzIzwrA5JzQ5LjcxIiwgKzg5wrAxMic0Ny42NCI!5e0!3m2!1sen!2s!4v1389526765558" width="100%" height="100%" frameborder="0" style="border:0"></iframe>';
	break;

	case 'khulna':
	echo '<iframe src="https://www.google.com/maps/embed?pb=!1m5!3m3!1m2!1s0x0%3A0x0!2zKzIywrA0OScxNi4wOCIsICs4OcKwMzMnMTEuMDUi!5e0!3m2!1sen!2s!4v1389526816311" width="100%" height="100%" frameborder="0" style="border:0"></iframe>';
	break;

	case 'sylhet':
	echo '<iframe src="https://www.google.com/maps/embed?pb=!1m5!3m3!1m2!1s0x0%3A0x0!2zKzI0wrA1Myc0My44MCIsICs5McKwNTInMjkuMzgi!5e0!3m2!1sen!2s!4v1389526866374" width="100%" height="100%" frameborder="0" style="border:0"></iframe>';
	break;
	
	case 'novoair':
	echo '<iframe src="map.php" width="100%" height="100%" frameborder="0" style="border:0" scrolling="no"></iframe>';
	break;
	
}
?>
</body>
</html>