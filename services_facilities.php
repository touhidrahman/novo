<?php
	require_once('setting/config.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
 <?php
 	include_once('inc_top_includes.php');
 ?>
</head>
<body class="inner">

    <!-- top nav -->
			 <?php
                include_once('inc_topnav.php');
             ?>     
             
	<!-- end top nav -->  
    
    

<br>
<br>
<br>

<div class="container help-line">
    <div>
        <img src="images/help-line.png" class="hidden-xs">
        <img src="images/help-line2.png" class="visible-xs">
    </div>
</div>


<!-- main navigation -->
			 <?php
                include_once('inc_mainnav.php');
             ?>           
<!-- end main navigation -->





<div class="slide-wrapper">

<!-- inner page Carousel -->
 <?php
    include_once('inc_carousel_inner.php');
 ?>   
<!-- end inner page Carousel -->  

<!-- content -->
    <div class="container">
    
    	<div class="body-container">
    
    
        
         <div class="breadcrumb">
        <div class="row">
          <div  class="col-sm-6">
            <h1><b>Services  </b>  &amp; Facilities</h1>
          </div>
          <div class="col-sm-6 text-right"> Services / Services  &amp; Facilities </div>
        </div>
      </div>     
        
      


        
        <div class="page-contents">
        

        
        
        
        
        
        

	




        <div class="row">
        	

            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
	         <h2>Travel Safely, Comfortably &amp; On-Time with NOVOAIR</h2>
             
<br>
<br>

<ul>        
 
  <li>Your Safety, our  priority - Twin Jet Engine aircraft for a safer journey.</li>
  <li>Your Comfort, our  responsibility – Leather Seat Advance Air-conditioning System to ensure utmost comfort.</li>
  <li>Your Time is our Business - On-Time Departure</li>
     <li>Family Friendly Airline –       Infant Free On-Board*</li>
     <li>Online System,   easy &amp; secured payment system is just clicks away
  <br>
  
  <ul style="margin-top:5px;">
    <li>100% E-Ticketing</li>
    <li>Bar Coded  Boarding Pass</li>
    <li>Web Ticketing  Service</li>
    <li>E-Payment</li>
  </ul>
     </li>
</ul>
		
        
    <br>
<br>



  




                
            </div>
            
            <div class="first-col col-lg-3 col-md-3  hidden-sm hidden-xs ">
                <!-- icon buttons -->
                 <?php
                    include_once('inc_inner_sidebar_iconbuttons.php');
                 ?>
                <!-- end icon buttons -->
            </div>            
            
        </div>
		
        <div class="clearfix">&nbsp;</div>
        
                <!-- icon buttons on bottom -->
                 <?php
                    include_once('inc_inner_bottombar_iconbuttons.php');
                 ?>
                <!-- end icon buttons on bottom-->
<br>
<br>




<!-- footer -->
 <?php
 	include_once('inc_footer.php');
 ?>
<!-- end footer -->








                    
        </div>
        
        
        
    </div>
    </div>
    <!-- end content -->


</div>

<div style="clear:both"></div>







 <?php
 	include_once('inc_bottom_includes.php');
 ?>


   

</body>
</html>
