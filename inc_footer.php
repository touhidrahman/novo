&nbsp;
<div class="row social-link-wrapper"><span class="col-xs-12"><div><img src="images/home-social-icons.png" usemap="#Map" class="img-responsive center-block">
      <map name="Map"><area shape="rect" coords="213,21,259,72" href="http://www.youtube.com/flynovoair">
        <area shape="rect" coords="106,22,151,70" href="http://www.facebook.com/pages/novoair/128001720686051?fref=ts">
        <area shape="rect" coords="163,21,209,72" href="http://www.google.com/+flynovoair">
      </map>
</div></span></div>



<div class="footer">
	<div class="row tfest">
    	<span class="col-md-2 col-sm-4">
            <strong>Quick Links</strong>
            <a href="https://secure.flynovoair.com/bookings/flight_selection.aspx">Book Flights</a>
            <a href="destinations.php?show=all">Destinations We Fly</a>
            <a href="flight_schedule.php">Flight Schedule</a>
            <a href="#">Current Offers</a>
            <a href="#">Holiday Packages</a>
        </span>
        <span class="col-md-2 col-sm-4">
            <strong>Travel Info</strong>
            <a href="destinations.php?show=all">Places to See</a>
            <a href="prohibited_items.php">Prohabited Items</a>
            <a href="baggage_info.php">Baggage Information</a>

        </span>
        <span class="col-md-2 col-sm-4">
            <strong>Services</strong>
            <a href="terms.php">eTicket Terms &amp; Condition</a>
            <a href="#">Sales Offices</a>
            <a href="agent.php">Agent Login</a>
        </span>
        <span class="col-md-2 col-sm-4">
            <strong>About NOVOAIR</strong>
            
            <a href="about_us.php">Company Information</a>
            <a href="http://www.novoair-bd.com/novoair.php">Corporate Website</a>
            <a href="job_career_at_novoair.php">Career</a>
</span>
        <span class="col-md-1 hidden-xs hidden-sm">&nbsp;</span>
        <span class="col-md-3 col-sm-4">
                <strong>Contact us</strong>
                HOTLINE: 13603<br>
                PHONE: 09666722224, 0298718912<br>
                MOBILE: 01755656660-1<br>
                FAX: 029890684        <br>
        
		<a href="feedback.php">Submit a Feedback</a>
		<a href="javascript:void(0)"  class="sub_window"  path="http://www.flynovoair.com/webim/client.php" width="650" height="560">Live Chat</a>
            <a href="http://www.facebook.com/pages/novoair/128001720686051?fref=ts">Facebook</a>
		</span>
	</div>
</div>

            <p class="copyright">
            Copyright by NOVOAIR 2015 | <a href="#">Privacy Policy</a>
            </p>
