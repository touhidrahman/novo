<?php
	require_once('setting/config.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
 <?php
 	include_once('inc_top_includes.php');
 ?>
</head>
<body class="inner">

    <!-- top nav -->
			 <?php
                include_once('inc_topnav.php');
             ?>     
             
	<!-- end top nav -->  
    
    

<br>
<br>
<br>

<div class="container help-line">
    <div>
        <img src="images/help-line.png" class="hidden-xs">
        <img src="images/help-line2.png" class="visible-xs">
    </div>
</div>


<!-- main navigation -->
			 <?php
                include_once('inc_mainnav.php');
             ?>           
<!-- end main navigation -->





<div class="slide-wrapper">

<!-- inner page Carousel -->
 <?php
    include_once('inc_carousel_inner.php');
 ?>   
<!-- end inner page Carousel -->             
             





  <!-- content -->
    <div class="container">
    
    	<div class="body-container">
    
    
        
         <div class="breadcrumb">
        <div class="row">
          <div  class="col-sm-6">
            <h1><b>Baggage </b>  &amp; Cargo Information</h1>
          </div>
          <div class="col-sm-6 text-right"> Travel Info / Baggage &amp; Cargo Information </div>
        </div>
      </div>     
        
      


        
        <div class="page-contents">
        

        
        
        
        
        
        

	




        <div class="row">
        	

            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
	         <h2>NOVOAIR Baggage Allowances and Dimension, for Domestic Routes</h2>
             
	<br>

<p>
<img src="images/page_headers/baggage.jpg" class="img-responsive">
</p>    
   <br> 

		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table baggage">
      <tbody>
        <tr>
          <th scope="col">PASSENGER TYPE</th>
          <th scope="col">CHECKED BAGGAGE</th>
          <th scope="col">CABIN BAGGAGE</th>
          <th scope="col">EXCESS BAGGAGE</th>
        </tr>
        <tr>
          <td><strong>Adult</strong></td>
          <td>20 Kg<br>
            Maximum 2 pc per person<br>
            Dimension per pc: <br>
          40 x 60 x 100 cm (15x24x40 in)</td>
          <td>7 Kg<br>
            1 pc per person<br>
            Dimension per pc: <br>
          15 x 22 x 35 cm (6 x 8 x 14 in)</td>
          <td rowspan="3">TK 100 per Kg</td>
        </tr>
        <tr>
          <td><strong>Child</strong></td>
          <td>20 Kg<br>
            Maximum 2 pc per person<br>
            Dimension per pc: <br>
          40 x 60 x 100 cm (15x24x40in)</td>
          <td>7 Kg<br>
            1 pc per person<br>
            Dimension per pc: <br>
          15 x 22 x 35 cm (6 x 8 x 14 in)</td>
        </tr>
        <tr>
          <td><strong>Infant</strong></td>
          <td>10 Kg<br>
            Dimension: <br>
          40 x 60 x 100 cm (15x24x40 in)</td>
          <td>Carryon Bag<br>
            Dimension: <br>
          15 x 22 x 35 cm (6 x8x14 in)</td>
        </tr>
      </tbody>
    </table>
        
    <br>
<br>

        
<img src="images/trlv/baggage_rules.jpg" class="img-responsive">

  




                
            </div>
            
            <div class="first-col col-lg-3 col-md-3  hidden-sm hidden-xs ">
                <!-- icon buttons -->
                 <?php
                    include_once('inc_inner_sidebar_iconbuttons.php');
                 ?>
                <!-- end icon buttons -->
            </div>            
            
        </div>
		
        <div class="clearfix">&nbsp;</div>
        
                <!-- icon buttons on bottom -->
                 <?php
                    include_once('inc_inner_bottombar_iconbuttons.php');
                 ?>
                <!-- end icon buttons on bottom-->
<br>
<br>




<!-- footer -->
 <?php
 	include_once('inc_footer.php');
 ?>
<!-- end footer -->








                    
        </div>
        
        
        
    </div>
    </div>
    <!-- end content -->


</div>

<div style="clear:both"></div>







 <?php
 	include_once('inc_bottom_includes.php');
 ?>


   

</body>
</html>
