<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="q1">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#cq1" aria-expanded="true" aria-controls="cq1">
          What is Manage a Booking?
        </a>
      </h4>
    </div>
    <div id="cq1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="q1">
      <div class="panel-body">
        With Manage a Booking, you can:
        <ul>
          <li>View, print out and e-mail your flight information</li>
          <li>Change your booking date, time and fare class</li>
          <li>Edit your frequent flyer number, contact details, and advance passenger information</li>
        </ul>
      </div>
    </div>
  </div>

  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="q2">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#cq2" aria-expanded="false" aria-controls="cq2">
          How many times can I make use of Manage Booking?
        </a>
      </h4>
    </div>
    <div id="cq2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="q2">
      <div class="panel-body">
        There is no limit on the number of times to use Manage Booking.
      </div>
    </div>
  </div>

  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="q3">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#cq3" aria-expanded="false" aria-controls="cq3">
          What is an electronic ticket?
        </a>
      </h4>
    </div>
    <div id="cq3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="q3">
      <div class="panel-body">
        NOVOAIRs' electronic tickets do away with paper tickets and allow you to book flights conveniently over the internet. After you have completed the online transaction to purchase your tickets, you will be shown a confirmation page and sent a confirmation email, both containing your personal and flight details and a unique reference number. All you need do is print out either one of these and present it along with your photo ID/passport at the check-in desk.
      </div>
    </div>
  </div>

  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="q4">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#cq4" aria-expanded="false" aria-controls="cq4">
          What is acceptable as Photo ID?
        </a>
      </h4>
    </div>
    <div id="cq4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="q4">
      <div class="panel-body">
        According CAAB, providing photo ID during your flight is mandatory. For NOVOAIR domestic flight, you may provide National ID, Driving License, Passport, Official/Student ID or any kind of ID card with Photo and name is mentioned.
      </div>
    </div>
  </div>

  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="q5">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#cq5" aria-expanded="false" aria-controls="cq5">
          Can I travel with a pet?
        </a>
      </h4>
    </div>
    <div id="cq5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="q5">
      <div class="panel-body">
        Pet animal will not be accepted on NOVOAIR flights.
      </div>
    </div>
  </div>

  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="q6">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#cq6" aria-expanded="false" aria-controls="cq6">
          How can I use my credit card to book a ticket for someone else?
        </a>
      </h4>
    </div>
    <div id="cq6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="q6">
      <div class="panel-body">
        Just book and pay for the ticket through our online booking engine at flynovoair.com<br>
Make sure you enter the cardholder information (name, email address etc) correctly. Note that NOVOAIR may contact the card holder and require to submit additional information online to confirm the payment, or require to present the credit card at check-in.

      </div>
    </div>
  </div>

  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="q7">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#cq7" aria-expanded="false" aria-controls="cq7">
          If I book online, how do I get my ticket?
        </a>
      </h4>
    </div>
    <div id="cq7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="q7">
      <div class="panel-body">
        If you book online, you will receive an electronic ticket (also referred to as an ‘eticket’) by email to the address you provided when during the booking process.
      </div>
    </div>
  </div>

  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="q8">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#cq8" aria-expanded="false" aria-controls="cq8">
          Is smoking allowed on any NOVOAIR flights?
        </a>
      </h4>
    </div>
    <div id="cq8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="q8">
      <div class="panel-body">
        All NOVOAIR flights are non-smoking. All Emirates codeshare flights are also non-smoking.
The non-smoking policy also includes the use of e-cigarettes. No e-cigarettes can be used on board NOVOAIR flights.

      </div>
    </div>
  </div>

  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="q9">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#cq9" aria-expanded="false" aria-controls="cq9">
          Can I cancel my booking online?
        </a>
      </h4>
    </div>
    <div id="cq9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="q9">
      <div class="panel-body">
        No, to cancel your booking and request a refund please send us e-mail to reservation@flynovoair.com or request directly to NOVOAIR Sales counter. Please note this is for bookings made directly with NOVOAIR website or Sales office only.<br>
        If you booked through a travel agent and haven’t started your journey yet, please contact your travel agent to make any changes.<br>
        There may be a fee associated with cancelling your booking depending on the fare conditions of your ticket.

      </div>
    </div>
  </div>

  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="q10">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#cq10" aria-expanded="false" aria-controls="cq10">
          What happens to my seat or meal requests?
        </a>
      </h4>
    </div>
    <div id="cq10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="q10">
      <div class="panel-body">
These features are not available at this moment.

      </div>
    </div>
  </div>

  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="q11">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#cq11" aria-expanded="false" aria-controls="cq11">
          What should I do if I’ve lost a piece of property?
        </a>
      </h4>
    </div>
    <div id="cq11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="q11">
      <div class="panel-body">
        If you’ve lost property on board one of our aircraft, please contact the customer service at the airport where your flight landed.
        <br>
        If you’ve lost baggage, you’ll need to fill out a report. We’ll then be in touch as soon as your baggage is found to find out how we can return your bags to you.

      </div>
    </div>
  </div>

</div>
