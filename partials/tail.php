<div style="clear:both"></div>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<style>
#toTop{
	position: fixed;
	bottom: 10px;
	right: 10px;
	cursor: pointer;
	display: none;
}
</style>
<script>
$(document).ready(function(){
      $('body').append('<div id="toTop" class="btn btn-default"><span class="glyphicon glyphicon-chevron-up"></span> Back to Top</div>');
    	$(window).scroll(function () {
			if ($(this).scrollTop() != 0) {
				$('#toTop').fadeIn();
			} else {
				$('#toTop').fadeOut();
			}
		});
    $('#toTop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
});
</script>

<!-- color box -->
<link rel="stylesheet" href="js/plugins/colorbox/colorbox.css" />
<script src="js/plugins/colorbox/jquery.colorbox.js"></script>
<!-- end color box -->
<script language="javascript" src="js/plugins/bootstrap-switch/bootstrap-switch.js"></script>


<script language="javascript" src="js/plugins/bootstrap-responsive-tabs-master/responsive-tabs.js"></script>



<script language="javascript" src="js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
