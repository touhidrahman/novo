<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">


<meta name="description" content="Novoair is an airline based in Dhaka, Bangladesh. Novoair operates domestic flights, from its hub at Shahjalal International Airport, with three Embraer ERJ 145 aircraft.">
<meta name="author" content="Novoair">
<title>NOVOAIR</title>
<link rel="icon" href="images/favicon.ico">

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="js/plugins/bootstrap-switch/bootstrap-switch.css" rel="stylesheet">


<link href="js/plugins/bootstrap-datepicker/datepicker3.css" rel="stylesheet">

<link href="fonts/webfonts.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="css/custom.css" rel="stylesheet">

<!-- Yamm styles-->
<link href="yamm/yamm.css" rel="stylesheet">
<link href="css/menu-styles.css" rel="stylesheet">

<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- google fonts -->
<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
<!-- font-family: 'Oswald', sans-serif; -->
<link href='http://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'>
<!-- font-family: 'Abel', sans-serif; -->

<!-- yamm -->
<script>
$(document).on('click', '.yamm .dropdown-menu', function(e) {
  e.stopPropagation()
});
</script>
</head>
