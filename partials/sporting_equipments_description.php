<h2 id="baseball">Baseball Equipment</h2>


<P>Baseball bats are accepted as limited liability inside an equipment bag. There are no fees as long as the bag fits within the size and weight guidelines. However, one item of baseball equipment will count as a checked bag.</P>
<P>Individual bats are not recommended and not permitted in carry-on baggage.</P>

<hr>

<h2 id="bicycle">Bicycles</h2>

<P>
	<B>What is accepted:</B> </P>
<P>Bicycles will be accepted in a hard-sided, padded case designed for bicycles. If not in a hard-sided case, bikes will be accepted with the handle bars secured sideways and pedals removed. The bicycle must also be encased in plastic foam, a cardboard box
	<EM>(domestic flights only)</EM>, or similar material to prevent damage. </P>
<P>
	<B>Domestic and International Flights:</B> </P>
<P>Bicycles will be accepted on domestic and certain international flights* for a fee of
	<STRONG>_________ per bike each way </STRONG>and will count as a checked bag. Excess baggage fees may apply. </P>
<P>
	<EM>Please note</EM>: overweight baggage fees will not be assessed for bicycles. However, the maximum allotment of 99 pounds per bag still applies. Bicycle cases should contain bicycles only; cases containing additional items may be subject
	to excess baggage fees. </P>
<P>
	<EM>Also note:</EM> if bicycle and container are less than 62 dimensional inches and under 50 pounds, the bike fee will not be assessed. </P>

<hr>

<h2 id="bowling">Bowling Equipment</h2>


<P>There is no additional charge for bowling equipment as long as all items are within size and weight limitations. However, one item of bowling equipment will count as a checked bag. </P>
<P>
	<B>One item of bowling equipment is considered:</B>
</P>
<UL>
	<LI>one bowling ball</LI>
	<LI>one bowling bag designed for this purpose, and</LI>
	<LI>one pair of bowling shoes </LI>
</UL>

<hr>

<h2 id="drone">Drones</h2>

<P>Drones may be accepted as checked baggage or carry on if it fits in the overhead bin or beneath the seat with the battery installed.</P>
<P>A drone is considered a Portable Electronic Device (PED) and must remain in the off position for the duration of the flight to prevent accidental activation. </P>
<P>Drones are generally operated on Lithium Ion/Polymer Rechargeable Batteries. Watt hours on these are the same as all other lithium ion batteries 100Wh and 160Wh  and two spares may be carried as carry on when placed in a plastic bag and its terminal
	protected with tape. Below 100Wh there are no quantity restrictions as to the amount of batteries you can carry.
</P>

<hr>

<h2 id="firearms">Firearms/Archery/Shooting Equipment and Ammunition</h2>

<!-- <P>
	<B>Firearms are not permitted on international flights to or from the U.S.</B>
</P> -->
<P>There is no additional charge or limit for checking shooting equipment. However, one piece of shooting equipment will count as a checked bag. </P>
<P>You must be 18 years of age to check a firearm. </P>
<P>Firearms, shooting equipment and related items will be accepted only as checked baggage subject to the following specific conditions: </P>
<P>All firearms must be unloaded and must be packed in either a lockable crush-proof container specifically designed for the firearm, or in a hard sided container with locks or combination locks (where the customer checking the firearm retains the combination
	numbers). If the firearm case cannot be locked, NOVOAIR will not accept the item. NOVOAIR recommends that you provide the key or combination to the security officer if he or she needs to open the container. You should remain present during screening
	to take the key back after the container is cleared. Once all of the criteria is met, the firearm may be placed in your checked bag.</P>
<P>Ammunition for the firearm cannot be placed in the same container as the firearm, but may be in the same checked bag as long as everything is packaged properly. Ammunition must be housed in a separate container that is completely separate and distinct
	from the firearms locked box. The ammunition must be packaged in a fiber (such as cardboard), wood or metal box specifically designed for carrying small amounts of ammunition. Ammunition is limited to 10 pounds per customer. Ammunition will not be permitted
	in carry-on or checked baggage on international flights.  </P>

<P>
	<B>One item of shooting equipment is considered:</B>
</P>
<P>One rifle case containing no more than two rifles with or without scopes, one shooting net, noise suppressors, and a small rifle tool set, or one shotgun case containing no more than two shotguns, or one pistol case containing no more than four pistols,
	or one bow and quiver of arrows and maintenance kit enclosed in a case or container of sufficient strength to protect the bow and quiver with arrows from accidental damage. </P>
<P>
	<B><I>Please note:</I></B> BB guns and air guns are also considered as shooting equipment and will be treated as such.</P>

<hr>


<h2 id="fishing">Fishing Equipment</h2>

<P>There is no additional charge for fishing equipment; however, one item of fishing equipment will count as a checked bag. Fishing poles are exempt from the standard size requirements (62&quot;) but should still follow weight and other equipment guidelines.
</P>
<P>
	<B><U>One item of fishing equipment is considered: </B>
	</U>
</P>
<UL>
	<LI>two rods</LI>
	<LI>one reel</LI>
	<LI>one landing net</LI>
	<LI>one pair of fishing boots (properly encased)</LI>
	<LI>one fishing tackle box </LI>
</UL>
<P>
	<STRONG>Please note:</STRONG> all equipment should be packed in a hard-sided container</P>
<P>Fishing poles are allowed as carry-on baggage provided they fit in the overhead compartment. Small hooks for fly fishing or fresh water hooks are acceptable as allowed by TSA (Transportation Security Administration). Deep sea fishing hooks are permitted
	in checked bags.</P>
<P>
	<STRONG>
		<U>Spear Guns</U>
	</STRONG>
	<hr>Spear Guns must be in a hard-sided container or with the tip of the spear protected to prevent damage and must be within the size and weight guidelines, or excess fees would apply.</P>
<P>NOVOAIR does not allow compressed gas cylinders. If the gas cylinder cannot be removed from the spear gun, it will not be allowed.</P>

<hr>

<h2 id="golf">Golfing Equipment</h2>

<P>Golf equipment is accepted on all flights. There is no additional charge or oversized fee for golf bags as long as they are within the standard weight limitations. A golf bag will count as a checked bag. </P>
<P>
	<B><U>One item of golfing equipment is considered: </U></B>
</P>
<UL>
	<LI>One golf bag containing not more than:
		<UL>
			<LI>14 golf clubs</LI>
			<LI>3 golf balls</LI>
			<LI>one pair of golf shoes</LI>
		</UL>
	</LI>
</UL>
<P>
	<STRONG>Please note:</STRONG> all equipment should be packed in a hard-sided container. NOVOAIR will accept Golf Bags in a soft-sided travel bag, but will not be liable for damage to the bag or contents.</P>

<hr>

<h2 id="hockey">Hockey Sticks and Ice Skates</h2>

<P>There is no additional charge for hockey sticks/lacrosse sticks transported in one equipment bag that fits within the size and weight restrictions; however, up to two (2) hockey or lacrosse sticks taped together outside of a checked bag will count as
	one checked item. If you are checking in any additional bags you will be charged the additional baggage fees. </P>
<P>Ice skates are prohibited from your carry-on baggage, but they may be transported to your destination in your checked baggage.</P>


<hr>

<h2 id="paddle">Paddle Boards and Paddles</h2>


Paddle boards and paddles are not accepted on NOVOAIR flights due to size constraints and difficulty of transport.

<hr>

<h2 id="parachute">Parachutes</h2>


<P>Parachutes may be checked or carried onboard if they do not contain any type of canister or cartridge and meet the size/weight requirements.</P>
<P>
	<STRONG>Note:</STRONG> Airport crewmembers may request that a parachute be unfolded in their presence. Customers traveling with a parachute are advised to arrive at least 30 minutes prior to the airport&#39;s recommended arrival window.</P>

<hr>

<h2 id="scooter">Scooters</h2>


<P>Manual and approved battery powered scooters will be accepted for transport. Gas powered scooters are NOT accepted for transport.</P>
<P>A manual scooter is a razor-type scooter without a battery. It is operated by the rider physically pushing the scooter. There is no battery attached. </P>
<P>
	<B>A battery powered scooter can be one of three types</B> : </P>
<UL>
	<LI>razor-type scooter</LI>
	<LI>powered chair-type scooter (for disabled customers)</LI>
	<LI>Segway Personal Transporter (used as a medical device) powered by an NiMH battery pack (PT)</LI>
</UL>
<P>All three types will have nonspillable batteries, and therefore can remain attached to the scooter during transport. The battery wires will be disconnected in order to protect them from potential damage, unless the scooter design provides an effective
	means of preventing unintentional activation. </P>

<hr>

<h2 id="scuba">Scuba Equipment</h2>


<P>Regulators, Buoyancy Compensators, masks, snorkels and fins are all acceptable as either checked or carry-on baggage. </P>
<P>Neither full nor empty compressed gas cylinders (scuba tanks) are allowed in checked or carry-on baggage. </P>
<P>There is no additional charge for scuba equipment as long as the items are within size and weight limitations.
</P>

<hr>

<h2 id="selfie">Selfie-Sticks / Tripods</h2>


At this time, there are no restrictions for the carry on of selfie-sticks, tripods or other telescopic devices as a personal item as long as they fit within the accepted carry-on baggage dimensions.


<hr>

<h2 id="skate">Skateboards</h2>

Skateboards/longboards can be accepted as a carry-on or checked item, as long as it is within NOVOAIR’s size limitations.  If checked in, it will only be accepted as a conditionally checked item at the airport.  Conditionally accepted items
are accepted only at the customer&#39;s own risk.  NOVOAIR will not be liable for damage, loss, or spoilage of conditionally accepted items.

<hr>

<h2 id="ski">Skis and Snowboards</h2>

<P>There is no additional charge for ski and snowboard equipment; however, one item of equipment will count as a checked bag. Skis and snowboards are exempt from the standard size requirements (62&quot;) but should still follow weight and other equipment
	guidelines.
</P>
<P>
	<U>One item of ski equipment is considered</U>: </P>
<UL>
	<LI>One pair of skis, with one pair of ski boots and one pair of ski poles. (
		<STRONG>Please note</STRONG>: ski boots, if packed separately from skiing equipment, must be in a ski boot bag to be considered part of the one piece of checked baggage) or</LI>
	<LI>One snowboard, with one pair of snowboard boots. (
		<STRONG>Please note</STRONG>: snowboard boots, if packed separately from snowboarding equipment, must be in a snow boot bag to be considered part of the one piece of checked baggage). Also note, customers will be charged an excess baggage fee if the snowboard
		exceeds our checked baggage weight requirements. </LI>
</UL>
<P>The equipment must be checked in at the ticket counter. All ski or snowboard equipment must be in a sturdy container for protection.</P>

<hr>

<h2 id="surf">Surfing Equipment</h2>

<P>
	<B>What is accepted:</B> </P>
<P>One surfboard per case; size and weight restrictions do not apply, except items weighing more than one hundred pounds (100 pounds) will not be accepted as checked baggage. We recommend that surfboards travel in a hard-sided (rather than soft-sided) case
	to prevent damage.
</P>
<P>
	<B>Domestic Flights:</B> </P>
<P>Surfboards are accepted on domestic flights for a fee of _______ per board each way and will count as a checked bag. Excess baggage fees may apply.

</P>
<P>
	<B>International Flights:</B> </P>
<P>Surfboards are also accepted on international flights for a fee of ________ per board each way and will count as a checked bag. Excess baggage fees may apply. </P>

<hr>

<h2 id="tennis">Tennis Rackets</h2>

<P>A tennis racket may be carried on the aircraft and counts as one carry-on item.*</P>

<hr>

<h2 id="waterski">Water Skiing Equipment</h2>

<P>There is no additional charge for water ski equipment on domestic flights; however, one item of equipment will count as a checked bag. Skis are exempt from the standard size requirements (62&quot;) but should still follow weight and other equipment
	guidelines.
</P>
<P>
	<B><U>One item of water skiing equipment is considered:</B>
	</U>
</P>
<UL>
	<LI>one pair of water skis</LI>
	<LI>one tow rope</LI>
	<LI>one life preserver</LI>
	<I><B>or</B></I>
	<LI>one wakeboard</LI>
	<LI>One tow rope</LI>
	<LI>One life preserver</LI>
</UL>

<hr>
