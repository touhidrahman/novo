<?php	require_once('setting/config.php'); ?>
<!DOCTYPE html>
<html lang="en">

<?php	include_once('partials/head.php'); ?>

	<body class="inner">

		<br>
		<br>
		<br>

		<div class="container help-line">
			<div>
				<img src="images/help-line.png" class="hidden-xs">
				<img src="images/help-line2.png" class="visible-xs">
			</div>
		</div>

		<!-- main navigation -->
		<?php include_once('partials/mainnav2.php'); ?>
			<!-- end main navigation -->


			<div class="slide-wrapper">

				<?php	include_once('partials/inner_carousel.php'); ?>


					<!-- content -->
					<div class="container">
						<div class="body-container">

							<div class="breadcrumb">
								<div class="row">
									<div class="col-sm-6">
										<h1><b>Travelling with Pets</b></h1>
									</div>
									<div class="col-sm-6 text-right"> Travel Info / Other Information </div>
								</div>
							</div>

							<div class="page-contents">

								<div class="row">
									<div class="col-sm-9 col-xs-12">
										<h2>Before you go</h2>
										<p>
											<ul>
												<li>Check the vaccination and documentation required for each destination on your trip.</li>
												<li>Don't forget your pet's identification tags and license.</li>
												<li>Remember your pet carrier cannot exceed 17"L x 12.5"W x 8.5"H and the combined weight of your pet and the carrier must not exceed 20 pounds.</li>
											</ul>
										</p>
										<hr>
										<h2>Booking your pet</h2>
										<h4>Pets can be booked calling our hotline number 13603.</h4>
										<p>Remember: <br>
											<ul>
												<li>There is a non-refundable pet fee of _______ each way.</li>
												<li>It is best to book early. A limited number of pets are allowed on each flight.</li>
												<li>Only one pet per customer is allowed.</li>
												<li>Your pet carrier can not exceed 17"L x 12.5"W x 8.5"H and the combined weight of your pet and the carrier must not exceed 20 pounds.</li>
												<li>Your pet and carrier count as one carry-on bag onboard.</li>
											</ul>
										</p>
										<hr>
										<h2>At the airport</h2>
										<p>
											<ul>
												<li>Check in your pet at the counter.</li>
												<li>At the security check-point, remove your pet and carry (or walk) it while the pet carrier is x-rayed.</li>
												<li>Your pet and carrier count as one carry-on bag onboard.</li>
												<li>All pets must remain inside the pet carrier while at the airport and onboard the aircraft.</li>
												<li>If your pet needs to relieve itself, you must exit the airport and return through security.</li>
											</ul>
										</p>
										<hr>
										<h2>Travel checklist</h2>
										<p>Traveling with your pet is a breeze when you're well-prepared and informed. Use the checklist below to ensure you have everything you need for a smooth trip:<br>
											<ul>
												<li>Necessary vaccinations and documentation</li>
												<li>ID tags</li>
												<li>Pet license</li>
												<li>Approved pet carrier (no larger than 17"L x 12.5"W x 8.5"H)</li>
												<li>Pet snacks and treats</li>
												<li>Pet supplies for the trip</li>
												<li>A pre-flight workout for your pet. That way they'll adapt more quickly to their new surroundings and sleep better.</li>
											</ul>
										</p>
									</div>

									<div class="col-sm-3 col-xs-12">
										<?php	include_once('partials/sidebar_otherinfo.php'); ?>
									</div>
								</div>


								<div class="clearfix">&nbsp;</div>
								<br>
								<br>
								<!-- footer -->
								<?php	include_once('partials/footer.php'); ?>
								<!-- end footer -->
							</div>
							<!-- end page content -->
						</div>
					</div>
					<!-- end container -->
			</div>
			<!-- end slide wrapper -->

			<?php	include_once('partials/tail.php'); ?>

	</body>

</html>
