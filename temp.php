<?php require_once('setting/config.php'); ?>
<!DOCTYPE html>
<html lang="en">
<?php include_once('partials/head.php'); ?>

	<body class="inner">
		<br>
		<br>
		<br>
		<div class="container help-line">
			<div>
				<img src="images/help-line.png" class="hidden-xs">
				<img src="images/help-line2.png" class="visible-xs"> </div>
		</div>
		<!-- main navigation -->
		<?php include_once('partials/mainnav2.php'); ?>
		<!-- end main navigation -->
		<div class="slide-wrapper">
			<?php include_once('partials/inner_carousel.php'); ?>
			<!-- content -->
			<div class="container">
				<div class="body-container">

					<div class="breadcrumb">
						<div class="row">
							<div class="col-sm-6">
								<h1><b>Contacts </b> And Addresses</h1>
							</div>
							<div class="col-sm-6 text-right"> About Us / Contacts </div>
						</div>
					</div>

					<div class="page-contents">
						<!-- ==============START CONTENT============= -->
						<div class="row">
							<div class="col-sm-9 col-xs-12">
								<?php include_once('places_to_see/cxb.php'); ?>
							</div>

							<div class="col-sm-3 col-xs-12">

							</div>
						</div>



						<!-- ==============END CONTENT============= -->
						<div class="clearfix">&nbsp;</div>
						<br>
						<br>
						<!-- footer -->
						<?php include_once('partials/footer.php'); ?>
							<!-- end footer -->
					</div>
					<!-- end page content -->
				</div>
			</div>
			<!-- end container -->
		</div>
		<!-- end slide wrapper -->
		<?php include_once('partials/tail.php'); ?>
	</body>

</html>
