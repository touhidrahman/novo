<?php	require_once('setting/config.php'); ?>
<!DOCTYPE html>
<html lang="en">

<?php	include_once('partials/head.php'); ?>

	<body class="inner">

		<br>
		<br>
		<br>

		<div class="container help-line">
			<div>
				<img src="images/help-line.png" class="hidden-xs">
				<img src="images/help-line2.png" class="visible-xs">
			</div>
		</div>

		<!-- main navigation -->
		<?php include_once('partials/mainnav2.php'); ?>
			<!-- end main navigation -->


			<div class="slide-wrapper">

				<?php	include_once('partials/inner_carousel.php'); ?>


					<!-- content -->
					<div class="container">
						<div class="body-container">

							<div class="breadcrumb">
								<div class="row">
									<div class="col-sm-6">
										<h1><b>Sporting Gears and Equipments</b></h1>
									</div>
									<div class="col-sm-6 text-right"> Travel Info / Other Information </div>
								</div>
							</div>

							<div class="page-contents">

								<div class="row">
									<div class="col-sm-9 col-xs-12">
										<?php	include_once('partials/sporting_equipments_description.php'); ?>
									</div>

									<div class="col-sm-3 col-xs-12">
										<div class="well">
											<strong class="text-muted">QUICK LINKS</strong>
											<ul class="list-unstyled">
												<li><a href="#baseball">Baseball</a></li>
												<li><a href="#bicyle">Bicycle</a></li>
												<li><a href="#bowling">Bowling</a></li>
												<li><a href="#drone">Drones</a></li>
												<li><a href="#firearms">Firearms/Shooting/Archery</a></li>
												<li><a href="#fishing">Fishing</a></li>
												<li><a href="#golf">Golf</a></li>
												<li><a href="#hockey">Hockey Sticks & Ice Skates</a></li>
												<li><a href="#paddle">Paddle Boards & Paddles</a></li>
												<li><a href="#parachute">Parachutes</a></li>
												<li><a href="#scooter">Scooters</a></li>
												<li><a href="#scuba">Scuba Equipment</a></li>
												<li><a href="#selfie">Selfie-Sticks / Tripods</a></li>
												<li><a href="#skate">Skateboards</a></li>
												<li><a href="#ski">Skis and Snowboards</a></li>
												<li><a href="#surf">Surfing</a></li>
												<li><a href="#tennis">Tennis Rackets</a></li>
												<li><a href="#waterski">Water Skiing</a></li>
											</ul>
										</div>
										<?php	include_once('partials/sidebar_otherinfo.php'); ?>
									</div>
								</div>


								<div class="clearfix">&nbsp;</div>
								<br>
								<br>
								<!-- footer -->
								<?php	include_once('partials/footer.php'); ?>
								<!-- end footer -->
							</div>
							<!-- end page content -->
						</div>
					</div>
					<!-- end container -->
			</div>
			<!-- end slide wrapper -->

			<?php	include_once('partials/tail.php'); ?>

	</body>

</html>
