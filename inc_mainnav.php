<nav class="primary-menu navbar navbar-default">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php">
				<img src="images/logo-novoair.png" class="img-responsive">
			</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">




			<ul class="nav navbar-nav navbar-right nav-pills">


				<li class="active dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Plan a Trip<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="https://secure.flynovoair.com/bookings/flight_selection.aspx">Book Flights</a></li>
						<li><a href="javascript:void(0)">Book Holiday Package</a></li>
						<li><a href="javascript:void(0)">Book Hotel</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="https://secure.flynovoair.com/bookings/retrieve_reservation.aspx">Manage Booking</a></li>
					</ul>
				</li>


				<li>
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Travel Info <span class="caret"></span></a>
					<ul class="dropdown-menu multi-level">


						<li class="dropdown-submenu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Destinations</a>
							<ul class="dropdown-menu">
								<li class="dropdown-submenu">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">International</a>
									<ul class="dropdown-menu">
										<li><a href="javascript:void(0)">Yangon</a></li>
                    <li role="separator" class="divider"></li>
										<li><a href="javascript:void(0)">Future Destinations</a></li>
									</ul>
								</li>

								<li class="dropdown-submenu">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">Domestic</a>
									<ul class="dropdown-menu">
										<li><a href="javascript:void(0)">Dhaka</a></li>
										<li><a href="javascript:void(0)">Chittagong</a></li>
										<li><a href="javascript:void(0)">Cox's Bazar</a></li>
										<li><a href="javascript:void(0)">Jessore</a></li>
										<li><a href="javascript:void(0)">Sylhet</a></li>
									</ul>
								</li>
							</ul>
						</li>

						<li class="dropdown-submenu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Tourist Information</a>
							<ul class="dropdown-menu">
								<li><a href="#">Tourist Spots</a></li>
								<li><a href="#">Tips &amp; Guides</a></li>
							</ul>
						</li>

						<li role="separator" class="divider"></li>


						<li><a href="flight_schedule.php">Flight Schedule</a></li>
						<li><a href="fare_chart.php">Fare Search</a></li>
						<li><a href="table-schedule_todays.php">Flight Satus</a></li>
						<li><a href="javascript:void(0)">Route Map</a></li>
						<li role="separator" class="divider"></li>

						<li class="dropdown-submenu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Baggage Information</a>
							<ul class="dropdown-menu">
								<li><a href="#">Baggage Size</a></li>
								<li><a href="#">Prohibited Items</a></li>
								<li><a href="#">Permitted Items</a></li>
							</ul>
						</li>


						<li role="separator" class="divider"></li>
						<li><a href="#">Gallery</a></li>
					</ul>
				</li>




				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="services_facilities.php">In-flight Comforts</a></li>
						<li><a href="">Special Assistance</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="">Smiles Frequent Flyer</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="">Visa Services</a></li>
						<li><a href="">Holiday Packages</a></li>
						<li><a href="">Locate Nearest Sales Office</a></li>
					</ul>
				</li>




				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About Us<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="about_us.php">Company Information</a></li>

						<li role="separator" class="divider"></li>

						<li><a href="contact.php">Contact</a></li>
						<li><a href="javascript:void(0)" class="sub_window" path="http://www.flynovoair.com/webim/client.php" width="650" height="560">Live Chat</a></li>
						<li><a href="http://www.facebook.com/pages/novoair/128001720686051?fref=ts">Facebook</a></li>
						<li><a href="https://twitter.com/intent/user?screen_name=novoair">Twitter</a></li>
						<li><a href="">Youtube</a></li>
						<li><a href="">Give Feedback</a></li>

						<li role="separator" class="divider"></li>

						<li><a href="">Terms &amp; Conditions</a></li>
						<li><a href="">Privacy Policy</a></li>

					</ul>
				</li>

				<!--<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="#">Action</a></li>
						<li><a href="#">Another action</a></li>
						<li><a href="#">Something else here</a></li>
						<li role="separator" class="divider"></li>
						<li class="dropdown-header">Nav header</li>
						<li><a href="#">Separated link</a></li>
						<li><a href="#">One more separated link</a></li>
					</ul>
				</li>-->
			</ul>


		</div>
		<!--/.nav-collapse -->
	</div>
</nav>
