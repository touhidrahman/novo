<?php
	require_once('setting/config.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
 <?php
 	include_once('inc_top_includes.php');
 ?>
</head>
<body class="inner">

    <!-- top nav -->
			 <?php
                include_once('inc_topnav.php');
             ?>     
             
	<!-- end top nav -->  
    
    

<br>
<br>
<br>

<div class="container help-line">
    <div>
        <img src="images/help-line.png" class="hidden-xs">
        <img src="images/help-line2.png" class="visible-xs">
    </div>
</div>


<!-- main navigation -->
			 <?php
                include_once('inc_mainnav.php');
             ?>           
<!-- end main navigation -->





<div class="slide-wrapper">
<!-- inner page Carousel -->
 <?php
    include_once('inc_carousel_inner.php');
 ?>   
<!-- end inner page Carousel -->   



  <!-- content -->
    <div class="container">
    
    	<div class="body-container">
    
    
        
         <div class="breadcrumb">
        <div class="row">
          <div  class="col-sm-6">
            <h1><b>Fare </b> Chart</h1>
          </div>
          <div class="col-sm-6 text-right"> Travel Info / Fare </b>Chart </div>
        </div>
      </div>     
        
      


        
        <div class="page-contents">
        

        
        
        
        
        
        

	




        <div class="row">
        	

            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
	         <h2>ONE WAY FARE (BDT)</h2>
             

	<br>

<p>
<img src="images/page_headers/farechart.jpg" class="img-responsive">
</p>    
   <br> 
        
    <table border="0" cellpadding="0" cellspacing="0" class="table table fare-chart">
        <tr>
          <th width="50%">ROUTE</th>
          <th width="25%">FARE TYPE</th>
          <th width="25%">FARE</th>
        </tr>
        <tr>
          <td><strong>Dhaka - Chittagong </strong>& vice versa</td>
          <td colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table"	>
              <tr>
                <td width="50%" rowspan="2">Flexible</td>
                <td>8,200 </td>
              </tr>
              <tr>
                <td>7,600</td>
              </tr>
              <tr>
                <td rowspan="2">Saver</td>
                <td>7,100 </td>
              </tr>
              <tr>
                <td>6,600</td>
              </tr>
              <tr>
                <td rowspan="2">Discounted</td>
                <td>6,000 </td>
              </tr>
              <tr>
                <td>5,500</td>
              </tr>
              <tr>
                <td rowspan="2">Special</td>
                <td>5,000 </td>
              </tr>
              <tr>
                <td>4,700</td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td><strong>Dhaka - Cox’s Bazar</strong> & vice versa</td>
          <td colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table"	>
              <tr>
                <td width="50%" rowspan="2">Flexible</td>
                <td>9,200</td>
              </tr>
              <tr>
                <td>8,600</td>
              </tr>
              <tr>
                <td rowspan="2">Saver</td>
                <td>8,100 </td>
              </tr>
              <tr>
                <td>7,500</td>
              </tr>
              <tr>
                <td rowspan="2">Discounted</td>
                <td>6,800 </td>
              </tr>
              <tr>
                <td>6,400</td>
              </tr>
              <tr>
                <td rowspan="2">Special</td>
                <td>6,000 </td>
              </tr>
              <tr>
                <td>5,700</td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td><strong>Dhaka - Sylhet</strong> & vice versa</td>
          <td colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table"	>
              <tr>
                <td width="50%" rowspan="2">Flexible</td>
                <td>7,200 </td>
              </tr>
              <tr>
                <td>6,500</td>
              </tr>
              <tr>
                <td rowspan="2">Saver</td>
                <td>5,900 </td>
              </tr>
              <tr>
                <td>5,500</td>
              </tr>
              <tr>
                <td rowspan="2">Discounted</td>
                <td>5,000 </td>
              </tr>
              <tr>
                <td>4,400</td>
              </tr>
              <tr>
                <td rowspan="2">Special</td>
                <td>4,000 </td>
              </tr>
              <tr>
                <td>3,700</td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td><strong>Dhaka - Jessore</strong> & vice versa</td>
          <td colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table"	>
              <tr>
                <td width="50%" rowspan="2">Flexible</td>
                <td>6,700 </td>
              </tr>
              <tr>
                <td>6,100</td>
              </tr>
              <tr>
                <td rowspan="2">Saver</td>
                <td>5,600 </td>
              </tr>
              <tr>
                <td>5,000</td>
              </tr>
              <tr>
                <td rowspan="2">Discounted</td>
                <td>4,500</td>
              </tr>
              <tr>
                <td>4,200</td>
              </tr>
              <tr>
                <td rowspan="2">Special</td>
                <td>4,000 </td>
              </tr>
              <tr>
                <td>3,700</td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td><strong>Dhaka - Yangon</strong> & vice versa</td>
          <td colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table"	>
              <tr>
                <td>Special</td>
                <td>25,220 </td>
              </tr>
              
            </table></td>
        </tr>
      </table>
        
<br>
<br>


             
<ul class="smalltext">

					  <li>

					    Conditions apply

				      </li>

					  <li>Fares are inclusive of all Taxes &amp; Sur-Charges.</li>

					  <li>Fares, Taxes &amp; Sur-Charges are subject to change without any prior notice.                   

				      </li>

					  </ul>            
                    
               <br>
<br>
<strong>Baggage Allowances</strong>

<ul>

                                  <li>Checked Baggage – 20 Kg</li>

                                  <li>Cabin Baggage – 7 Kg</li>

                                  <li>Excess and Over-Weight Baggage – BDT 100 per Kg</li>

                               </ul>




                
            </div>
            
            <div class="first-col col-lg-3 col-md-3  hidden-sm hidden-xs ">
                <!-- icon buttons -->
                 <?php
                    include_once('inc_inner_sidebar_iconbuttons.php');
                 ?>
                <!-- end icon buttons -->
            </div>            
            
        </div>
		
        <div class="clearfix">&nbsp;</div>
        
                <!-- icon buttons on bottom -->
                 <?php
                    include_once('inc_inner_bottombar_iconbuttons.php');
                 ?>
                <!-- end icon buttons on bottom-->
<br>
<br>




<!-- footer -->
 <?php
 	include_once('inc_footer.php');
 ?>
<!-- end footer -->








                    
        </div>
        
        
        
    </div>
    </div>
    <!-- end content -->


</div>

<div style="clear:both"></div>







 <?php
 	include_once('inc_bottom_includes.php');
 ?>


   

</body>
</html>
