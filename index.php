<?php	require_once('setting/config.php'); ?>
<!DOCTYPE html>
<html lang="en">

<?php	include_once('inc.head.php'); ?>

<body class="home">
		<br>
		<br>
		<br>

		<div class="container help-line">
			<div>
				<img src="images/help-line.png" class="hidden-xs">
				<img src="images/help-line2.png" class="visible-xs">
			</div>
		</div>

		<!-- main navigation -->
		<?php include_once('inc.mainnav2.php'); ?>
		<!-- end main navigation -->


		<div class="slide-wrapper">

			<?php	include_once('home/carousel.php'); ?>


			<!-- content -->
			<div class="container body-wrapper" s tyle="border:0px solid #000000; bac kground:#EC4447;">
				<div class="body-container" sty le="border:10px solid #000000; background:#EC4447;">

					<?php	include_once('home/booking_form.php'); ?>


					<div class="page-contents">

						<?php	include_once('home/fare_deals.php'); ?>

						<div class="clearfix">&nbsp;</div>

						<?php	include_once('home/destinations.php'); ?>

						<br>
						<br>


						<!-- ongoing offers -->
						<?php	include_once('home/ongoing_offers.php'); ?>
						<!-- end ongoing offers -->

						<!-- footer -->
						<?php	include_once('inc.footer.php'); ?>
							<!-- end footer -->
					</div>
					<!-- end page content -->


				</div>
			</div>
			<!-- end container -->
		</div>
		<!-- end slide wrapper -->

		<?php	include_once('inc.tail.php'); ?>

</body>

</html>
