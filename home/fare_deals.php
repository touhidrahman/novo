<h1><b>Fare </b>Deals</h1>

<div class="row featured-deals animate">
    <div class="col-md-3 col-sm-6 col-xs-6"><span><h3>Coxs Bazar</h3><h2>TK 5500</h2></span><img src="images/feature_deal/coxsbazar.jpg" class="img-responsive"></div>
    <div class="col-md-3 col-sm-6 col-xs-6"><span><h3>Chittagong</h3><h2>TK 4500</h2></span><img src="images/feature_deal/chittagong.jpg" class="img-responsive"></div>
    <div class="clearfix visible-sm"></div>
    <div class="col-md-3 col-sm-6 col-xs-6"><span><h3>Sylhet</h3><h2>TK 4200</h2></span><img src="images/feature_deal/sylhet.jpg" class="img-responsive"></div>
    <div class="col-md-3 col-sm-6 col-xs-6"><span><h3>Jessore</h3><h2>TK 4000</h2></span><img src="images/feature_deal/jessore.jpg" class="img-responsive"></div>
</div>
