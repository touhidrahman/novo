<div class="row destination-icons-left animate">

	<div class="first-col col-lg-3 col-md-3  hidden-sm hidden-xs">
		<a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=novoair">
			<img src="images/left-btn-locate-salse-office.jpg" class="img-responsive">
		</a>
		<a href="javascript:void(0)" class="sub_window" path="http://www.flynovoair.com/webim/client.php" width="650" height="560">
			<img src="images/left-btn-live-chat.jpg" class="img-responsive">
		</a>
		<a href="flight_schedule.php">
			<img src="images/left-btn-flight-schedule.jpg" class="img-responsive">
		</a>
		<a href="todays_flight_schedule.php">
			<img src="images/left-btn-todays-flights.jpg" class="img-responsive">
		</a>
	</div>
	<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">

		<div class="bg-compass">
			<h1 class="padding-top-10">
				<strong>DESTINATIONS</strong>
			</h1>

			<div class="home-content-scroll">

				<div class="row">

					<div class="col-xs-3">
						<img src="images/destinations/yangon.jpg" class="img-responsive">
					</div>
					<div class="col-xs-9">
						<strong>Yangon</strong>, is a former capital of Myanmar (Burma) and the capital of Yangon Region. Yangon is the country's largest city with a population of over five million, and is the most important commercial centre
						<a href="destinations.php?show=dhaka">More</a>
					</div>

				</div>
				<div class="row">

					<div class="col-xs-3">
						<img src="images/destinations/dhaka.jpg" class="img-responsive">
					</div>
					<div class="col-xs-9">
						<strong>Dhaka</strong>, is the capital and largest city of Bangladesh. With its colorful history and rich cultural traditions, Dhaka is known the world over as the city of mosques and muslin.
						<a href="destinations.php?show=dhaka">More</a>
					</div>

				</div>
				<div class="row">
					<div class="col-xs-3">
						<img src="images/destinations/chittagong.jpg" class="img-responsive">
					</div>
					<div class="col-xs-9">
						<strong>Chittagong</strong> is the biggest seaport and second largest town in Bangladesh situated by the Bay of Bengal. It is 264 km away east of Dhaka, famous for hill areas
						<a href="destinations.php?show=chittagong">More</a>
					</div>

				</div>
				<div class="row">
					<div class="col-xs-3">
						<img src="images/destinations/coxesbazar.jpg" class="img-responsive">
					</div>
					<div class="col-xs-9">
						<strong>Cox's Bazar</strong> is one of the most attractive tourist spots & the longest sea beach in the world (approx. 120 km long). Miles of golden sands, towering cliffs, surfing waves,
						<a href="destinations.php?show=coxbazar">More</a>
					</div>
				</div>

				<div class="row">

					<div class="col-xs-3">
						<img src="images/destinations/jessore.jpg" class="img-responsive">
					</div>
					<div class="col-xs-9">
						<strong>Jessore</strong> is a district in the southwestern region of Bangladesh. Apart from some attractive colonial architecture, most notably the huge Court House building, there is little to see in Jessore in terms of sights
						<a href="destinations.php?show=jessore">More</a>
					</div>
				</div>


				<div class="row">

					<div class="col-xs-3">
						<img src="images/destinations/sylhet.jpg" class="img-responsive">
					</div>
					<div class="col-xs-9">
						<strong>Sylhet</strong> is the land of Shrine’s, natural Hills, forests, beautiful tree plantations and lots of Haors. It is an old city with full of natural beauties. A large number of tourists come every year to visit Sylhet.
						<a href="destinations.php?show=sylhet">More</a>
					</div>
				</div>

			</div>
		</div>

	</div>

</div>

<div class="clearfix">&nbsp;</div>













<div class="row hidden-lg hidden-md  destination-icons-bottom animate">
  <div class="col-xs-3">
    <a href="javascript:void(0);" class="sub_window" title="View Location Map" width="700px" height="600px" path="corp_contact_googlemap.php?target=novoair">
      <img src="images/left-btn-locate-salse-office-2.jpg" class="img-responsive hidden-xs">
      <img src="images/icons/location.jpg" class="img-responsive hidden-sm">
    </a>
  </div>
  <div class="col-xs-3">
    <a href="javascript:void(0)" class="sub_window" path="http://www.flynovoair.com/webim/client.php" width="650" height="560">
      <img src="images/left-btn-live-chat.jpg" class="img-responsive hidden-xs">
      <img src="images/icons/chat.jpg" class="img-responsive hidden-sm">
    </a>
  </div>
  <div class="col-xs-3">
    <a href="flight_schedule.php">
      <img src="images/left-btn-flight-schedule.jpg" class="img-responsive hidden-xs">
      <img src="images/icons/schedule.jpg" class="img-responsive hidden-sm">
    </a>
  </div>
  <div class="col-xs-3">
    <a href="todays_flight_schedule.php">
      <img src="images/left-btn-todays-flights.jpg" class="img-responsive hidden-xs">
      <img src="images/icons/todays-flight.jpg" class="img-responsive hidden-sm">
    </a>
  </div>
</div>
