<div class="search-panel" style="z-index:5000">
	<!--	        <img src="images/dummy-search.jpg" width="100%">
&nbsp;-->

	<div class="row search-bg test22">
		<div class="col-md-9 search">



			<!-- search form -->



			<ul class="nav nav-tabs responsive" id="myTab">
				<li class="active"><a href="#book">BOOK NOW</a></li>
				<li><a href="#manage">MANAGE BOOKING</a></li>
			</ul>

			<div class="tab-content responsive">


				<div class="tab-pane active" id="book">

					<form id="searchForm" met hod="get" ac tion="http://secure.flynovoair.com/bookings/flight_selection.aspx" target="_top" class="flight_search TT_RT-checked " ons ubmit="document.getElementById('content').className+=' searching';">
						<div class="row [1]">

							<div class="trip-type-part col-sm-3">

								<!-- radio -->
								<div class="trip_type row" id="flight_types">

									<div class="col-xs-4 col-sm-12" style="text-align:right">
										<input type="radio" name="TT" id="TT_RT" value="RT" checked />
										<label for="TT_RT" class="selected" title="Rount Trip" alt="Rount Trip">ROUND</label>
									</div>
									<div class="col-xs-4 col-sm-12">
										<input type="radio" name="TT" id="TT_OW" value="OW" />
										<label for="TT_OW" title="One Way Trip" alt="One Way Trip">ONE WAY</label>
									</div>
									<div class="col-xs-4 col-sm-12">
										<input type="radio" name="TT" id="TT_MC" value="MC" onClick="parent.location.href='http://secure.flynovoair.com/bookings/flight_selection.aspx?TT=MC';" />
										<label for="TT_MC" title="Multy City" alt="Multy City">MULTI-CITY <span class="glyphicon glyphicon-new-window"></span></label>
										<!--<input type="text" name="result">-->
									</div>

								</div>

								<!-- end radio -->

							</div>
							<div class="form-part col-sm-9">

								<!-- form -->


								<div class="row test2d">
									<div class="col-lg-9">
										<div class="from-to-part row  [location] ">

											<div class="from-part col-sm-6">
												<div class="styled-select">
													<select name="DC" title="From" alt="From">
														<option value="" selected>FROM</option>
														<option value="CGP" data-targets="DAC">Chittagong</option>
														<option value="CXB" data-targets="DAC">Cox&#39;s Bazar</option>
														<option value="DAC" data-targets="CGP,CXB,JSR,ZYL" selected>Dhaka</option>
														<option value="JSR" data-targets="DAC">Jessore</option>
														<option value="ZYL" data-targets="DAC">Sylhet</option>
														<option value="RGN" data-targets="DAC">Yangon</option>
													</select>
												</div>
											</div>
											<div class="to-part col-sm-6">
												<div class="styled-select">
													<select name="AC" title="To" alt="To">
														<option value="" selected>TO</option>
														<option value="DAC" data-targets="CGP,CXB,JSR,ZYL">Dhaka</option>
														<option value="CGP" data-targets="DAC" selected>Chittagong</option>
														<option value="CXB" data-targets="DAC">Cox&#39;s Bazar</option>
														<option value="JSR" data-targets="DAC">Jessore</option>
														<option value="ZYL" data-targets="DAC">Sylhet</option>
														<option value="RGN" data-targets="DAC">Yangon</option>
													</select>
												</div>

											</div>
										</div>


										<div class="row  [date] ">

											<div class="from-part col-sm-6">
												<input type="text" name="from-date" id="from-date" cl ass="form-control" value="DEPARTING" title="Departing Date" alt="Departing Date">
											</div>
											<div class="to-part col-sm-6">
												<input type="text" name="to-date" id="to-date" cl ass="form-control" value="RETURNING" title="Returning Date" alt="Returning Date">
											</div>
										</div>

									</div>
									<div class="col-lg-3">
										<div class="adult-child-part row">
											<div class="col-xs-4 col-lg-12">
												<div class="styled-select no-icon">
													<select name="PA" title="Number of Adults" alt="Number of Adults">
														<option value="">ADULT</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
													</select>
												</div>
											</div>
											<div class="col-xs-4  col-lg-6">
												<div class="styled-select no-icon">
													<select name="PC" title="Number of Children" alt="Number of Children">
														<option value="">CHILD</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
													</select>
												</div>
											</div>

											<div class="col-xs-4 col-lg-6">
												<div class="styled-select no-icon">
													<select name="PI" title="Number of Infants" alt="Number of Infants">
														<option value="">INFANT</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
													</select>
												</div>
											</div>

										</div>
									</div>
								</div>

								<div class="row test2d">

									<div class="col-sx-12 col-sm-9  col-md-9">
										<div class="row fixed-flexible" id="is_flexible">
											<div class="col-xs-6">
												<input type="radio" name="FL" value="" id="FL_OFF" />
												<label for="FL_OFF" title="Fixed Dates" alt="Fixed Dates">FIXED DATE</label>
											</div>
											<div class="col-xs-6">
												<input type="radio" name="FL" value="on" id="FL_ON" checked />
												<label for="FL_ON" class="selected" title="Flexible Dates" alt="Flexible Dates">FLEXIBLE DATE</label>
											</div>
										</div>

									</div>
									<div class="col-sx-12 col-sm-3 col-md-3">
										<input type="submit" value="SEARCH" class="btn-search">
									</div>

								</div>



								<!-- end form -->
							</div>

						</div>
					</form>
				</div>

				<div class="tab-pane" id="manage">

					<form id="manageBookingForm" method="post" action="http://secure.flynovoair.com/bookings/retrieve_reservation.aspx" target="_top" class="flight_search TT_RT-checked " on submit="document.getElementById('content').className+=' searching';">


						<div class="row manage-booking">
							<div class="col-sm-4">


								<div class="row">
									<label class="col-xs-12">Reservation #</label>
									<div class="col-xs-12">
										<input type="text" name="pnr" value="">
									</div>
								</div>


							</div>
							<div class="col-sm-4">

								<div class="row">
									<label class="col-xs-12">First or Last Name</label>
									<div class="col-xs-12">
										<input type="text" name="passengerName">
									</div>
								</div>

							</div>
							<div class="col-sm-4">

								<div class="row">
									<label class="hidden-xs">&nbsp;</label>
									<div class="col-xs-12">
										<input type="submit" class="btn-manage" value="Find Reservation" style="width:120px!important;">
									</div>
								</div>
							</div>

						</div>
						<label class="visible-xs">&nbsp;</label>
						<div class="row">
							<div class="col-xs-12 instruction">*Enter either the first OR the last name of one passenger on the reservation.</div>
						</div>
					</form>
				</div>
			</div>





			<!-- end search form -->



		</div>
		<div class="col-md-3 smile-bg">

			&nbsp;

			<!-- com smiles login -->
			<?php include_once('smiles_login.php');?>
				<!-- end com smiles login -->







		</div>

	</div>


</div>
