<?php
function parserSide($feedURL, $status = null) {
	
	$rss = simplexml_load_file($feedURL);
	
	
	
	echo '<table class="table">';
			
			echo '<tr><th>Flight</th><th>From</th><th>&nbsp;</th><th>To</th><th>Time</th><th>Status</th></tr>';

		foreach ($rss->channel->item as $feedItem)
		{
			
			if($feedItem->DepartureStatus != 'Cancelled')
			{
				echo '<tr>';
				echo '<td>'.$feedItem->FlightNumber.'</td>';
				echo '<td>'.$feedItem->Origin.'</td>';
				echo '<td>-</td>';
				echo '<td>'.$feedItem->Destination.'</td>';
				echo '<td>'.$feedItem->STD.'</td>';
				if($status != null)
				{
					echo '<td>'.$feedItem->DepartureStatus.'</td>';
				}else{
					echo '<td>NA</td>';
					}
				echo '</tr>';
			}
	
		}
	
	
	echo '</table>';
}

?>